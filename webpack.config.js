const path = require('path');
const autoprefixer = require('autoprefixer');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CopyWebpackPlugin = require("copy-webpack-plugin");
const SRC_DIR = path.resolve(__dirname, "src");
const Proton = path.resolve(__dirname, "Proton");
const DIST_DIR = path.resolve(__dirname, "build/www");
const babelpolyfill = require("babel-polyfill");



module.exports = {
    devtool: 'cheap-module-eval-source-map',
    entry: ["babel-polyfill","./src/index.js"],
    output: {
        path: path.resolve(__dirname, 'dist'),
        filename: 'bundle.js',
        chunkFilename: '[id].js',
        publicPath: ''
    },
    resolve: {
        extensions: ['.js', '.jsx'],
        modules: ['node_modules'],
        alias: {
          Proton: path.join(__dirname, './Proton/')
        }
    },
   
    module: {
        rules: [
            {
                test: /\.js$/,
                loader: 'babel-loader',
                exclude: /node_modules/
            },
            {
                test: /\.(html)$/,
                use: {
                  loader: 'html-loader',
                  options: {
                    attrs: [':data-src']
                  }
                }
              },
              
            
            {
                test: /\.css$/,
                exclude: /node_modules/,
                use: [
                    { loader: 'style-loader' },
                    {
                        loader: 'css-loader',
                        options: {
                            importLoaders: 1,
                            modules: true,
                            localIdentName: '[name]__[local]__[hash:base64:5]'
                        }
                    },
                    {
                        loader: 'postcss-loader',
                        options: {
                            ident: 'postcss',
                            plugins: () => [
                                autoprefixer({
                                    browsers: [
                                        "> 1%",
                                        "last 2 versions"
                                    ]
                                })
                            ]
                        }
                    },
                    
                            
                ]
            },
            {
                test: /\.(png|jpe?g|gif)$/,
                loader: 'url-loader?limit=8000&name=images/[name].[ext]'
            }
        ]
    },
    devServer: {
        headers:{
            
             "X-Frame-Options": "SAMEORIGIN",
           //  "X-Frame-Options": "allow-from https://openam.collabkare.com/",
             "Access-Control-Allow-Origin": "*",
             "Access-Control-Allow-Methods": "GET, POST, PUT, DELETE, PATCH, OPTIONS",
             "Access-Control-Allow-Headers": "X-Requested-With, content-type, Authorization"
           },
        //contentBase :'./dist',
        disableHostCheck: true,

        historyApiFallback: true,
        compress: true,
        port: 8080
    },
    plugins: [
        // new CopyWebpackPlugin(
        //     [

        //         {
        //             from: SRC_DIR + "src/assets/Data/production.json",
        //             to: DIST_DIR + "/data/poduction.json"
        //         },


        //     ],

        // ),
        // new CopyPlugin([
        //     { from: 'src/assets/Data/production.json', to: '/data/poduction.json' },
        
        //   ]),
        new HtmlWebpackPlugin({
            template: __dirname + '/src/index.html',
            filename: 'index.html',
            inject: 'body'
        })
    ]
};