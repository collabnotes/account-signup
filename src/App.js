import React, { Component } from 'react';
import Layout from "./components/containers/Layout/Layout"
import AccountSign from "./components/containers/AccountSign/AccountSign"
import { Route, Switch } from "react-router-dom";
import Account from "./components/containers/Account/Account";
import Login from "./components/containers/Login/Login"
import "./index.css";
import { connect } from 'react-redux';
import { config } from './components/redux/action-creators/action-creators'
import Proton from './components/containers/Proton/Proton';
import Redirecturl from './components/containers/Redirect/Redirect';
import AccountStandard from './components/containers/Accountstandard/AccountStandard';
import ForgotPassword from './components/containers/ForgotPassword/ForgotPassword';
import Profile from './components/containers/Profile/profile';
import NotFound from './components/containers/NotFound/NotFound';
import ResetPassword from "./components/containers/Profile/Resetpassword";
import TwoStepVerification from "./components/containers/Profile/twostepverification";
import EmailVerification from "./components/containers/Account/emailverification/emailverification";
import Start from './components/containers/start/start'
class App extends Component {
    constructor(props) {
        super(props);
    }
    componentDidMount() {
        this.props.config();
    }
    render() {

        return (
            <Layout>
                <Switch>

                    {/* <Route path='/account' component={AccountSign} /> */}
                    {/* <Route path='/account' ><Account/></Route>  */}

                    <Route path='/signup' component={AccountSign} />

                    <Route path='/home' component={Redirecturl} />
                    <Route path='/demo' component={AccountStandard} />
                    <Route path='/profile' component={Profile} />
                    <Route path='/forgotpassword' component={ForgotPassword} />
                    {/* <Route path='/signup' ><AccountSign/></Route> */}
                    <Route path="/resetpassword" component={ResetPassword} />
                    <Route path="/twostepverification" component={TwoStepVerification} />
                    <Route path='/verification' component={EmailVerification} />
                    <Route path='/login' component={Login} />
                    <Route path='/' component={Start} />
                    {/* <Route path='/' component={Proton} /> */}


                    {/* <Route path='/login' ><Login/></Route>  */}
                </Switch>

            </Layout>
        );
    }
}
const mapStateToProps = state => {
    return {
        isAuthenticated: state.login.isAuthenticated,

    }
}
const mapDispatchToProps = dispatch => {
    return {

        config: () => {
            dispatch(config());
        }
    };
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(App);








