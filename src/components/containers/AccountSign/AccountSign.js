import React, { useState } from 'react';
import AppBar from '@material-ui/core/AppBar';
import Button from '@material-ui/core/Button';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardHeader from '@material-ui/core/CardHeader';
import CssBaseline from '@material-ui/core/CssBaseline';
import Grid from '@material-ui/core/Grid';
import StarIcon from '@material-ui/icons/StarBorder';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Link from '@material-ui/core/Link';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import Box from '@material-ui/core/Box';
import Account from "../Account/Account";
 import AccountStandard from '../Accountstandard/AccountStandard'

const useStyles = makeStyles((theme) => ({
    '@global': {
        ul: {
            margin: 0,
            padding: 0,
            listStyle: 'none',
        },
    },
    appBar: {
        borderBottom: `1px solid ${theme.palette.divider}`,
    },
    toolbar: {
        flexWrap: 'wrap',
    },
    toolbarTitle: {
        flexGrow: 1,
    },
    link: {
        margin: theme.spacing(1, 1.5),
    },
    heroContent: {
        padding: theme.spacing(8, 0, 6),
    },
    cardHeader: {
        backgroundColor:
            theme.palette.type === 'light' ? theme.palette.grey[200] : theme.palette.grey[700],
    },
    cardPricing: {
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'baseline',
        marginBottom: theme.spacing(2),
    },
    footer: {
        borderTop: `1px solid ${theme.palette.divider}`,
        marginTop: theme.spacing(8),
        paddingTop: theme.spacing(3),
        paddingBottom: theme.spacing(3),
        [theme.breakpoints.up('sm')]: {
            paddingTop: theme.spacing(6),
            paddingBottom: theme.spacing(6),
        },
    },
}));



export default function AccountSign() {
    const classes = useStyles();
    const [enter, setEnter] = useState(true)
    const [enterprise, setEnterprise] = useState(false)
    const [bus, setBuss] = useState(false)
    const handleenterprise = () => {
        setEnter(false)
        setEnterprise(true)
    }
    const handleBus = () => {
        setEnter(false)
        setBuss(true)
    }
    const tiers = [
        {
            title: 'Basic',
            price: '0',
            description: ['10 users included', '2 GB of storage', 'Help center access', 'Email support'],
            buttonText: 'Sign up for free',
            buttonVariant: 'outlined',
            onClick:handleBus
        },
        // {
        //     title: 'Business',
        //     subheader: 'Most popular',
        //     price: '0',
        //     description: [
        //         '20 users included',
        //         '10 GB of storage',
        //         'Help center access',
        //         'Priority email support',
        //     ],
        //     buttonText: 'Sign up for free',
        //     buttonVariant: 'outlined',
        //     onClick:handleBus
        // },
        {
            title: 'Enterprise',
            price: '0',
            description: [
                '50 users included',
                '30 GB of storage',
                'Help center access',
                'Phone & email support',
            ],
            buttonText: 'Sign up for free',
            buttonVariant: 'outlined',
            onClick:handleenterprise
        },
    ];
    return (
        <React.Fragment>
            <Account />
           {/* {enter ? <div>
          <CssBaseline />

            <Container maxWidth="sm" component="main" className={classes.heroContent}>
                <Typography component="h1" variant="h2" align="center" color="textPrimary" gutterBottom>
                  
        </Typography>

            </Container>
            
            <Container maxWidth="md" component="main" style={{marginRight: '0px'}}>
                <Grid container spacing={5} alignItems="flex-end">
                    {tiers.map((tier) => (
                        // Enterprise card is full width at sm breakpoint
                        <Grid item key={tier.title} xs={12} sm={6} md={4} >
                            <Card>
                                <CardHeader
                                    title={tier.title}
                                    subheader={tier.subheader}
                                    titleTypographyProps={{ align: 'center' }}
                                    subheaderTypographyProps={{ align: 'center' }}
                                    action={tier.title === 'Pro' ? <StarIcon /> : null}
                                    className={classes.cardHeader}
                                />
                                <CardContent>
                                    <div className={classes.cardPricing}>
                                        <Typography component="h2" variant="h3" color="textPrimary">
                                            ${tier.price}
                                        </Typography>
                                        <Typography variant="h6" color="textSecondary">
                                            /mo
                                    </Typography>
                                    </div>
                                    <ul>
                                        {tier.description.map((line) => (
                                            <Typography component="li" variant="subtitle1" align="center" key={line}>
                                                {line}
                                            </Typography>
                                        ))}
                                    </ul>
                                </CardContent>
                                <CardActions>
                                    <Button fullWidth variant={tier.buttonVariant} color="primary" onClick={tier.onClick}>
                                        {tier.buttonText}
                                    </Button>
                                </CardActions>
                            </Card>
                        </Grid>
                    ))}
                </Grid>
            </Container>
            </div>:''}
            {enterprise ? <Account /> : ''}

            {bus ? <div>
                <AccountStandard />
            </div> : ''} */}
        </React.Fragment>
    );
}