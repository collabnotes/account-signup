import React, { useState, useEffect } from "react";
import ReactDOM from "react-dom";
import QRCode from "qrcode.react";
import { Button, Grid } from "@material-ui/core";
import { makeStyles } from '@material-ui/core/styles';
import axios from "axios";
import Cookies from 'universal-cookie';
import IconButton from '@material-ui/core/IconButton';
import Visibility from '@material-ui/icons/Visibility';
import VisibilityOff from '@material-ui/icons/VisibilityOff';
import FormControl from '@material-ui/core/FormControl';
import OutlinedInput from '@material-ui/core/OutlinedInput';
import InputLabel from '@material-ui/core/InputLabel';
import InputAdornment from '@material-ui/core/InputAdornment';
import clsx from 'clsx';
const useStyles = makeStyles((theme) => ({
  paper: {
    //marginTop: theme.spacing(10),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  margin: {
    margin: theme.spacing(0),
    //margin: 'auto'
  },
  withoutLabel: {
    marginTop: theme.spacing(3),
  },
  textField: {
    width: '100%',
    //margin: 'auto'
  },

}));
const QrCode = (props) => {
  const cookies = new Cookies();
  const [qrValue, setQrValue] = useState("");
  const [data, setdata] = useState(props.mfadata)
  const classes = useStyles();
  useEffect(() => {


    axios.post(`https://account-api.collabkare.com/${cookies.get('domain')}/authenticate`, {
      "authId": data[0].authId,
      "template": "",
      "stage": "AuthenticatorOATH2",
      "header": "ForgeRock Authenticator (OATH)",
      "callbacks": [
        {
          "type": "ConfirmationCallback",
          "output": [
            {
              "name": "prompt",
              "value": ""
            },
            {
              "name": "messageType",
              "value": 0
            },
            {
              "name": "options",
              "value": [
                "Register device",
                "Skip this step"
              ]
            },
            {
              "name": "optionType",
              "value": -1
            },
            {
              "name": "defaultOption",
              "value": 0
            }
          ],
          "input": [
            {
              "name": "IDToken1",
              "value": 0
            }
          ]
        }
      ]
    }).then(res => {
      setQrValue(res.data.callbacks[1].output[0].value.split("'")[3])
      console.log(res.data.callbacks[1].output[0].value.split("'")[3], res)
      handleQRcode(res)
    })
      .catch(err => {
        console.log(err)
      })

  }, [])
  const handleQRcode = (res) => {
    console.log(res)
    axios.post(`https://account-api.collabkare.com/${cookies.get('domain')}/authenticate`, {
      "authId": res.data.authId,
      "template": "",
      "stage": "AuthenticatorOATH5",
      "header": "Register a device",
      "callbacks": [
        {
          "type": "TextOutputCallback",
          "output": [
            {
              "name": "message",
              "value": "\n            Scan the barcode image below with the ForgeRock Authenticator App. Once registered click the button to\n            enter your verification code and login.\n        "
            },
            {
              "name": "messageType",
              "value": "0"
            }
          ]
        },
        {
          "type": "TextOutputCallback",
          "output": [
            {
              "name": "message",
              "value": res.data.callbacks[1].output[0].value
            },
            {
              "name": "messageType",
              "value": "4"
            }
          ]
        },
        {
          "type": "ConfirmationCallback",
          "output": [
            {
              "name": "prompt",
              "value": ""
            },
            {
              "name": "messageType",
              "value": 0
            },
            {
              "name": "options",
              "value": [
                "Login using verification code"
              ]
            },
            {
              "name": "optionType",
              "value": -1
            },
            {
              "name": "defaultOption",
              "value": 0
            }
          ],
          "input": [
            {
              "name": "IDToken3",
              "value": 0
            }
          ]
        }
      ]
    }).then(res => {
      console.log(res)
      setvalue(res)
    }).catch(err => {
      console.log(err)
    })
  }
  const [value, setvalue]=useState('')
  const downloadQRCode = () => {

    // Generate download with use canvas and stream
    const canvas = document.getElementById("qr-gen");
    const pngUrl = canvas
      .toDataURL("image/png")
      .replace("image/png", "image/octet-stream");
    let downloadLink = document.createElement("a");
    downloadLink.href = pngUrl;
    downloadLink.download = `${'collabkare'}.png`;
    document.body.appendChild(downloadLink);
    downloadLink.click();
    document.body.removeChild(downloadLink);
  };
  const [val, setVal] = React.useState({
    otp: "",
    showOtp: false,
    password: "",
  });
  const handleChangeOtp = (prop) => (event) => {
    setVal({ ...val, [prop]: event.target.value });
  };

  const handleClickShowOtp = () => {
    setVal({ ...val, showOtp: !val.showOtp });
  };

  const handleMouseDownOtp = (event) => {
    event.preventDefault();
  };
  const [next, setnext] = useState(false)
  const handleNext = () => {
    setnext(true)
  }
  const handleotpsubmit = (e) => {
    if (!val.password) return
    e.preventDefault();
    axios.post(`https://account-api.collabkare.com/${cookies.get('domain')}/authenticate`, {
      "authId": value.data.authId,
      "template": "",
      "stage": "AuthenticatorOATH7",
      "header": "ForgeRock Authenticator (OATH)",
      "callbacks": [
        {
          "type": "NameCallback",
          "output": [
            {
              "name": "prompt",
              "value": "Enter verification code:"
            }
          ],
          "input": [
            {
              "name": "IDToken1",
              "value": val.password
            }
          ]
        },
        {
          "type": "ConfirmationCallback",
          "output": [
            {
              "name": "prompt",
              "value": ""
            },
            {
              "name": "messageType",
              "value": 0
            },
            {
              "name": "options",
              "value": [
                "Submit",
                "Skip this step"
              ]
            },
            {
              "name": "optionType",
              "value": -1
            },
            {
              "name": "defaultOption",
              "value": 0
            }
          ],
          "input": [
            {
              "name": "IDToken2",
              "value": 0
            }
          ]
        }
      ]

    })
      .then(res => {
        console.log(res)
        handle(res)
      })
      .catch(err => {

        console.log(err)
      });
      setVal({
        otp: '',
        showOtp: false,
        password: '',
    })
  }
  const handle = (res) => {
    axios.post(`https://account-api.collabkare.com/${cookies.get('domain')}/authenticate`, {
      "authId": res.data.authId,
      "template": "",
      "stage": "AuthenticatorOATH8",
      "header": "ForgeRock Authenticator (OATH) Recovery Codes",
      "callbacks": [
        {
          "type": "TextOutputCallback",
          "output": [
            {
              "name": "message",
              "value": "xWsl6Ttpmj"
            },
            {
              "name": "messageType",
              "value": "0"
            }
          ]
        },
        {
          "type": "TextOutputCallback",
          "output": [
            {
              "name": "message",
              "value": "t8jcr6tTjl"
            },
            {
              "name": "messageType",
              "value": "0"
            }
          ]
        },
        {
          "type": "TextOutputCallback",
          "output": [
            {
              "name": "message",
              "value": "dmrMfcUl3u"
            },
            {
              "name": "messageType",
              "value": "0"
            }
          ]
        },
        {
          "type": "TextOutputCallback",
          "output": [
            {
              "name": "message",
              "value": "V9OrX4fkhn"
            },
            {
              "name": "messageType",
              "value": "0"
            }
          ]
        },
        {
          "type": "TextOutputCallback",
          "output": [
            {
              "name": "message",
              "value": "xZlaHMWhoQ"
            },
            {
              "name": "messageType",
              "value": "0"
            }
          ]
        },
        {
          "type": "TextOutputCallback",
          "output": [
            {
              "name": "message",
              "value": "WkfsntroXe"
            },
            {
              "name": "messageType",
              "value": "0"
            }
          ]
        },
        {
          "type": "TextOutputCallback",
          "output": [
            {
              "name": "message",
              "value": "xXzwbJlJgy"
            },
            {
              "name": "messageType",
              "value": "0"
            }
          ]
        },
        {
          "type": "TextOutputCallback",
          "output": [
            {
              "name": "message",
              "value": "fV5elsZr23"
            },
            {
              "name": "messageType",
              "value": "0"
            }
          ]
        },
        {
          "type": "TextOutputCallback",
          "output": [
            {
              "name": "message",
              "value": "0rsHz10Xdk"
            },
            {
              "name": "messageType",
              "value": "0"
            }
          ]
        },
        {
          "type": "TextOutputCallback",
          "output": [
            {
              "name": "message",
              "value": "htwLdaQ011"
            },
            {
              "name": "messageType",
              "value": "0"
            }
          ]
        },
        {
          "type": "ConfirmationCallback",
          "output": [
            {
              "name": "prompt",
              "value": ""
            },
            {
              "name": "messageType",
              "value": 0
            },
            {
              "name": "options",
              "value": [
                "Continue"
              ]
            },
            {
              "name": "optionType",
              "value": -1
            },
            {
              "name": "defaultOption",
              "value": 0
            }
          ],
          "input": [
            {
              "name": "IDToken11",
              "value": "0"
            }
          ]
        }
      ]
    }).then(res => {
      setTimeout(() => {
        console.log(res)
        const cookies = new Cookies();
        //console.log('iPlanetDirectoryPro:'+ res.data.tokenId)
        cookies.set('iPlanetDirectoryPro', res.data.tokenId, {
          domain: getTopLevelDomain(),
          path: '/',
          expires: new Date((new Date()).getTime() + 3 * 60000)
        });



        var encodedKey2 = decodeURIComponent(window.location.href, "UTF-8");
        var uu = new URL(encodedKey2)
        let params = new URLSearchParams(uu.search.slice(1));
        params.set('realm', cookies.get('domain'));
        let aa = params.toString()
        var dd = decodeURIComponent(aa, "UTF-8");
        var e = dd.split("?")
        var d = uu.searchParams.get("goto");
        var c = encodedKey2.includes("manager");
        var f = uu.searchParams.get("redirect_uri");
        var g = d == null ? null : d.split("?")


        {
          f !== null && c && res.data.successUrl == '/am/console' ?
            window.location = f :
            f !== null && !c && g !== null && res.data.successUrl == "/am/console" ?
              window.location = g[0] + "?" + e[1]
              : res.data.successUrl == '/am/console' ? window.location = '/home' : ''

        }



      }, 2500)
    }).catch(err => {

      console.log(err)
    });
  }
  return (
    <div className={classes.paper}>
      {!next ?
        qrValue !== "" ?
          <div>
            {/* <p>Download Google Authenticator App and scan QR Code</p> */}
            <QRCode
              id="qr-gen"
              value={qrValue}
              size={250}
              level={"H"}
              includeMargin={true}
            />
            <p>

              <Button variant='outlined' type="button" onClick={downloadQRCode}>
                Download QR Code
        </Button>&nbsp;
        <Button variant='outlined' onClick={handleNext}>Next</Button>
            </p></div> : <p>loading QR code</p> :
        <div>
          <h3 style={{ textAlign: 'center', color: 'black' }}><b>2 step verification</b>
            {/* <CloseIcon onClick={handlesignback} style={{ float: 'right', backgroundColor: '#d5d5d5', borderRadius: '20px' }} /> */}
          </h3>
          {/* <Divider /> */}
          <p style={{ textAlign: 'center', color: 'rgba(0, 0, 0, 0.68)', fontSize: '12px' }}>The extra step shows that it's really you trying to sign in</p>
          <br />
          <div style={{ display: 'flex' }}>
            <img style={{ height: '45px', marginTop: '11px' }} src="../../../../src/assets/images/send-OTP-message.png"></img>&nbsp;&nbsp;
      <p style={{ textAlign: '-webkit-left', color: 'rgba(0, 0, 0, 0.68)', fontSize: '15px' }}> Please Enter the conformation code you see on your Google Authenticator app</p>
          </div>
          {/* <br /> */}

          <Grid container spacing={2}>
            <Grid item xs={12} sm={12}>

              <form onSubmit={handleotpsubmit}>
                <FormControl className={clsx(classes.margin, classes.withoutLabel, classes.textField)} variant="outlined" >
                  <InputLabel htmlFor="standard-adornment-password" >6-digit code</InputLabel>
                  {/* style={{ transform: 'translate(14px, 13px) scale(1)',zIndex: '1',pointerEvents: 'none'}} */}
                  <OutlinedInput

                    /// id="outlined-adornment-password"
                    type={val.showOtp ? 'text' : 'password'}
                    value={val.password}
                    onChange={handleChangeOtp('password')}


                    //  margin="dense"
                    name="password"
                    autoComplete="on"
                    // error={filedError.optvalue !== ""}
                    // helperText={filedError.optvalue !== "" ? `${filedError.optvalue}` : ""}
                    required

                    endAdornment={
                      <InputAdornment position="end">
                        <IconButton
                          // aria-label="toggle password visibility"
                          onClick={handleClickShowOtp}
                          onMouseDown={handleMouseDownOtp}
                          edge="end"
                        >
                          {val.showOtp ? <Visibility /> : <VisibilityOff />}
                        </IconButton>
                      </InputAdornment>
                    }
                    labelWidth={85}
                  />
                </FormControl>
                <br /><br />

                <Button variant='outlined' type='submit'
                  style={{ width: '95%', backgroundColor: '#007AFF', color: 'white', float: 'right', margin: 'auto' }}>sign in </Button>
              </form>
            </Grid>


            <Grid item xs={12} sm={12}>
              <br />
              <div>
                {/* <Button style={{ color: '#007aff' }} onClick={handleoptback}>use another method</Button>
        <Button style={{ float: 'right', color: '#007aff' }}>resend</Button> */}

              </div>
            </Grid>
          </Grid>
        </div>}
    </div>
  );
}
export default QrCode;
export function getTopLevelDomain() {
  if (window.location.hostname === 'localhost')
    return 'localhost';

  let domains = window.location.hostname.split('.');
  return `${domains[domains.length - 2]}.${domains[domains.length - 1]}`;
};
