import React, { useState, useEffect } from 'react';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Link from '@material-ui/core/Link';
import { Divider } from '@material-ui/core';
import Container from '@material-ui/core/Container';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';
import ArrowBackIosIcon from '@material-ui/icons/ArrowBackIos';
import CloseIcon from '@material-ui/icons/Close';
import Radio from '@material-ui/core/Radio';
import IconButton from '@material-ui/core/IconButton';
import Visibility from '@material-ui/icons/Visibility';
import VisibilityOff from '@material-ui/icons/VisibilityOff';
import FormControl from '@material-ui/core/FormControl';
import OutlinedInput from '@material-ui/core/OutlinedInput';
import InputLabel from '@material-ui/core/InputLabel';
import InputAdornment from '@material-ui/core/InputAdornment';
import clsx from 'clsx';
import loginc from "./login.css";
import Cookies from 'universal-cookie';
import { loginsubmit, loginotp, mfalogin, skiplogin, emaildata } from "../../redux/action-creators/login"
import { connect } from 'react-redux';
import Snackbar from '@material-ui/core/Snackbar';
import Axios from 'axios';
import MuiAlert from '@material-ui/lab/Alert';
import OtpTimer from 'otp-timer'
//import { FRAuth, Config } from '@forgerock/javascript-sdk';
const emailRegex = RegExp(/^[^@]+@[^@]+\.[^@]+$/)
function Alert(props) {
    return <MuiAlert elevation={6} variant="filled" {...props} />;
}
import QRCode from './qrcode'
let hideEmail = function (email) {
    return email.replace(/(.{2})(.*)(?=@)/,
        function (gp1, gp2, gp3) {
            for (let i = 0; i < gp3.length; i++) {
                gp2 += "*";
            } return gp2;
        });
};
const Login = (props) => {
    const useStyles = makeStyles(theme => ({
        root: {
            height: '100vh',
        },
        image: {
            backgroundImage: 'url(../../../../src/assets/images/LogIn-BG-02.svg)',
            backgroundRepeat: 'no-repeat',
            backgroundColor: "#fafafa",
            backgroundSize: 'cover',
            height: '100vh',
            backgroundPositionX: "46%",
            backgroundPositionY: "100%",
            backgroundSize: "100%",
            backgroundPosition: 'center'




        },
        center: {
            display: 'block',
            marginLeft: 'auto',
            marginRight: 'auto',
            marginTop: '60px'
        },
        margin: {
            margin: theme.spacing(0),
            //margin: 'auto'
        },
        withoutLabel: {
            marginTop: theme.spacing(3),
        },
        textField: {
            width: '100%',
            //margin: 'auto'
        },

        paper: {
            backgroundColor: "white",
            marginTop: theme.spacing(10),
            marginBottom: theme.spacing(3),
            padding: theme.spacing(4),
            border: "1px solid #ebebeb",
            borderRadius: 22,
            [theme.breakpoints.up(650 + theme.spacing(3) * 2)]: {
                marginTop: theme.spacing(5),
                marginBottom: theme.spacing(3),
                padding: theme.spacing(4),
                borderRadius: 5,
                border: "1px solid #ebebeb",
            }
        },
        layout: {
            width: "auto",
            marginLeft: theme.spacing(2),
            marginRight: theme.spacing(2),
            [theme.breakpoints.up(400 + theme.spacing(2) * 2)]: {
                width: 400,
                marginLeft: "auto",
                marginRight: "auto",
                padding: '0'
            }
        },

        form: {
            width: '100%', // Fix IE 11 issue.
            marginTop: theme.spacing(1),
        },
        submit: {
            margin: theme.spacing(3, 0, 2),
        },
    }));


    const initialFormState = { id: null, email: '', password: '', otp: '', showPassword: false, showOtp: false, optvalue: '' }
    const [user, setUser] = useState(initialFormState);
    const [show, setShow] = useState(true)
    const [shows, setShows] = useState(false);
    const [opt, setOPt] = useState(false);

    const [values, setValues] = React.useState({

        password: '',
        showPassword: false,

    });



    const handleClickShowPassword = () => {
        setValues({ ...values, showPassword: !values.showPassword });
    };

    const handleMouseDownPassword = (event) => {
        event.preventDefault();
    };

    const [val, setVal] = React.useState({
        otp: "",
        showOtp: false,
        password: "",
    });
    const handleChangeOtp = (prop) => (event) => {
        setVal({ ...val, [prop]: event.target.value });
    };

    const handleClickShowOtp = () => {
        setVal({ ...val, showOtp: !val.showOtp });
    };

    const handleMouseDownOtp = (event) => {
        event.preventDefault();
    };

    const handleInputChange = event => {
        //setUser({ ...user, [props]: event.target });

        const { name, value } = event.target
        const lengthValidate = value.length > 0 && value.length < 3
        switch (name) {
            case "email":
                formErrors.email = lengthValidate
                    ? "required"
                    : ""
                ///formErrors.email = emailRegex.test(value) ? "" : "Email id is invalid"
                break
            case "password":
                formErrors.password = lengthValidate
                    ? "Minimum 3 characaters required"
                    : ""
                break

            default:
                break
        }
        setFieldError({
            ...formErrors

        })
        formErrors.email.length !== 0 ? setIsError(true)
            : formErrors.password.length !== 0 ? setIsError(true) : setIsError(false)

        setUser({ ...user, [name]: value })



    }
    const [filedError, setFieldError] = useState({
        ...user
    })
    const formErrors = { ...filedError }
    const [isError, setIsError] = useState(false)
    const isEmpty = user.email.length > 0 && user.password.length > 0
    const handlesign = () => {
        // setShow(false);
        // setOPt(true)
        // setShows(true)
        setreg(false)
        setShow(false);
        //setShows(false);
        setOPt(true)
        setShows(false)
    }
    const handlesignback = () => {
        setShow(true);
        setShows(false);
        setOPt(false);
    }
    const [resopt, setresopt] = useState([])
    const handleopt = (e) => {
        e.preventDefault();
        const cookies = new Cookies();
        Axios.post(`${'https://account-api.collabkare.com/'}${cookies.get('domain')}/authenticate`, {
            "authId": datas.authId,
            "callbacks": [
                {
                    "type": "ChoiceCallback",
                    "output": [
                        {
                            "name": "prompt",
                            "value": "Please Choose a MFA Of Your Choice"
                        },
                        {
                            "name": "choices",
                            "value": [
                                "EMAIL",
                                "TOTP"
                            ]
                        },
                        {
                            "name": "defaultChoice",
                            "value": 0
                        }
                    ],
                    "input": [
                        {
                            "name": "IDToken1",
                            "value": value
                        }
                    ]
                }
            ]

        }


        ).then(res => {
            console.log(res)
            setresopt(res.data)
        })
            .catch(err => {
                seter(err.response.data.message)
                setOpen(true);
                console.log(err)
            });





        //props.mfalogin(value)
        setShow(false);
        //setShows(false);
        setOPt(true)
        setShows(false)
        // } else if (value == 2) {
        //     props.skiplogin(value)
        // }
        // else if (value == 1) {

        //    // props.mfalogin(value)

        //     setShow(false);
        //     //setShows(false);
        //     setOPt(true)
        //     setShows(false)
        // }
        // else {

        // }

    }
    const handleoptback = () => {
        setShow(false);
        setShows(true);
        setOPt(false)
    }

    const [value, setValue] = React.useState('0');
    const handleChange = event => {
        setValue(event.target.value);
    };

    const [otps, setotps] = useState(false)
    const [datas, setdatas] = useState([])
    const handlesubmit = (e) => {
        if (!user.email || !user.password) return

        e.preventDefault();
        console.log(user)
        let email = JSON.stringify(user.email);
        let domain = email.substring(email.lastIndexOf("@") + 1);



        const cookies = new Cookies();
        cookies.set('domain', domain.split('.')[0], {
            domain: getTopLevelDomain(),
            path: '/',
            // expires: new Date((new Date()).getTime() + 3 * 60000)
        });
        cookies.set('email', user.email);
        sessionStorage && sessionStorage.clear && sessionStorage.clear();
        localStorage && localStorage.clear && localStorage.clear();
        // localStorage.setItem("domain", domain.slice(0, -5));
        //props.loginsubmit(user)

        Axios.post(`${'https://account-api.collabkare.com/'}${cookies.get('domain')}/authenticate`

            , {
                "username": user.email,
                "password": user.password,
            }

        ).then(res => {
            setdatas(res.data)
            console.log(res)
            if (JSON.stringify(res.data.callbacks[0].output[0].value).includes('One Time Password')) {
                setotps(true)
                setShow(false)
            } else {
                setShow(false)
                setShows(true)
            }



            // dispatch(login(res.data, userdata))

        })
            .catch(err => {
                seter(err.response.data.message)
                setOpen(true);
                console.log(err)

            });





        setUser(initialFormState)

    }
    const [loc, setloc] = React.useState()
    const handleotpsubmit = (e) => {
        if (!val.password) return
        e.preventDefault();
        const cookies = new Cookies();

        Axios.post(`${'https://account-api.collabkare.com/'}${cookies.get('domain')}/authenticate`, {

            "authId": datas.authId,

            "callbacks": [
                {
                    "type": "PasswordCallback",
                    "output": [
                        {
                            "name": "prompt",
                            "value": "One Time Password"
                        }
                    ],
                    "input": [
                        {
                            "name": "IDToken1",
                            "value": val.password
                        }
                    ]
                }
            ]


        })
            .then(res => {
                const cookies = new Cookies();
                cookies.set('iPlanetDirectoryPro', res.data.tokenId, {
                    domain: getTopLevelDomain(),
                    path: '/',
                    expires: new Date((new Date()).getTime() + 30 * 60000)
                });
                var encodedKey2 = decodeURIComponent(window.location.href, "UTF-8");
                var uu = new URL(encodedKey2)
                let params = new URLSearchParams(uu.search.slice(1));
                params.set('realm', cookies.get('domain'));
                let aa = params.toString()
                var dd = decodeURIComponent(aa, "UTF-8");
                var e = dd.split("?")
                var d = uu.searchParams.get("goto");
                var c = encodedKey2.includes("manager");
                var f = uu.searchParams.get("redirect_uri");
                var g = d == null ? null : d.split("?")

                {
                    f !== null && c && res.data.successUrl == '/am/console' ?
                        window.location = f :
                        f !== null && !c && g !== null && res.data.successUrl == "/am/console" ?
                            window.location = g[0] + "?" + e[1]
                            : res.data.successUrl == '/am/console' ? window.location = '/home' : ''

                }
            })
            .catch(err => {
                seter(err.response.data.message)
                setOpen(true);
                console.log(err)
            });


        setVal({
            otp: '',
            showOtp: false,
            password: '',
        })
        console.log(val)
    }
    const handleotp = (e) => {
        if (!val.password) return
        e.preventDefault();
        console.log(val)
        const cookies = new Cookies();
        if (value == 0) {

            Axios.post(`${'https://account-api.collabkare.com/'}${cookies.get('domain')}/authenticate`, {

                "authId": resopt.authId,

                "callbacks": [
                    {
                        "type": "PasswordCallback",
                        "output": [
                            {
                                "name": "prompt",
                                "value": "One Time Password"
                            }
                        ],
                        "input": [
                            {
                                "name": "IDToken1",
                                "value": val.password
                            }
                        ]
                    }
                ]


            })
                .then(res => {
                    const cookies = new Cookies();
                    cookies.set('iPlanetDirectoryPro', res.data.tokenId, {
                        domain: getTopLevelDomain(),
                        path: '/',
                        expires: new Date((new Date()).getTime() + 30 * 60000)
                    });
                    var encodedKey2 = decodeURIComponent(window.location.href, "UTF-8");
                    var uu = new URL(encodedKey2)
                    let params = new URLSearchParams(uu.search.slice(1));
                    params.set('realm', cookies.get('domain'));
                    let aa = params.toString()
                    var dd = decodeURIComponent(aa, "UTF-8");
                    var e = dd.split("?")
                    var d = uu.searchParams.get("goto");
                    var c = encodedKey2.includes("manager");
                    var f = uu.searchParams.get("redirect_uri");
                    var g = d == null ? null : d.split("?")

                    {
                        f !== null && c && res.data.successUrl == '/am/console' ?
                            window.location = f :
                            f !== null && !c && g !== null && res.data.successUrl == "/am/console" ?
                                window.location = g[0] + "?" + e[1]
                                : res.data.successUrl == '/am/console' ? window.location = '/home' : ''

                    }
                })
                .catch(err => {
                    seter(err.response.data.message)
                    setOpen(true);
                    console.log(err)
                });

        } else if (value == 1) {
            Axios.post(`${'https://account-api.collabkare.com/'}${cookies.get('domain')}/authenticate`, {
                "authId": resopt.authId,
                "template": "",
                "stage": "AuthenticatorOATH7",
                "header": "ForgeRock Authenticator (OATH)",
                "callbacks": [
                    {
                        "type": "NameCallback",
                        "output": [
                            {
                                "name": "prompt",
                                "value": "Enter verification code:"
                            }
                        ],
                        "input": [
                            {
                                "name": "IDToken1",
                                "value": val.password
                            }
                        ]
                    },
                    {
                        "type": "ConfirmationCallback",
                        "output": [
                            {
                                "name": "prompt",
                                "value": ""
                            },
                            {
                                "name": "messageType",
                                "value": 0
                            },
                            {
                                "name": "options",
                                "value": [
                                    "Submit",
                                    "Skip this step"
                                ]
                            },
                            {
                                "name": "optionType",
                                "value": -1
                            },
                            {
                                "name": "defaultOption",
                                "value": 0
                            }
                        ],
                        "input": [
                            {
                                "name": "IDToken2",
                                "value": 0
                            }
                        ]
                    }
                ]


            })
                .then(res => {
                    const cookies = new Cookies();
                    cookies.set('iPlanetDirectoryPro', res.data.tokenId, {
                        domain: getTopLevelDomain(),
                        path: '/',
                        expires: new Date((new Date()).getTime() + 30 * 60000)
                    });
                    var encodedKey2 = decodeURIComponent(window.location.href, "UTF-8");
                    var uu = new URL(encodedKey2)
                    let params = new URLSearchParams(uu.search.slice(1));
                    params.set('realm', cookies.get('domain'));
                    let aa = params.toString()
                    var dd = decodeURIComponent(aa, "UTF-8");
                    var e = dd.split("?")
                    var d = uu.searchParams.get("goto");
                    var c = encodedKey2.includes("manager");
                    var f = uu.searchParams.get("redirect_uri");
                    var g = d == null ? null : d.split("?")

                    {
                        f !== null && c && res.data.successUrl == '/am/console' ?
                            window.location = f :
                            f !== null && !c && g !== null && res.data.successUrl == "/am/console" ?
                                window.location = g[0] + "?" + e[1]
                                : res.data.successUrl == '/am/console' ? window.location = '/home' : ''

                    }
                })
                .catch(err => {
                    seter(err.response.data.message)
                    setOpen(true);
                    console.log(err)
                });
        }

        setVal({
            otp: '',
            showOtp: false,
            password: '',
        })
        console.log(val)
    }
    const classes = useStyles();

    useEffect(() => {
        if (props.error) {
            setOpen(true);
            setShow(true);
            setShows(false);
            setOPt(false)
        }
    }, [props.error])
    useEffect(() => {
        if (props.userloading) {
            setShow(false);
            setShows(true);
            setOPt(false)

        }
    }, [props.userloading])
    useEffect(() => {
        setloc(localStorage.getItem('userdata'))
    }, [props.userdata])
    const [reg, setreg] = React.useState()
    useEffect(() => {
        setreg(localStorage.getItem('register'))
    }, [props.mfa])
    const [open, setOpen] = React.useState(false);
    const [er, seter] = React.useState('')
    const handleClick = () => {
        setOpen(true);
    };

    const handleClose = (event, reason) => {
        if (reason === 'clickaway') {
            return;
        }

        setOpen(false);
    };

    const cookies = new Cookies();
    useEffect(() => {
        if (cookies.get('iPlanetDirectoryPro') !== undefined) {
            Axios({
                method: 'post',
                url: 'https://auth.collabkare.com/am/json/sessions?_action=getSessionInfo',
                //'https://account-api.collabkare.com/sessioninfo',

                headers: {
                    'Content-Type': 'application/json',
                    'token': cookies.get('iPlanetDirectoryPro')
                },
            }).then(res => {
                {
                    res.status == 200 ?
                        window.location = '/home' : ''

                }
            })
                .catch(err => {
                    console.log(err)
                });
        }

    }, [])
    const handleotps = () => {
        setShow(true),
            setotps(false)
    }
    // ///timer
    // const { initialMinute = 5, initialSeconds = 0 } = props;
    // const [minutes, setMinutes] = useState(initialMinute);
    // const [seconds, setSeconds] = useState(initialSeconds);
    // useEffect(()=>{
    //     let myInterval = setInterval(() => {
    //             if (seconds > 0) {
    //                 setSeconds(seconds - 1);
    //             }
    //             if (seconds === 0) {
    //                 if (minutes === 0) {
    //                     clearInterval(myInterval)
    //                 } else {
    //                     setMinutes(minutes - 1);
    //                     setSeconds(59);
    //                 }
    //             } 
    //         }, 1000)
    //         return ()=> {
    //             clearInterval(myInterval);
    //           };
    //     });
    return (
        <div className={classes.image}>
            <Snackbar open={open} onClose={handleClose}
                anchorOrigin={{
                    vertical: 'top', horizontal: 'center'
                }}
            >
                <Alert elevation={0} onClose={handleClose} severity="error">
                    {er}
                </Alert>


            </Snackbar>

            {/* <p style={{ color: 'red' }}>{props.error}</p> */}

            <Grid container component="main" >
                <CssBaseline />
                <Grid item xs={12} sm={12} md={12}  >
                    <img className={classes.center} src='../../../../src/assets/images/Asset-1.svg' style={{width:'300px'}}/>
                    {/* <h3 style={{ textAlign: 'center', color: '#3682f7' }}>Take control of your health and well-being with CollabKare.</h3> */}
                    <div className={classes.layout}>
                        <Paper className={classes.paper}>

                            {show ? <div>

                                <form noValidate onSubmit={handlesubmit}>
                                    <Grid container spacing={2} noValidate>
                                        <Grid item xs={12} sm={12}>

                                            <TextField
                                                variant='outlined'
                                                margin="dense"
                                                fullWidth
                                                label="Email"
                                                name="email"
                                                placeholder="Your email address"
                                                type="email"
                                                value={user.email}
                                                onChange={handleInputChange}
                                                error={filedError.email !== ""}
                                                helperText={filedError.email !== "" ? `${filedError.email}` : ""}
                                                required

                                            />
                                            {sessionStorage.setItem("domain", user.email)}
                                        </Grid>
                                        <Grid item xs={12} sm={12}>


                                            <FormControl className={clsx(classes.margin, classes.withoutLabel, classes.textField)} variant="outlined" >
                                                <InputLabel htmlFor="standard-adornment-password" style={{ margin: '-6px -1px' }} >Password *</InputLabel>
                                                {/* style={{ transform: 'translate(14px, 13px) scale(1)',zIndex: '1',pointerEvents: 'none'}} */}
                                                <OutlinedInput

                                                    id="outlined-adornment-password"
                                                    type={values.showPassword ? 'text' : 'password'}
                                                    value={user.password}
                                                    onChange={handleInputChange}
                                                    margin="dense"
                                                    name="password"
                                                    autoComplete="on"
                                                    error={filedError.password !== ""}
                                                    //helperText={filedError.password !== "" ? `${filedError.password}` : ""}
                                                    required
                                                    endAdornment={
                                                        <InputAdornment position="end">
                                                            <IconButton
                                                                aria-label="toggle password visibility"
                                                                onClick={handleClickShowPassword}
                                                                onMouseDown={handleMouseDownPassword}
                                                                edge="end"
                                                            >
                                                                {values.showPassword ? <Visibility /> : <VisibilityOff />}
                                                            </IconButton>
                                                        </InputAdornment>
                                                    }
                                                    labelWidth={70}
                                                />
                                            </FormControl>



                                        </Grid>
                                    </Grid>
                                    <br />
                                    <Button
                                        variant='outlined'
                                        type="submit"

                                        disabled={!isEmpty || isError}
                                        // onClick={handlesign}
                                        style={{ width: '100%', backgroundColor: '#007AFF', color: 'white' }}
                                    >
                                        Sign in</Button>

                                    <br />
                                </form>
                                <Divider />
                                <br />
                                <Grid container>
                                    <Grid item xs>
                                        <Link href="/signup" style={{ color: '#007AFF' }}>
                                            SignUp
                                        </Link>
                                    </Grid>
                                    <Grid>

                                        <Link href="/forgotpassword" variant="body2" style={{ color: '#007AFF', float: 'right' }}>
                                            {"Forgot Username or Password?"}
                                        </Link>
                                    </Grid>
                                </Grid>

                            </div> : ''}

                            {otps ? <div >

                                <h3 style={{ textAlign: 'center', color: 'black' }}><b>2 step verification</b>
                                    <CloseIcon onClick={handleotps} style={{ float: 'right', backgroundColor: '#d5d5d5', borderRadius: '20px' }} /></h3>

                                <p style={{ textAlign: 'center', color: 'rgba(0, 0, 0, 0.68)', fontSize: '12px' }}>The extra step shows that it's really you trying to sign in</p>
                                <br />
                                <div style={{ display: 'flex' }}>
                                    <img style={{ height: '45px', marginTop: '11px' }} src="../../../../src/assets/images/send-OTP-message.png"></img>&nbsp;&nbsp;
                                    <p style={{ textAlign: '-webkit-left', color: 'rgba(0, 0, 0, 0.68)', fontSize: '15px' }}> Please Enter the conformation code you see on your email</p>
                                </div>


                                <Grid container spacing={2}>
                                    <Grid item xs={12} sm={12}>

                                        <form onSubmit={handleotpsubmit}>
                                            <FormControl className={clsx(classes.margin, classes.withoutLabel, classes.textField)} variant="outlined" >
                                                <InputLabel htmlFor="standard-adornment-password" >6-digit code</InputLabel>

                                                <OutlinedInput


                                                   // type={val.showOtp ? 'text' : 'password'}
                                                    value={val.password}
                                                    onChange={handleChangeOtp('password')}


                                                    //  margin="dense"
                                                    name="password"
                                                    autoComplete="on"
                                                    error={filedError.optvalue !== ""}
                                                    // helperText={filedError.optvalue !== "" ? `${filedError.optvalue}` : ""}
                                                    required

                                                    // endAdornment={
                                                    //     <InputAdornment position="end">
                                                    //         <IconButton
                                                    //             // aria-label="toggle password visibility"
                                                    //             onClick={handleClickShowOtp}
                                                    //             onMouseDown={handleMouseDownOtp}
                                                    //             edge="end"
                                                    //         >
                                                    //             {val.showOtp ? <Visibility /> : <VisibilityOff />}
                                                    //         </IconButton>
                                                    //     </InputAdornment>
                                                    // }
                                                    labelWidth={85}
                                                />
                                            </FormControl>
                                            <br /><br />

                                            <Button variant='outlined' type='submit'
                                                style={{ width: '100%', backgroundColor: '#007AFF', color: 'white', float: 'right', margin: 'auto' }}>sign in </Button>
                                        </form>
                                    </Grid>


                                    <Grid item xs={12} sm={12}>
                                        <br />
                                        <div>
                                            <Button style={{ color: '#007aff' }} onClick={handleoptback}>use another method</Button>
                                            {/* <Button style={{ float: 'right', color: '#007aff' }}><div>
                                                {minutes === 0 && seconds === 0
                                                    ? null
                                                    : <h1> {minutes}:{seconds < 10 ? `0${seconds}` : seconds}</h1>
                                                }
                                            </div></Button> */}
                                            <div style={{ float: 'right', color: '#007aff', marginTop: '3px' }}>
                                                <OtpTimer ButtonText={<span className={loginc.buttoncss}>back</span>} seconds={59} minutes={4} text={''} resend={handleotps} />
                                            </div>

                                        </div>
                                    </Grid>
                                </Grid>

                            </div> : ""}



                            {shows ? <div>
                                <form noValidate onSubmit={handleopt}>
                                    <Grid container spacing={6}>
                                        <Grid item xs={12} sm={12}>
                                            <div style={{ display: 'flex', marginTop: '-10px' }}>
                                                <ArrowBackIosIcon onClick={handlesignback} style={{ color: '#3682f7' }} />
                                                <h3 style={{ textAlign: 'center', color: 'black', margin: 'auto' }}>Choose a way to verify</h3>

                                            </div>

                                            <Divider style={{ marginTop: '10px' }} />
                                        </Grid>



                                        <div style={{ display: 'flex' }}>
                                            <Radio color="primary"
                                                checked={value === '0'}
                                                value="0"
                                                onChange={handleChange}
                                            />&nbsp;&nbsp;

                                    <img style={{ width: '40px', height: '40px', margin: 'auto' }} src="../../../../src/assets/images/send-OTP-email.png"></img>&nbsp;&nbsp;&nbsp;
                                    <span style={{ fontSize: '12px', margin: 'auto', color: '#565656' }}> Get a verification code at <b>{hideEmail(cookies.get('email'))}</b> </span>

                                        </div>
                                        <br />
                                        <div style={{ display: 'flex' }}>
                                            <Radio color="primary"
                                                checked={value === '1'}
                                                value="1"
                                                onChange={handleChange}
                                            />&nbsp;&nbsp;
                                    <img style={{ width: '42px', height: '40px', margin: 'auto' }} src="../../../../src/assets/images/send-OTP-Google-app.png"></img>&nbsp;&nbsp;&nbsp;
                                    <span style={{ fontSize: '12px', margin: 'auto', color: '#565656' }}> Get a verification code from<b> Google Authentication</b> app</span>

                                        </div>



                                        {/* {console.log(loc)}
                                        {loc == 'true' ?
                                            <div style={{ display: 'flex' }}>


                                                <Radio color="primary"
                                                    checked={value === '2'}
                                                    value="2"
                                                    onChange={handleChange}

                                                />&nbsp;&nbsp;&nbsp;&nbsp;

                                            <span style={{ fontSize: '14px', margin: 'auto', color: '#565656' }}> skip</span>


                                            </div> : <div><br /><br /><br /></div>} */}

                                        <br />
                                        <br />
                                        <div style={{ marginTop: '15%' }}></div>

                                        <Button variant='outlined' type='submit'
                                            //onClick={handleopt}
                                            style={{ width: '90%', backgroundColor: '#007AFF', color: 'white', float: 'right', margin: 'auto' }}>
                                            {value == 3 ? 'sign in' : 'next'} </Button>

                                    </Grid>
                                </form>
                            </div> : ''}
                            {opt ? <div >
                                {reg == 'true' ? !props.mfaloading ? <QRCode mfadata={props.mfa} click={handlesign} /> : <p>loading</p> : <div>
                                    <h3 style={{ textAlign: 'center', color: 'black' }}><b>2 step verification</b>
                                        <CloseIcon onClick={handlesignback} style={{ float: 'right', backgroundColor: '#d5d5d5', borderRadius: '20px' }} /></h3>

                                    <p style={{ textAlign: 'center', color: 'rgba(0, 0, 0, 0.68)', fontSize: '12px' }}>The extra step shows that it's really you trying to sign in</p>
                                    <br />
                                    <div style={{ display: 'flex' }}>
                                        <img style={{ height: '45px', marginTop: '11px' }} src="../../../../src/assets/images/send-OTP-message.png"></img>&nbsp;&nbsp;
                                    <p style={{ textAlign: '-webkit-left', color: 'rgba(0, 0, 0, 0.68)', fontSize: '15px' }}>
                                            {value == 0 ? 'Please Enter the conformation code you see on your email' :
                                                "Please Enter the conformation code you see on your Google Authenticator app"}
                                        </p>
                                    </div>


                                    <Grid container spacing={2}>
                                        <Grid item xs={12} sm={12}>

                                            <form onSubmit={handleotp}>
                                                <FormControl className={clsx(classes.margin, classes.withoutLabel, classes.textField)} variant="outlined" >
                                                    <InputLabel htmlFor="standard-adornment-password" >6-digit code</InputLabel>

                                                    <OutlinedInput


                                                       // type={val.showOtp ? 'text' : 'password'}
                                                        value={val.password}
                                                        onChange={handleChangeOtp('password')}


                                                        //  margin="dense"
                                                        name="password"
                                                        autoComplete="on"
                                                        error={filedError.optvalue !== ""}
                                                        // helperText={filedError.optvalue !== "" ? `${filedError.optvalue}` : ""}
                                                        required

                                                        // endAdornment={
                                                        //     <InputAdornment position="end">
                                                        //         <IconButton
                                                        //             // aria-label="toggle password visibility"
                                                        //             onClick={handleClickShowOtp}
                                                        //             onMouseDown={handleMouseDownOtp}
                                                        //             edge="end"
                                                        //         >
                                                        //             {val.showOtp ? <Visibility /> : <VisibilityOff />}
                                                        //         </IconButton>
                                                        //     </InputAdornment>
                                                        // }
                                                        labelWidth={85}
                                                    />
                                                </FormControl>
                                                <br /><br />

                                                <Button variant='outlined' type='submit'
                                                    style={{ width: '100%', backgroundColor: '#007AFF', color: 'white', float: 'right', margin: 'auto' }}>sign in </Button>
                                            </form>
                                        </Grid>


                                        <Grid item xs={12} sm={12}>
                                            <br />
                                            <div>
                                                <Button style={{ color: '#007aff' }} onClick={handleoptback}>use another method</Button>
                                                {value == 1 ? <Button style={{ float: 'right', color: '#007aff' }}>resend</Button> : ''}
                                                {value == 0 ?
                                                    <div style={{ float: 'right', color: '#007aff', marginTop: '3px' }}>
                                                        <OtpTimer ButtonText={<span className={loginc.buttoncss}>back</span>} seconds={59} minutes={4} text={''} resend={handleotps} />
                                                    </div> : ''}
                                            </div>
                                        </Grid>
                                    </Grid>
                                </div>}
                            </div> : ""}

                        </Paper>
                    </div>
                </Grid>

            </Grid>

        </div>
    )
}
const mapStateToProps = state => {
    return {
        error: state.login.error,
        userloading: state.login.userloading,
        userdata: state.login.userdata,
        mfa: state.login.mfa,
        mfaloading: state.login.mfaloading,
        user: state.login.user

    }
}
const mapDispatchToProps = dispatch => {
    return {
        loginsubmit: data => {
            dispatch(loginsubmit(data));
        },
        loginotp: data => {
            dispatch(loginotp(data));
        },
        mfalogin: data => {
            dispatch(mfalogin(data));
        },
        skiplogin: data => {
            dispatch(skiplogin(data))
        },
        emaildata: data => {
            dispatch(emaildata(data))
        }

    };
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Login);

export function getTopLevelDomain() {
    if (window.location.hostname === 'localhost')
        return 'localhost';

    let domains = window.location.hostname.split('.');
    return `${domains[domains.length - 2]}.${domains[domains.length - 1]}`;
};














