import React, { useEffect, useState } from 'react';
import Cookies from 'universal-cookie';
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import CssBaseline from '@material-ui/core/CssBaseline';
import CardActionArea from '@material-ui/core/CardActionArea';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import Link from '@material-ui/core/Link';
import IconButton from '@material-ui/core/IconButton';
import AccountCircle from '@material-ui/icons/AccountCircle';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import Popover from '@material-ui/core/Popover';
import AppBar from '@material-ui/core/AppBar';
import CameraIcon from '@material-ui/icons/PhotoCamera';
import Toolbar from '@material-ui/core/Toolbar';
import { connect } from 'react-redux';
import Avatar from 'react-avatar';
import { Divider } from '@material-ui/core';
import {Redirect} from 'react-router-dom';
const useStyles = makeStyles((theme) => ({
    icon: {
        marginRight: theme.spacing(2),
        width: '200px'
    },
    heroContent: {
        backgroundColor: theme.palette.background.paper,
        padding: theme.spacing(8, 0, 6),
    },
    heroButtons: {
        marginTop: theme.spacing(4),
    },
    cardGrid: {
        paddingTop: theme.spacing(8),
        paddingBottom: theme.spacing(8),
    },
    card: {
        height: '100%',
        display: 'flex',
        flexDirection: 'column',
    },
    cardMedia: {
        // paddingTop: '56.25%', // 16:9
        // width:'400px'
        width: '200px',
        height: '200px',
        marginLeft: 'auto',
        marginRight: 'auto'
    },
    cardContent: {
        flexGrow: 1,
        textAlign: 'center'
    },
    footer: {
        backgroundColor: theme.palette.background.paper,
        padding: theme.spacing(6),

    },
    link: {
        margin: theme.spacing(1, 1.5),
    },
}));

const Redirects = (props) => {
    const classes = useStyles();
    const manager = () => {
        window.location = "https://manager.collabkare.com/"
    }
    const portal = () => {
        window.location = "https://portal.collabkare.com/"
    }
    const cookies = new Cookies();
    if (cookies.get('iPlanetDirectoryPro') == undefined) {
        return <Redirect to="/login"/>
    }

    return (
        <React.Fragment>
            <CssBaseline />

            <main>
                <Container className={classes.cardGrid} maxWidth="md">
                    {/* End hero unit */}
                    <Grid
                        container
                        spacing={3}
                        direction="row"
                        alignItems="center"
                        justify="center"
                    // style={{ minHeight: '100vh' }}
                    >

                        <Grid item xs={12} sm={6} >
                            <Card className={classes.card}>
                                {/* <CardActionArea> */}
                                <br/>
                                <CardMedia
                                    className={classes.cardMedia}
                                    image="../../../../../src/assets/images/Asset-2.svg"
                                    title="Image title"
                                />
                                <CardContent className={classes.cardContent}>
                                    <Typography gutterBottom variant="h5" component="h2" >
                                        Manager
                                    </Typography>

                                </CardContent>
                                <CardActions>
                                    <Button fullWidth variant="outlined" className={classes.link} onClick={manager}>
                                        Open
                                      </Button>

                                </CardActions>

                            </Card>
                        </Grid>
                        <Grid item xs={12} sm={6} >
                            <Card className={classes.card}>
                                <br/>
                                <CardMedia
                                    className={classes.cardMedia}
                                    image="../../../../../src/assets/images/Asset-2.svg"
                                    title="Image title"
                                />
                                <CardContent className={classes.cardContent}>
                                    <Typography gutterBottom variant="h5" component="h2">
                                        Portal
                                   </Typography>

                                </CardContent>
                                <CardActions>
                                    <Button fullWidth variant="outlined" className={classes.link} onClick={portal}>
                                        Open
                                </Button>

                                </CardActions>
                            </Card>
                        </Grid>

                    </Grid>
                </Container>
            </main>

        </React.Fragment >

    )
}
export default Redirects;





