import React, { useState, Fragment } from "react"
import Stepper from "@material-ui/core/Stepper"
import Step from "@material-ui/core/Step"
import StepLabel from "@material-ui/core/StepLabel"
import FirstStep from "./FirstSteps"
import Confirm from "./Confirms"
import Success from "./Succes"
import MobileStepper from '@material-ui/core/MobileStepper';
import Button from '@material-ui/core/Button';
import { MuiThemeProvider, createMuiTheme } from "@material-ui/core/styles";
const emailRegex = RegExp(/^[^@]+@[^@]+\.[^@]+$/)
const phoneRegex = RegExp(/^[(]{0,1}[0-9]{3}[)]{0,1}[-\s\.]{0,1}[0-9]{3}[-\s\.]{0,1}[0-9]{4}$/)
const faxRegex = RegExp(/^\+?[0-9]{6,}$/)
const zipRegex = RegExp(/^[0-9]{5}(?:-[0-9]{4})?$/)
const passwordRegex = RegExp(/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,}$/)
const einvalidate = RegExp(/^([07][1-7]|1[0-6]|2[0-7]|[35][0-9]|[468][0-8]|9[0-589])-?\d{7}$/)
// Step titles
import Ein from 'ein-validator/dist/src/index'
import { makeStyles, useTheme } from '@material-ui/core/styles';
//import MobileStepper from '@material-ui/core/MobileStepper';
// from '@material-ui/core/Button';
import KeyboardArrowLeft from '@material-ui/icons/KeyboardArrowLeft';
import KeyboardArrowRight from '@material-ui/icons/KeyboardArrowRight';
import { connect } from 'react-redux';
import {signup} from '../.././redux/action-creators/signup'
const labels = ["About Me", "confirmation"]
const useStyles = makeStyles({
    root: {
        // maxWidth: 400,
        display: "flex",
        flexDirection: 'column'
        //flexGrow: 1,
    },
});
const AccountStandard = (props) => {
    const [steps, setSteps] = useState(0)
    const [fields, setFields] = useState({
        firstName: "",
        lastName: "",
        email: "",
        phone: "",
        password: "",

        confiramatiomemail: "",

        passwordconfirm: ""
    })
   

    // Copy fields as they all have the same name
    const [filedError, setFieldError] = useState({
        ...fields
    })

    const [isError, setIsError] = useState(false)

    // Proceed to next step
    const handleNext = () => setSteps(steps + 1)

    // Go back to prev step
    const handleBack = () => setSteps(steps - 1)
    const formErrors = { ...filedError }
    // Handle fields change
    const handleChange = input => ({ target: { value } }) => {


        // Handle errors

        const lengthValidate = value.length > 0 && value.length < 3
        const lengthValidates = value.length > 0 && value.length < 2

        switch (input) {
            case "firstName":
                formErrors.firstName = lengthValidate
                    ? "Minimum 3 characaters required"
                    : ""

                break

            case "lastName":
                formErrors.lastName = lengthValidate
                    ? "Minimum 3 characaters required"
                    : ""
                break



            case "email":

                formErrors.email = /^([\w-.]+@(?!gmail\.com)(?!yahoo\.com)(?!hotmail\.com)(?!outlook\.com)(?!gmx\.com)(?!aol\.com)(?!yandex\.com)(?!lycos\.com)(?!zoho\.com)(?!mail\.com)(?!tutanota\.com)(?!protonmail\.com)(?!icloud\.com)([\w-]+.)+[\w-]{2,4})?$/.test(value) ? "" : "please use your company email address to register"
                fields.confiramatiomemail == value ? formErrors.confiramatiomemail = "" : formErrors.confiramatiomemail = "email are not matching"
                break
            case "confiramatiomemail":
                fields.email == value ? formErrors.confiramatiomemail = "" : formErrors.confiramatiomemail = "email are not matching"
                //formErrors.confiramatiomemail = emailRegex.test(value) ? "" : "email are not matching"
                break
            case "password":
                formErrors.password = passwordRegex.test(value) ? "" : "Minimum eight characters, at least one uppercase letter, one lowercase letter, one number and one special character"
                fields.passwordconfirm == value ? formErrors.passwordconfirm = "" : formErrors.passwordconfirm = "passwords are not matching"
                break

            case "passwordconfirm":
                fields.password == value ? formErrors.passwordconfirm = "" : formErrors.passwordconfirm = "passwords are not matching"
                break

            case "phone":
                formErrors.phone = phoneRegex.test(value)
                    ? ""
                    : "Must be with formats: 123-456-7890 (123)456-7890 1234567890 123.456.7890."
                break


            default:
                break
        }



        // set errors hook




        setFieldError({
            ...formErrors

        })
        formErrors.firstName.length !== 0 ? setIsError(true)
            : formErrors.lastName.length !== 0 ? setIsError(true)
                : formErrors.email.length !== 0 ? setIsError(true)
                    : formErrors.phone.length !== 0 ? setIsError(true)

                        : formErrors.confiramatiomemail.length !== 0 ? setIsError(true)

                            : formErrors.password.length !== 0 ? setIsError(true)
                                : formErrors.passwordconfirm.length !== 0 ? setIsError(true)
                                    : setIsError(false)


        // Object.values(formErrors).forEach(error =>
        //   error.length > 0 ? setIsError(true) : setIsError(false)
        // )
        // Set values to the fields
        setFields({
            ...fields,
            [input]: value
        })

    }


    // React.useEffect(() => {
    //   Object.values(formErrors).forEach(error =>
    //     error.length > 0 ? setIsError(true) : setIsError(false)
    //   )
    // }, [])
    const handleSteps = step => {
        switch (step) {
            case 0:
                return (
                    <FirstStep
                        handleNext={handleNext}
                        handleChange={handleChange}
                        values={fields}
                        isError={isError}
                        filedError={filedError}
                    />
                )

            case 1:
                return (
                    <Confirm
                        handleNext={handleNext}
                        handleBack={handleBack}
                        values={fields}
                        props={props}
                    />
                )
            default:
                break
        }
    }
    const theme = createMuiTheme({
        overrides: {

            MuiMobileStepper: {
                root: {
                    display: 'flex',
                    padding: '0px',
                    background: 'none',
                    flexDirection: 'column'
                },
                dot: {
                    width: '12px',
                    height: '12px',
                    margin: '2px 2px',
                    borderRadius: '50%',
                    backgroundColor: 'rgb(187, 184, 184)'
                },
                dotActive: {
                    backgroundColor: '#007AFF'
                }
            },


        }
    }
    )
    // Handle components
    const classes = useStyles();
    return (
        <Fragment>
            {steps === labels.length ? (

                <Success
                    values={fields}
                />
            ) : (
                    <Fragment >
                        <MuiThemeProvider theme={theme}>
                          
                            <MobileStepper

                                variant="dots"
                                steps={2}
                              
                                activeStep={steps}
                                style={{ color: 'white', }}
                                className={classes.root}
                               
                                position="bottom"
                           
                            >
                                {labels.map(label => (
                                    <Step key={label}>
                                        <StepLabel>{label}</StepLabel>
                                    </Step>
                                ))}
                               
                            </MobileStepper>
                            {handleSteps(steps)}
                        </MuiThemeProvider>
                    </Fragment>
                )}
            <div>


            </div>
        </Fragment>
    )
}
const mapStateToProps = state => {
    return {
       
    }
  }
  const mapDispatchToProps = dispatch => {
    return {
      signup: data => {
            dispatch(signup(data));
        },
       
    };
  };
  
  export default connect(
    mapStateToProps,
    mapDispatchToProps
  )(AccountStandard);
  



