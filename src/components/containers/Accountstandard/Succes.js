import React, { Fragment } from "react"
import Typography from "@material-ui/core/Typography"
import TextareaAutosize from '@material-ui/core/TextareaAutosize';
import { Button, Grid } from "@material-ui/core";
import Link from '@material-ui/core/Link';
import Axios from "axios"
import CssBaseline from '@material-ui/core/CssBaseline';
import Paper from '@material-ui/core/Paper';
import Box from '@material-ui/core/Box';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import { makeStyles } from '@material-ui/core/styles';
const useStyles = makeStyles((theme) => ({
  root: {
    height: '100vh',
  },
  image: {
    backgroundImage: 'url(../../../../src/assets/images/Check-your-email.svg)',
    backgroundRepeat: 'no-repeat',
    backgroundColor:
      theme.palette.type === 'light' ? theme.palette.grey[50] : theme.palette.grey[900],
    backgroundSize: 'cover',
    backgroundPosition: 'center',
  },

  paper: {
    margin: theme.spacing(2, 2),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',

  },

  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing(1),
  },

}));
const Succes = ()=> {

 
  const classes = useStyles();
  return (
    <Grid container component="main" className={classes.root}>
      <CssBaseline />
      <Grid item xs={false} sm={4} md={6} className={classes.image} />
      <Grid item xs={12} sm={8} md={6} component={Paper} elevation={6} style={{ boxShadow: 'none' }} >
        <div className={classes.paper} style={{ boxShadow: 'none' }} >
          {/* <Avatar className={classes.avatar}> */}
          <img src="../../../../src/assets/images/Collabkare-Icon.svg" style={{ display: 'flex', flexDirection: 'column', textAlign: 'center' }}></img>

          {/* </Avatar> */}
          <br />
          <img src="../../../../src/assets/images/send-OTP-email.png" style={{ display: 'flex', flexDirection: 'column', textAlign: 'center' }}></img>
          <form noValidate>






            <Typography variant="h5" align="center">
              Check your inbox.
      </Typography>
            <p style={{ textAlign: 'center' }}>You have been send an activation email</p>
            <p style={{ textAlign: 'center' }}>in order to start using your account, you need to confirm your email address.</p>
            <br />
            <div style={{ textAlign: 'center' }}>
              <p style={{ color: '#007AFF', marginRight: '183px' }}>Tell us more (if you want)</p>


              <TextareaAutosize
                style={{ height: '120px', width: '350px', resize: 'vertical' }}
                aria-label="minimum height"
                rowsMin={5}
                placeholder="" />
              <br />
              <Button variant='contained' style={{ backgroundColor: '#007AFF', color: 'white', marginRight: '260px' }}>Submit</Button>

            </div>
          </form>
          <Link href="/login" variant="body2" style={{ color: '#007AFF', marginLeft: '90px' }}>
               Click here for Sign in
              </Link>
        </div>
      </Grid>
    </Grid>
  )
}

export default Succes








