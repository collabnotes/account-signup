import React, { Fragment } from "react"
import List from "@material-ui/core/List"
import ListItem from "@material-ui/core/ListItem"
import ListItemText from "@material-ui/core/ListItemText"
import Divider from "@material-ui/core/Divider"
import Button from "@material-ui/core/Button"
import { Grid } from "@material-ui/core"
import Axios from "axios"
import CssBaseline from '@material-ui/core/CssBaseline';
import Paper from '@material-ui/core/Paper';
import Box from '@material-ui/core/Box';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import { makeStyles } from '@material-ui/core/styles';
import { MuiThemeProvider, createMuiTheme } from "@material-ui/core/styles";
import Typography from '@material-ui/core/Typography';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import CloseIcon from '@material-ui/icons/Close';
import Checkbox from '@material-ui/core/Checkbox';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Link from '@material-ui/core/Link';
// Destructure props

const useStyles = makeStyles((theme) => ({
  root: {
    height: '100vh',
  },
  image: {
    backgroundImage: 'url(../../../../src/assets/images/Createaccount.svg)',
    backgroundRepeat: 'no-repeat',
    backgroundColor:
      theme.palette.type === 'light' ? theme.palette.grey[50] : theme.palette.grey[900],
    backgroundSize: 'cover',
    backgroundPosition: 'center',
  },

  paper: {
    margin: theme.spacing(2, 2),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },

  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing(1),
  },
  but: {
    marginLeft: '132px'
  },
  buts: {
    marginLeft: '132px',
    color: 'white',
    backgroundColor: '#007AFF'

  }

}));
const Confirm = ({
  props,
  handleNext,
  handleBack,
  values: { firstName, lastName, email, phone, city, zipcode, address, companyphone, confiramatiomemail, userid, organisationname, organisationtax, Tax, state, password ,address2}
}) => {
  const theme = createMuiTheme({
    overrides: {
      MuiButton: {
        outlined: {
          border: '1px solid #007AFF'
        },
      },
      MuiTypography: {
        body1: {
          fontSize: '0.875rem',
          color: 'rgba(0, 0, 0, 0.54)'
        },
        body2: {
          fontSize: '1rem',
          color: 'black !important'
        },
        colorTextSecondary: {

          color: 'black'

        }
      },
    },
  }
  )
  let emails = JSON.stringify(email);
  let domain = emails.substring(emails.lastIndexOf("@") + 1);
organisationname=domain.split('.')[0]
  const [value,setValue]=React.useState({firstName, lastName, email, phone, city, zipcode, address, companyphone, confiramatiomemail,  organisationname, organisationtax, Tax, state, password,address2})
  const classes = useStyles();
  
  
  
  const [open, setOpen] = React.useState(false);
  const [states, setState] = React.useState({
    check: false,

  });
  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };
  const handleChanges = name => event => {
    setState({ ...states, [name]: event.target.checked });
    //setOpen(true);
  };
  const classa = (!check ? classes.but : classes.buts)
  const { check } = states;
  const handle=(e)=>{
      e.preventDefault();
     console.log(value)
     props.signupsubmit(value)
      handleNext()
  }
  return (
    <MuiThemeProvider theme={theme}>
      
      <Grid container component="main" className={classes.root}>
        <CssBaseline />
        <Grid item xs={false} sm={4} md={6} className={classes.image} />
        <Grid item xs={12} sm={8} md={6} component={Paper} elevation={6} style={{ boxShadow: 'none' }} >
          <div className={classes.paper} style={{ boxShadow: 'none' }} >
            {/* <Avatar className={classes.avatar}> */}
            <img src="../../../../src/assets/images/Asset-1.svg" style={{ display: 'flex', flexDirection: 'column', textAlign: 'center',width:'300px' }}></img>

            {/* </Avatar> */}

            <h4 style={{ textAlign: 'center', color: '#146e87' }}>User info</h4>
            <form className={classes.form} noValidate onSubmit={
              handle
            }>
              <Grid container spacing={2}>
                <List style={{ paddingLeft: '100px' }}>
                  <Grid container spacing={2}>

                    <Grid item xs={12} sm={6}>

                      <ListItem>
                        <ListItemText primary="First Name" secondary={firstName} />
                      </ListItem>
                    </Grid>

                    <Grid item xs={12} sm={6}>
                      <ListItem>
                        <ListItemText primary="Last Name" secondary={lastName} />
                      </ListItem>
                    </Grid>

                    <Grid item xs={12} sm={6}>
                      <ListItem>
                        <ListItemText primary="Email Address" secondary={email} />
                      </ListItem>
                    </Grid>
                    <Grid item xs={12} sm={6}>
                      <ListItem>
                        <ListItemText
                          primary="Personal phone"
                          secondary={phone.length > 0 ? phone : "Not Provided"}
                        />
                      </ListItem>
                    </Grid>
                    <Grid item xs={12} sm={12}>
                      <h4 style={{ textAlign: 'center', marginRight: '50px', color: '#146e87' }}>Organisation info</h4></Grid>
                    <Grid item xs={12} sm={6}>
                      <ListItem>
                        <ListItemText primary="Organisation Name" secondary={organisationname} />
                      </ListItem>

                    </Grid>
                    <Grid item xs={12} sm={6}>
                      <ListItem>
                        <ListItemText primary="Company phone" secondary={companyphone} />
                      </ListItem>
                    </Grid>

                    <Grid item xs={12} sm={6}>
                      <ListItem>
                        <ListItemText primary="Address" secondary={address} />
                      </ListItem>
                    </Grid>
                    {/* <Grid item xs={12} sm={6}>
            <ListItem>
              <ListItemText

                primary="Userid," secondary={userid}
              />
            </ListItem>
          </Grid> */}



                    {/* <Grid item xs={12} sm={6}>
            <ListItem>
              <ListItemText primary="Company phone" secondary={companyphone} />
            </ListItem>
          </Grid> */}



                    <Grid item xs={12} sm={6}>
                      <ListItem>
                        <ListItemText primary="City" secondary={city} />
                      </ListItem>
                    </Grid>
                    <Grid item xs={12} sm={6}>
                      <ListItem>
                        <ListItemText primary="State" secondary={state} />
                      </ListItem>
                    </Grid>
                    <Grid item xs={12} sm={6}>
                      <ListItem>
                        <ListItemText
                          primary="Zipcode" secondary={zipcode}
                        />
                      </ListItem>
                    </Grid>
                  </Grid>
                </List>
                <FormControlLabel
                style={{marginLeft:'90px'}}
              control={<Checkbox value="remember" style={{ color: '#007AFF' }} checked={check} onChange={handleChanges('check')} />}
              label={<h5>I agree to the  <Link onClick={handleClickOpen} href="#" style={{ color: '#007AFF' }}>
                terms and conditions
          </Link></h5>}

            />
            <br /><br />

           
            <Dialog
              open={open}
              onClose={handleClose}
              aria-labelledby="alert-dialog-title"
              aria-describedby="alert-dialog-description"
            >
              <DialogTitle style={{ color: 'white', backgroundColor: '#007AFF' }} id="alert-dialog-title">{"Terms and conditions"}<CloseIcon onClick={handleClose} style={{ float: 'right' }} /></DialogTitle>

              <DialogContent dividers>
                <Typography gutterBottom>

                  1. General
                  These Terms and Conditions ("Agreement")
                  governs the use of the services ("Service")
                  that are made available by Website.com Solutions Inc.
                  ("Website.com", "we" or "us"). These Terms and Conditions represent
                  the whole agreement and understanding between Website.com and the individual or entity who subscribes to our
                  service ("Subscriber" or "you").
                  
                  PLEASE READ THIS AGREEMENT CAREFULLY. By submitting your
                  application and by your use of the Service, you agree to comply
                  with all of the terms and conditions set out in this Agreement.
                  Website.com may terminate your account at any time, with or without notice,
                  for conduct that is in breach of this Agreement, for conduct that Website.com believes is harmful to its business,
                  or for conduct where the use of the Service is harmful to any other party.
                  
                  Website.com may, in its sole discretion, change or modify this Agreement at any time, with or without notice.
                  Such changes or modifications shall be made effective for all Subscribers upon posting of the modified Agreement to
                  this web address (URL): http://www.website.comm/terms-and-conditions/. You are responsible to read this document from time
                  to time to ensure that your use of the Service remains in compliance with this Agreement.

                </Typography>
                <Typography gutterBottom>

                </Typography>
              </DialogContent>
              <DialogActions>

                <Button onClick={handleClose} style={{ color: 'white', backgroundColor: '#007AFF' }} autoFocus>
                  Agree
          </Button>
              </DialogActions>
            </Dialog>
                <br /><br />
                <Grid container spacing={2}>
                  <Grid item xs={6} sm={6}>

                    <Button
                      variant="outlined"
                      // color="#007AFF"
                      style={{ color: '#007AFF' }}
                      onClick={handleBack}
                      style={{ marginLeft: "90px" }}
                    >
                      Back
         </Button>
                  </Grid>
                  <Grid item xs={6} sm={6}>


                    <Button
                      style={{ marginLeft: 20 }}
                      variant="contained"
                      //color="primary"
                      type='submit'
                      className={classa}
                      disabled={!check} 
                     // style={{ backgroundColor: '#007AFF', color: 'white', marginLeft: "65px" }}
                     // onClick={handleNext}
                    >
                      Create Account
        </Button>

                  </Grid>
                </Grid>
              </Grid>
            </form>
          </div>
        </Grid>
      </Grid>
    </MuiThemeProvider>
  )
}
const mapStateToProps = state => {
  return {
      error: state.login.error,
      userloading:state.login.userloading
  }
}
const mapDispatchToProps = dispatch => {
  return {
    signupsubmit: data => {
          dispatch(signupsubmit(data));
      },
     
  };
};

export default Confirm
 








