import React, { Fragment } from "react"
import Grid from "@material-ui/core/Grid"
import TextField from "@material-ui/core/TextField"
import Button from "@material-ui/core/Button"
import { Divider } from "@material-ui/core"
import { Link } from "react-router-dom"
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormHelperText from '@material-ui/core/FormHelperText';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import CssBaseline from '@material-ui/core/CssBaseline';
import Paper from '@material-ui/core/Paper';
import Box from '@material-ui/core/Box';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import { makeStyles } from '@material-ui/core/styles';
import { MuiThemeProvider, createMuiTheme } from "@material-ui/core/styles";
const useStyles = makeStyles((theme) => ({
  root: {
    height: '100vh',
  },
  image: {
    backgroundImage: 'url(../../../../src/assets/images/create-organization.svg)',
    backgroundRepeat: 'no-repeat',
    backgroundColor:
      theme.palette.type === 'light' ? theme.palette.grey[50] : theme.palette.grey[900],
    backgroundSize: 'cover',
    backgroundPosition: 'center',
  },

  paper: {
    margin: theme.spacing(2, 2),
    //display: 'flex',
    // flexDirection: 'column',
    alignItems: 'center',
  },
  paper1: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',

  },
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing(1),
  },
  formControl: {
    // margin: theme.spacing(1),
    marginTop: theme.spacing(1),
    width: '100%',
  },
  selectEmpty: {
    marginTop: theme.spacing(2),
  },
  but: {
    marginLeft: '132px'
  },
  buts: {
    marginLeft: '132px',
    color: 'white',
    backgroundColor: '#007AFF'

  }

}));
// Destructure props
const SecondStep = ({
  handleNext,
  handleBack,
  handleChange,
  //handleChangetax,
  //Tax: Tax,
  values: { city,email, zipcode, companyphone, organisationname, organisationtax, address, state, address2, Tax },
  filedError,
  isError
}) => {
  // Check if all values are not empty

  const isEmpty =
    companyphone.length > 0
    // && organisationname.length > 0 
     && organisationtax.length > 0
    && address.length > 0 && city.length > 0 && zipcode.length > 0 && state.length > 0
  const classes = useStyles();

  const theme = createMuiTheme({
    overrides: {
      MuiButton: {
        outlined: {
          border: '1px solid #007AFF'
        },
      },
    }
  }
  )
  const classa = (!isEmpty || isError ? classes.but : classes.buts)
  let emails = JSON.stringify(email);
  let domain = emails.substring(emails.lastIndexOf("@") + 1);
  organisationname=domain.split('.')[0]
  return (
    <Grid container component="main" className={classes.root}>
      <CssBaseline />
      <Grid item xs={false} sm={4} md={6} className={classes.image} />
      <Grid item xs={12} sm={8} md={6} component={Paper} elevation={6} style={{ boxShadow: 'none' }} >
        <div className={classes.paper} style={{ boxShadow: 'none' }} >
          <div className={classes.paper1}>
            <img src="../../../../src/assets/images/Asset-1.svg" style={{ display: 'flex', flexDirection: 'column', textAlign: 'center',width:'300px' }}></img>

          </div>
          <h4 style={{ textAlign: 'center', color: '#146e87' }}>Organisation setup</h4>

          <form className={classes.form} noValidate>
            <Grid container spacing={2}>
              <Grid item xs={12} sm={12}>
                <TextField
                  fullWidth
                  variant='outlined'
                  label='Organisation Name'
                  name="organisationname"
                  placeholder="Enter the name of company you work for"
                  defaultValue={organisationname}
                  //value={organisationname}
                  disabled
                  onChange={handleChange("organisationname")}
                  margin="dense"
                  error={filedError.organisationname !== ""}
                  helperText={filedError.organisationname !== "" ? `${filedError.organisationname}` : ""}
                  required
                />
              </Grid>
              <Grid item xs={12} sm={6}>
                <TextField
                  fullWidth
                  variant='outlined'
                  label="Organisation's Tax ID"
                  name="organisationtax"
                  placeholder="Organisation Tax ID"
                  //type="email"
                  defaultValue={organisationtax}
                  onChange={handleChange("organisationtax")}
                  margin="dense"
                  error={filedError.organisationtax !== ""}
                  helperText={filedError.organisationtax !== "" ? `${filedError.organisationtax}` : ""}
                  required
                />
              </Grid>
              <Grid item xs={12} sm={6}>
                <FormControl fullWidth
                  required
                  variant="outlined"
                  defaultValue={Tax}
                  className={classes.formControl}
                  //margin="normal"
                 
                  id="demo-simple-select-outlined">
                  <InputLabel htmlFor="Tax">Tax</InputLabel>
                  <Select value={Tax}
                   id="demo-simple-select-outlined-label"
                   labelId="demo-simple-select-outlined-label"
                    onChange={handleChange("Tax")}
                    margin="dense"
                   

                  >
                    <MenuItem value={'EIN'}>EIN</MenuItem>
                    <MenuItem value={'SSN'}>SSN</MenuItem>
                  </Select>
                </FormControl>
                {/* <FormControl variant="outlined"  className={classes.formControl}>
                 <InputLabel
                  //ref={inputLabel}
                  id="demo-simple-select-outlined-label"
                 
                  >

                </InputLabel>
                <Select
                  labelId="demo-simple-select-outlined-label"
                  id="demo-simple-select-outlined"
                  margin="dense"
                  value={Tax}
                  onChange={handleChangetax}
                  //style={{width: '310px',marginTop:'8px'}}
                  //className={classes.formControl}
                //labelWidth={labelWidth}
                >
                  <MenuItem value={'EIN'}>EIN</MenuItem>
                  <MenuItem value={'SSN'}>SSN</MenuItem>
                </Select>
              </FormControl> */}
              </Grid>
              <Grid item xs={12} sm={6}>
                <TextField
                  fullWidth
                  variant='outlined'
                  label="Organisation Phone number"
                  name="companyphone"
                  required
                  placeholder="i.e: (123)456-7890"
                  defaultValue={companyphone}
                  onChange={handleChange("companyphone")}
                  margin="dense"
                  error={filedError.companyphone !== ""}
                  helperText={filedError.companyphone !== "" ? `${filedError.companyphone}` : ""}
                />
              </Grid>
              <Grid item xs={12} sm={6} ></Grid>
              <Grid item xs={12} sm={6} >
                <TextField
                  fullWidth
                  variant='outlined'
                  label="Address Line1"
                  name="address"
                  placeholder="Address Line1"
                  defaultValue={address}
                  onChange={handleChange("address")}
                  margin="dense"
                  error={filedError.address !== ""}
                  helperText={
                    filedError.address !== "" ? `${filedError.address}` : ""
                  }
                  required
                />
              </Grid>
              <Grid item xs={12} sm={6} >
                <TextField
                  fullWidth
                  variant='outlined'
                  label="Address Line2"
                  name="address2"
                  placeholder="Address Line2"
                  defaultValue={address}
                  onChange={handleChange("address2")}
                  margin="dense"
                //  error={filedError.address2 !== ""}
                // helperText={
                //   filedError.address2 !== "" ? `${filedError.address2}` : ""
                // }
                //required
                /></Grid>
              <Grid item xs={12} sm={6} >
                <TextField
                  fullWidth
                  variant='outlined'
                  label="City"
                  name="city"
                  placeholder="Enter your City"
                  defaultValue={city}
                  onChange={handleChange("city")}
                  margin="dense"
                  error={filedError.city !== ""}
                  helperText={filedError.city !== "" ? `${filedError.city}` : ""}
                  required
                />
              </Grid>
              {/* <Grid item xs={12} sm={6} ></Grid> */}
              <Grid item xs={12} sm={6}>
                <TextField
                  fullWidth
                  variant='outlined'
                  label="Zip Code"
                  name="zipcode"
                  placeholder="Enter your zipcode"
                  defaultValue={zipcode}
                  onChange={handleChange("zipcode")}
                  margin="dense"
                  error={filedError.zipcode !== ""}
                  helperText={filedError.zipcode !== "" ? `${filedError.zipcode}` : ""}
                  required
                />
              </Grid>
              {/* <Grid item xs={12} sm={6} ></Grid> */}
              <Grid item xs={12} sm={6}>
                <TextField
                  fullWidth
                  variant='outlined'
                  label="State/Province/Region"
                  name="state"
                  placeholder="Enter your state"
                  defaultValue={state}
                  onChange={handleChange("state")}
                  margin="dense"
                  error={filedError.state !== ""}
                  helperText={filedError.state !== "" ? `${filedError.state}` : ""}
                  required
                />
              </Grid>
            </Grid>
            <br /><br />
            <Grid container spacing={2}>
              <Grid item xs item xs={6} sm={6}>
                <MuiThemeProvider theme={theme}>
                  <Button
                    variant="outlined"
                    //color="default"
                    style={{ color: '#007AFF' }}
                    onClick={handleBack}
                    style={{ marginLeft: "90px" }}
                  >
                    Back
               </Button></MuiThemeProvider>
              </Grid>
              <Grid item xs item xs={6} sm={6}>


                <Button
                  variant="contained"
                   disabled={!isEmpty || isError  }
                  //color="primary"
                  // style={{ backgroundColor: '#007AFF' ,color:'white',marginLeft: "132px"}}
                  className={classa}
                  onClick={handleNext}
                >
                  next
            </Button>

              </Grid>
            </Grid>

          </form>
        </div>
      </Grid>
    </Grid>


  )
}

export default SecondStep
