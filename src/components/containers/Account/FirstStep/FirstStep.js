import React, { Fragment } from "react"
import Grid from "@material-ui/core/Grid"
import TextField from "@material-ui/core/TextField"
import Button from "@material-ui/core/Button"
import Link from '@material-ui/core/Link';
import Checkbox from '@material-ui/core/Checkbox';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import CssBaseline from '@material-ui/core/CssBaseline';
import Paper from '@material-ui/core/Paper';
import { makeStyles } from '@material-ui/core/styles';
// Destructure props
import clsx from 'clsx';
import CloseIcon from '@material-ui/icons/Close';
import IconButton from '@material-ui/core/IconButton';
import Input from '@material-ui/core/Input';

import InputLabel from '@material-ui/core/InputLabel';
import InputAdornment from '@material-ui/core/InputAdornment';

import FormControl from '@material-ui/core/FormControl';
import OutlinedInput from '@material-ui/core/OutlinedInput';
import Visibility from '@material-ui/icons/Visibility';
import VisibilityOff from '@material-ui/icons/VisibilityOff';
import PasswordStrengthBar from 'react-password-strength-bar';

import Styes from "../FirstStep/FirstStep.css"
const useStyles = makeStyles((theme) => ({
  root: {
    height: '100vh',
  },
  image: {
    backgroundImage: 'url(../../../../src/assets/images/Personal-detalis.svg)',
    backgroundRepeat: 'no-repeat',

    background: 'cover',
    backgroundPosition: 'center',
    backgroundSize: 'cover',
  },
  paper: {
    margin: theme.spacing(2, 2),
    //display: 'flex',
    // flexDirection: 'column',
    alignItems: 'center',
  },
  paper1: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',

  },
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing(1),
  },
  margin: {
    margin: theme.spacing(1),
  },

  textField: {
    width: '100%',
    marginLeft: '1px'
  },
  but: {
    marginLeft: '132px'
  },
  buts: {
    marginLeft: '132px',
    color: 'white',
    backgroundColor: '#007AFF'

  }
}));
const FirstStep = ({
  handleNext,
  handleChange,
  values: { email, phone, confiramatiomemail, password, passwordconfirm, firstName, lastName },
  filedError,
  isError
}) => {
  // Check if all values are not empty
  // const [open, setOpen] = React.useState(false);
  // const [opens, setOpens] = React.useState(false);
  // const [opend, setOpend] = React.useState(false);
 // const [value, setValue] = React.useState('no');
  
  const [values, setValues] = React.useState({
    password: '',
    showPassword: false,
  });
  // const handleChangep = (prop) => (event) => {
  //   setValues({ ...values, [prop]: event.target.value });
  // };

  const handleClickShowPassword = () => {
    setValues({ ...values, showPassword: !values.showPassword });
  };

  const handleMouseDownPassword = (event) => {
    event.preventDefault();
  };
 
  // const handleOpen = () => {
  //   setOpen(true)
  //   setOpend(true)
  //   setOpens(false)
  // }
  // const handleClose = () => {
  //   setOpen(false)
  //   setOpend(false)
  //   setOpens(false)
  // }
  // const handleOpens = () => {
  //   setOpen(true)
  //   setOpend(false)
  //   setOpens(true)
  // }
  // const handleCloses = () => {
  //   setOpen(true)
  //   setOpend(true)
  //   setOpens(false)
  // }
  // const handleChanged = event => {
  //   setValue(event.target.value);
  // };
  
  const Empty =
    email.length > 0 && confiramatiomemail.length > 0  && firstName.length > 0 && lastName.length > 0 && phone.length > 0
    // && passwordconfirm.length > 0


  const classes = useStyles();
  const classa = (!Empty || isError ? classes.but : classes.buts)
  return (
    <Grid container component="main" className={classes.root}>
      <CssBaseline />
      <Grid item xs={false} sm={4} md={6} className={classes.image} />
      <Grid item xs={12} sm={8} md={6} component={Paper} elevation={6} style={{ boxShadow: 'none' }} >
        <div className={classes.paper} style={{ boxShadow: 'none' }} >
          {/* <Avatar className={classes.avatar}> */}
          <div className={classes.paper1}>
            <img src="../../../../src/assets/images/Asset-1.svg"  style={{ display: 'flex', flexDirection: 'column',width:'300px' }}></img>
          </div>
          {/* </Avatar> */}
          <h3 style={{ textAlign: 'center' }}> YOUR JOURNEY STARTS HERE</h3>
          <p style={{ textAlign: 'center' }}>Enjoy unlimited features for free for 30days. Start your free trial today or
                <Link to="#" style={{ color: '#007AFF' }}> book a demo</Link></p>
          <h4 style={{ textAlign: 'center', color: '#146e87' }}>Create my account</h4>
          <form className={classes.form} noValidate>
            <Grid container spacing={2}>
              <Grid item xs={12} sm={6}>
                <TextField
                  fullWidth
                  variant='outlined'
                  label="First Name"
                  name="firstName"
                  placeholder="Your first name"
                  defaultValue={firstName}
                  onChange={handleChange("firstName")}
                  margin="dense"
                  error={filedError.firstName !== ""}
                  helperText={
                    filedError.firstName !== "" ? `${filedError.firstName}` : ""
                  }
                  required
                />
              </Grid>
              <Grid item xs={12} sm={6}>
                <TextField
                  fullWidth
                  variant='outlined'
                  label="Last Name"
                  name="lastName"
                  placeholder="Your last name"
                  defaultValue={lastName}
                  onChange={handleChange("lastName")}
                  margin="dense"
                  error={filedError.lastName !== ""}
                  helperText={
                    filedError.lastName !== "" ? `${filedError.lastName}` : ""
                  }
                  required
                />
              </Grid>
              <Grid item xs={12} sm={6}>
                <TextField
                  variant='outlined'
                  fullWidth
                  label="Email"
                  name="email"
                  placeholder="Your email address"
                  type="email"
                  defaultValue={email}
                  onChange={handleChange("email")}
                  margin="dense"
                  error={filedError.email !== ""}
                  helperText={filedError.email !== "" ? `${filedError.email}` : ""}
                  required
                />
              </Grid>
              <Grid item xs={12} sm={6}>
                <TextField
                  fullWidth
                  variant='outlined'
                  label="Confirm Email"
                  name="confiramatiomemail"
                  placeholder="Your email address"
                  type="email"
                  defaultValue={confiramatiomemail}
                  onChange={handleChange("confiramatiomemail")}
                  margin="dense"
                  error={filedError.confiramatiomemail !== ""}
                  helperText={filedError.confiramatiomemail !== "" ? `${filedError.confiramatiomemail}` : ""}
                  required
                />
              </Grid>
              {/* <Grid item xs={12} sm={6} >
                <FormControl className={clsx(classes.margin, classes.textField)} variant="outlined">
                  <InputLabel htmlFor="outlined-adornment-password" style={{ margin: '-6px -1px' }}>Password</InputLabel>
                  <OutlinedInput
                    id="outlined-adornment-password"
                    type={values.showPassword ? 'text' : 'password'}
                    //value={password}
                    onChange={handleChange('password')}
                    margin="dense"
                    autoComplete="password"
                    endAdornment={
                      <InputAdornment position="end">
                        <IconButton
                          aria-label="toggle password visibility"
                          onClick={handleClickShowPassword}
                          onMouseDown={handleMouseDownPassword}
                          edge="end"
                        >
                          {values.showPassword ? <Visibility /> : <VisibilityOff />}
                        </IconButton>
                      </InputAdornment>
                    }
                    labelWidth={70}
                    defaultValue={password}
                    error={filedError.password !== ""}
                  //   helperText={filedError.password !== "" ? `${filedError.password}` : ""}
                  />
                  <PasswordStrengthBar password={password} />
                  <p style={{ color: "red" }}>{filedError.password !== "" ? `${filedError.password}` : ""}</p>
                </FormControl>
               
              </Grid>
              <Grid item xs={12} sm={6} >
                <TextField
                  variant="outlined"
                  margin="dense"
                  required
                  fullWidth
                  name="passwordconfirm"
                  label="Confirm Password"
                  type="password"
                  id="passwordconfirm"
                  autoComplete="passwordconfirm"
                  defaultValue={passwordconfirm}
                  onChange={handleChange("passwordconfirm")}
                  autoComplete="current-password-confirm"
                  error={filedError.passwordconfirm !== ""}
                  helperText={filedError.passwordconfirm !== "" ? `${filedError.passwordconfirm}` : ""}
                /></Grid> */}
              <Grid item xs={12} sm={6} >
                <TextField
                  fullWidth
                  variant='outlined'
                  label="Phone number"
                  name="phone"
                  required
                  placeholder="i.e: (123)456-7890"
                  defaultValue={phone}
                  onChange={handleChange("phone")}
                  margin="dense"
                  error={filedError.phone !== ""}
                  helperText={filedError.phone !== "" ? `${filedError.phone}` : ""}
                /></Grid>
            </Grid>
            <br />
            
            <Grid container spacing={2} >
              <Grid item xs={6} sm={6}>
                <Link href="/login" variant="body2" style={{ color: '#007AFF', marginLeft: '90px' }}>
                  Sign in instead
              </Link>
              </Grid>
              <Grid item xs={6} sm={6}>
                <Button

                  variant="contained"
                  disabled={!Empty || isError}
                  // color="primary"
                  //style={{ marginLeft:'132px' }}
                  className={classa}
                  onClick={handleNext}
                >
                  Next
              </Button>

              </Grid>
            </Grid>

          </form>
        </div>
      </Grid>
    </Grid>
    //)

  )
}

export default FirstStep












