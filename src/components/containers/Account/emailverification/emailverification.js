import React, { useState } from 'react';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import PasswordStrengthBar from 'react-password-strength-bar';
import Switch from '@material-ui/core/Switch';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import QRCode from "qrcode.react";
const emailRegex = RegExp(/^[^@]+@[^@]+\.[^@]+$/)
const passwordRegex = RegExp(/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,}$/)
import DialogActions from '@material-ui/core/DialogActions';
import Axios from 'axios';
const EmailVerification = (props) => {
    const initialForm = {
        id: null, username: '', password: "", confirmpassword: "",
    }
    const [orguser, setorgUser] = useState(initialForm)
    const classes = useStyles();
    const handleInputChange = event => {
        const { name, value } = event.target
        const lengthValidate = value.length > 0 && value.length < 3
        switch (name) {
            case "password":
                formErrors.password = passwordRegex.test(value) ? "" : "Minimum eight characters, at least one uppercase letter, one lowercase letter, one number and one special character"
                orguser.confirmpassword == value ? formErrors.confirmpassword = "" : formErrors.confirmpassword = "passwords are not matching"
                break
            case "username":
                formErrors.username = emailRegex.test(value) ? "" : "Email id is invalid"
                break
            case "confirmpassword":
                orguser.password == value ? formErrors.confirmpassword = "" : formErrors.confirmpassword = "passwords are not matching"
                break
            default:
                break
        }
        setFieldError({
            ...formErrors

        })
        //  formErrors.username.length !== 0 ? setIsError(true)
        formErrors.password.length !== 0 ? setIsError(true)
            : formErrors.confirmpassword.length !== 0 ? setIsError(true)
                : setIsError(false)

        setorgUser({ ...orguser, [name]: value })
    }
    const [filedError, setFieldError] = useState({
        ...orguser
    })
    const formErrors = { ...filedError }
    const [isError, setIsError] = useState(false)
    const isEmpty =
        // orguser.username.length > 0 &&
        orguser.password.length > 0 && orguser.confirmpassword.length > 0
    const [state, setState] = React.useState({
        checkedA: true,
        checkedB: true,
    });
    // React.useEffect(() => {
    //     let params = getQueryParams(props.location);
    //     {console.log(params.accessCode)}
    // }, [])

    const getQueryParams = (url) => {

        if (url.search) {

            let urlParams;
            let match,
                pl = /\+/g,  // Regex for replacing addition symbol with a space
                search = /([^&=]+)=?([^&]*)/g,
                decode = function (s) {
                    return decodeURIComponent(s.replace(pl, " "));
                },
                query = url.search.substring(1);

            urlParams = {};
            while (match = search.exec(query))
                urlParams[decode(match[1])] = decode(match[2]);
            return urlParams;
        }
    };
    const handleChange = (event) => {
        setState({ ...state, [event.target.name]: event.target.checked });
    };
    const [email, setemail] = useState('')
    React.useEffect(() => {
        let params = getQueryParams(props.location);
        Axios.get(`https://account-api.collabkare.com/activation/${params.accessCode}`)
            .then(function (response) {
                // handle success
                setemail(response.data.user.email)
                console.log(response.data);
            })
            .catch(function (error) {
                // handle error
                console.log(error);
            })
    }, [])
    const submit = (event) => {
        event.preventDefault()
        
        // console.log(orguser)
        let params = getQueryParams(props.location);
        Axios.post(`https://account-api.collabkare.com/activation/${params.accessCode}`, {

            email: email,
            password: orguser.confirmpassword

        })
            .then(function (response) {
                console.log(response);
                 window.location = '/login'
            })
            .catch(function (error) {
                console.log(error);

            });
        setorgUser(initialForm)
    }
    //two step verification
    // const [scan, setscan] = useState(false)
    // const initial = {
    //     codes: ""
    // }
    // const [code, setCode] = useState(initial)
    // const handlecodeChange = event => {
    //     const { name, value } = event.target
    //     const lengthValidate = value.length > 0 && value.length < 6
    //     switch (name) {
    //         case "codes":
    //             codeErrors.codes = lengthValidate
    //                 ? "Minimum 6 characaters required"
    //                 : ""
    //             break
    //         default:
    //             break
    //     }
    //     setcodeError({
    //         ...codeErrors
    //     })
    //     codeErrors.codes.length !== 0 ? seterr(true) : seterr(false)
    //     setCode({ ...code, [name]: value })
    // }
    // const [codeError, setcodeError] = useState({
    //     ...code
    // })
    // const [err, seterr] = useState(false)
    // const iserr = code.codes.length > 0
    // const codeErrors = { ...codeError }
    return (
        <Container component="main" maxWidth="xs">
            <CssBaseline />
            {/* {console.log(email)} */}
            <div className={classes.paper}>

                <Typography component="h1" variant="h5" style={{ textAlign: 'center' }}>
                    Verification
                   </Typography>
                <form className={classes.form} noValidate onSubmit={submit}>
                    <TextField
                        variant="outlined"
                        margin="normal"
                        required
                        margin='dense'
                        fullWidth
                        id="username"
                        label="User Name"
                        placeholder='User Name'
                        name="username"
                        autoComplete="username"
                        autoFocus
                        disabled
                        value={email}
                        onChange={handleInputChange}
                    // error={filedError.username !== ""}
                    // helperText={filedError.username !== "" ? `${filedError.username}` : ""}
                    />

                    <TextField
                        variant="outlined"
                        margin="normal"
                        required
                        fullWidth
                        margin='dense'
                        name="password"
                        placeholder="password"
                        label="Password"
                        type="password"
                        id="password"
                        autoComplete="current-password"
                        value={orguser.password}
                        onChange={handleInputChange}
                        error={filedError.password !== ""}
                        helperText={filedError.password !== "" ? `${filedError.password}` : ""}
                    />
                    <PasswordStrengthBar password={orguser.password} />
                    <TextField
                        variant="outlined"
                        margin="normal"
                        required
                        fullWidth
                        margin='dense'
                        placeholder='confirm Password'
                        name="confirmpassword"
                        label="confirm Password"
                        type="password"
                        id="confirmpassword"
                        autoComplete="confirm-password"
                        value={orguser.confirmpassword}
                        onChange={handleInputChange}
                        error={filedError.confirmpassword !== ""}
                        helperText={filedError.confirmpassword !== "" ? `${filedError.confirmpassword}` : ""}
                    />
                    {/* <FormControlLabel
                            control={<Switch
                                checked={state.checkedA}
                                color="primary"
                                onChange={handleChange}
                                name="checkedA" />}
                            label="Two step verification"
                        /> */}
                    <Button
                        type="submit"
                        fullWidth
                        variant="contained"
                        disabled={!isEmpty || isError}
                        color="primary"
                        className={classes.submit}
                    >
                        Save
          </Button>

                </form>
                {/* </div> :
                    <div style={{ textAlign: 'center' }}>
                        <Typography component="h1" variant="h5">
                            Enable two-factor Authentication
                   </Typography>

                        <p>Scan the QR code below with an Authentication application,Such as Google Authenticator,on your phone</p>

                        <QRCode
                            id="qr-gen"

                            value={"otpauth://totp/Collabnotes:admin@test2.com?secret=L5GZ5JYOFDYJMZQR47S4GROSCI&issuer=Collabnotes&digits=6&period=30"}
                            size={250}
                            level={"H"}
                            includeMargin={true}
                        />
                        <form className={classes.form} noValidate onSubmit={event => {
                            event.preventDefault()
                            setCode(initial)
                            console.log(code)

                        }}>
                        <TextField
                            variant="outlined"
                            margin="normal"
                            required
                            fullWidth
                            margin='dense'
                            placeholder='Enter code created by authenticator app'
                            name="codes"
                            label="6-digit code"
                            id="code"
                            autoComplete="codes"
                            value={code.codes}
                            onChange={handlecodeChange}
                            error={codeError.codes !== ""}
                            helperText={codeError.codes !== "" ? `${codeError.codes}` : ""}
                        />
                        <DialogActions>
                            <Button variant='outlined' color="primary">
                                skip
                        </Button>
                            <Button variant='outlined' type="submit" color="primary" disabled={!iserr || err}>
                                verify
                           </Button>
                        </DialogActions>
                        </form>
                    </div>} */}
            </div>

        </Container>
    )
}
export default EmailVerification;





const useStyles = makeStyles((theme) => ({
    paper: {
        marginTop: theme.spacing(10),
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
    },

    form: {
        width: '100%', // Fix IE 11 issue.
        marginTop: theme.spacing(1),
    },
    submit: {
        margin: theme.spacing(3, 0, 2),
    },
}));







