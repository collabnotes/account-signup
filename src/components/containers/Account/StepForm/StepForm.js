import React, { useState, Fragment } from "react"
import Stepper from "@material-ui/core/Stepper"
import Step from "@material-ui/core/Step"
import StepLabel from "@material-ui/core/StepLabel"
import FirstStep from "../FirstStep/FirstStep"
import SecondStep from "../SecondStep/SecondStep"
import Confirm from "../Confirm/Confirm"
import Success from "../Success/Success"
import MobileStepper from '@material-ui/core/MobileStepper';
import Button from '@material-ui/core/Button';
import { MuiThemeProvider, createMuiTheme } from "@material-ui/core/styles";
const emailRegex = RegExp(/^[^@]+@[^@]+\.[^@]+$/)
const phoneRegex = RegExp(/^\([0-9]{3}\)[0-9]{3}-[0-9]{4}$/)
//RegExp(/^\([0-9]{3}\)[0-9]{3}-[0-9]{4}$/)
//RegExp(/^[(]{0,1}[0-9]{3}[)]{0,1}[-\s\.]{0,1}[0-9]{3}[-\s\.]{0,1}[0-9]{4}$/)
const faxRegex = RegExp(/^\+?[0-9]{6,}$/)
const zipRegex = RegExp(/^[0-9]{5}(?:-[0-9]{4})?$/)
const passwordRegex = RegExp(/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,}$/)
const einvalidate = RegExp(/^([07][1-7]|1[0-6]|2[0-7]|[35][0-9]|[468][0-8]|9[0-589])-?\d{7}$/)
import Axios from "axios";
import Ein from 'ein-validator/dist/src/index'
import { makeStyles, useTheme } from '@material-ui/core/styles';
//import MobileStepper from '@material-ui/core/MobileStepper';
// from '@material-ui/core/Button';
import KeyboardArrowLeft from '@material-ui/icons/KeyboardArrowLeft';
import KeyboardArrowRight from '@material-ui/icons/KeyboardArrowRight';
import { connect } from 'react-redux';
import { signupsubmit } from '../.././../redux/action-creators/signup'
const labels = ["About Me", "Organisation info", "confirmation"]
const useStyles = makeStyles({
  root: {
    // maxWidth: 400,
    display: "flex",
    flexDirection: 'column'
    //flexGrow: 1,
  },
});
const StepForm = (props) => {
  const [steps, setSteps] = useState(0)
  const [fields, setFields] = useState({
    firstName: "",
    lastName: "",
    email: "",
    phone: "",
    password: "",
    city: "",
    zipcode: "",
    //fax: "",
    address: "",
    companyphone: "",
    confiramatiomemail: "",
    userid: "",
    organisationname: '',
    organisationtax: "",
    Tax: "EIN",
    state: "",
    address2: "",
    passwordconfirm: ""
  })

  const [Tax, SetTax] = React.useState('EIN');
  const handleChangetax = event => {
    SetTax(event.target.value);
  };

  // Copy fields as they all have the same name
  const [filedError, setFieldError] = useState({
    ...fields
  })

  const [isError, setIsError] = useState(false)

  // Proceed to next step
  const handleNext = () => setSteps(steps + 1)

  // Go back to prev step
  const handleBack = () => setSteps(steps - 1)
  const formErrors = { ...filedError }
  // Handle fields change
  const [org, setorg] = React.useState('')


  React.useEffect(() => {

    org == '' ?
      '' : getUser()

  }, [org])
  async function getUser() {
    try {
      let orgs = JSON.stringify(org);
      // let domain = orgs.substring(orgs.lastIndexOf("@") + 1);
      // let emails = JSON.stringify(email);
      let domain = orgs.substring(orgs.lastIndexOf("@") + 1);
     
      const response = await Axios.get(`https://account-api.collabkare.com/checkRealm/${domain.split('.')[0]}`);
      response.data == "It is a New Organization. " ? formErrors.email = '' : 
      response.data == "Organization is Already Existed. " ?
      formErrors.email = 'organisation already exist please click below link to sign in':formErrors.email = ''


    } catch (error) {
      console.error(error);
    }
  }

  const handleChange = input => ({ target: { value } }) => {


    // Handle errors

    const lengthValidate = value.length > 0 && value.length < 3
    const lengthValidates = value.length > 0 && value.length < 2

    switch (input) {
      case "firstName":
        formErrors.firstName = lengthValidate
          ? "Minimum 3 characaters required"
          : ""

        break
      case "address2":
        formErrors.address2 = lengthValidate
          ? "Minimum 3 characaters required"
          : ""

        break
      case "organisationname":
        formErrors.organisationname = lengthValidate
          ? "Minimum 3 characaters required"
          : ""
        break
      case "lastName":
        formErrors.lastName = lengthValidate
          ? "Minimum 3 characaters required"
          : ""
        break
      case "city":
        formErrors.city = lengthValidate
          ? "Minimum 3 characaters required"
          : ""
        break
      case "state":
        formErrors.state = lengthValidates
          ? "Minimum 2 characaters required"
          : ""
        break
      case "organisationtax":
        formErrors.organisationtax = einvalidate.test(value)
          ? ""
          : "Invalid EIN number"
        break
      case "address":
        formErrors.address = lengthValidate
          ? "Minimum 3 characaters required"
          : ""
        break
      case "userid":
        formErrors.userid = lengthValidate
          ? "user id should be 6-15 alphanumeric characters"
          : ""
        break
      case "zipcode":
        formErrors.zipcode = zipRegex.test(value) ? "" : "Must be with formats: 12345 or 12345-6789."
        break
      case "fax":
        formErrors.fax = faxRegex.test(value) ? "" : "Must be with formats: 12345."

        break
      case "email":
        formErrors.email = /^([\w-.]+@(?!gmail\.com)(?!yahoo\.com)(?!hotmail\.com)(?!outlook\.com)(?!gmx\.com)(?!aol\.com)(?!yandex\.com)(?!lycos\.com)(?!zoho\.com)(?!mail\.com)(?!tutanota\.com)(?!protonmail\.com)(?!icloud\.com)([\w-]+.)+[\w-]{2,4})?$/.test(value) ? "" : "please use your company email address to register"
        fields.confiramatiomemail == value ? formErrors.confiramatiomemail = "" : formErrors.confiramatiomemail = "email are not matching"
        // //formErrors.email = emailRegex.test(value) ? "" : "email id is invalid"
        // formErrors.email = /^([\w-.]+@(?!gmail\.com)(?!yahoo\.com)(?!hotmail\.com)(?!outlook\.com)(?!gmx\.com)(?!aol\.com)(?!yandex\.com)(?!lycos\.com)(?!zoho\.com)(?!mail\.com)(?!tutanota\.com)(?!protonmail\.com)(?!icloud\.com)([\w-]+.)+[\w-]{2,4})?$/.test(value) ? "" : "please use your company email address to register"


        // fields.confiramatiomemail == value ? formErrors.confiramatiomemail = "" : formErrors.confiramatiomemail = "email are not matching"
        break
      case "confiramatiomemail":
        fields.email == value ? formErrors.confiramatiomemail = "" : formErrors.confiramatiomemail = "email are not matching"
        //formErrors.confiramatiomemail = emailRegex.test(value) ? "" : "email are not matching"
        // fields.email == value ? formErrors.confiramatiomemail = "" : formErrors.confiramatiomemail = "email are not matching"
        setorg(fields.email)


        //formErrors.confiramatiomemail = emailRegex.test(value) ? "" : "email are not matching"
        break
      case "password":
        formErrors.password = passwordRegex.test(value) ? "" : "Minimum eight characters, at least one uppercase letter, one lowercase letter, one number and one special character"
        fields.passwordconfirm == value ? formErrors.passwordconfirm = "" : formErrors.passwordconfirm = "passwords are not matching"
        break

      case "passwordconfirm":
        fields.password == value ? formErrors.passwordconfirm = "" : formErrors.passwordconfirm = "passwords are not matching"
        break

      case "phone":
        formErrors.phone = phoneRegex.test(value)
          ? ""
          : "Must be with formats:(123)456-7890"
          // 123-456-7890 (123)456-7890 1234567890 123.456.7890."
        // 123-456-7890 

        ////// 1234567890 123.456.7890."
        break
      case "companyphone":
        formErrors.companyphone = phoneRegex.test(value)
          ? ""
          : "Must be with formats: (123)456-7890."
          //(123)456-7890"
        //123-456-7890 (123)456-7890 
        // 1234567890 123.456.7890."
        break

      default:
        break
    }



    // set errors hook




    setFieldError({
      ...formErrors

    })
    formErrors.firstName.length !== 0 ? setIsError(true)
      : formErrors.lastName.length !== 0 ? setIsError(true)
        : formErrors.email.length !== 0 ? setIsError(true)
          : formErrors.phone.length !== 0 ? setIsError(true)
            : formErrors.city.length !== 0 ? setIsError(true)
              : formErrors.zipcode.length !== 0 ? setIsError(true)
                : formErrors.address.length !== 0 ? setIsError(true)
                  : formErrors.companyphone.length !== 0 ? setIsError(true)
                    : formErrors.confiramatiomemail.length !== 0 ? setIsError(true)
                      : formErrors.userid.length !== 0 ? setIsError(true)
                        // : formErrors.organisationname.length !== 0 ? setIsError(true)
                        : formErrors.organisationtax.length !== 0 ? setIsError(true)
                          // : formErrors.Tax.length !== 0 ? setIsError(true)
                          : formErrors.state.length !== 0 ? setIsError(true)
                            //: formErrors.address2.length !== 0 ? setIsError(true)
                            //: formErrors.password.length !== 0 ? setIsError(true)
                            // : formErrors.passwordconfirm.length !== 0 ? setIsError(true)
                            : setIsError(false)


    // Object.values(formErrors).forEach(error =>
    //   error.length > 0 ? setIsError(true) : setIsError(false)
    // )
    // Set values to the fields
    setFields({
      ...fields,
      [input]: value
    })

  }


  // React.useEffect(() => {
  //   Object.values(formErrors).forEach(error =>
  //     error.length > 0 ? setIsError(true) : setIsError(false)
  //   )
  // }, [])
  const handleSteps = step => {
    switch (step) {
      case 0:
        return (
          <FirstStep
            handleNext={handleNext}
            handleChange={handleChange}
            values={fields}
            isError={isError}
            filedError={filedError}
          />
        )
      case 1:
        return (
          <SecondStep
            handleNext={handleNext}
            handleBack={handleBack}
            handleChange={handleChange}
            handleChangetax={handleChangetax}
            Tax={Tax}
            values={fields}
            isError={isError}
            filedError={filedError}
          />
        )
      case 2:
        return (
          <Confirm
            handleNext={handleNext}
            handleBack={handleBack}
            values={fields}
            props={props}
          />
        )
      default:
        break
    }
  }
  const theme = createMuiTheme({
    overrides: {

      MuiMobileStepper: {
        root: {
          display: 'flex',
          padding: '0px',
          background: 'none',
          flexDirection: 'column'
        },
        dot: {
          width: '12px',
          height: '12px',
          margin: '2px 2px',
          borderRadius: '50%',
          backgroundColor: 'rgb(187, 184, 184)'
        },
        dotActive: {
          backgroundColor: '#007AFF'
        }
      },


    }
  }
  )
  // Handle components
  const classes = useStyles();
  return (
    <Fragment>
      {steps === labels.length ? (

        <Success
          values={fields}
        />
      ) : (
          <Fragment >
            <MuiThemeProvider theme={theme}>
              {/* <Stepper */}
              <MobileStepper

                variant="dots"
                steps={3}
                //position="static"
                activeStep={steps}
                style={{ color: 'white', }}
                className={classes.root}
                // activeStep={steps}
                // style={{ paddingTop: 30, paddingBottom: 50 }}
                // alternativeLabel
                // variant="progress"
                // steps={3}
                position="bottom"
              // activeStep={steps}
              // style={{
              //   paddingTop: 10, paddingBottom: 10,width:'10px'

              // }}
              >
                {labels.map(label => (
                  <Step key={label}>
                    <StepLabel>{label}</StepLabel>
                  </Step>
                ))}
                {/* </Stepper> */}
              </MobileStepper>
              {handleSteps(steps)}
            </MuiThemeProvider>
          </Fragment>
        )}
      <div>


      </div>
    </Fragment>
  )
}
const mapStateToProps = state => {
  return {
    error: state.login.error,
    userloading: state.login.userloading
  }
}
const mapDispatchToProps = dispatch => {
  return {
    signupsubmit: data => {
      dispatch(signupsubmit(data));
    },

  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(StepForm);






//

//import React from 'react';


// const useStyles = makeStyles({
//   root: {
//     maxWidth: 400,
//     flexGrow: 1,
//   },
// });

// export default function DotsMobileStepper() {
//   const classes = useStyles();
//   const theme = useTheme();
//   const [activeStep, setActiveStep] = React.useState(0);

//   const handleNext = () => {
//     setActiveStep((prevActiveStep) => prevActiveStep + 1);
//   };

//   const handleBack = () => {
//     setActiveStep((prevActiveStep) => prevActiveStep - 1);
//   };

//   return (
//     <MobileStepper
//       variant="dots"
//       steps={6}
//       position="static"
//       activeStep={activeStep}
//       className={classes.root}
//       nextButton={
//         <Button size="small" onClick={handleNext} disabled={activeStep === 5}>
//           Next
//           {theme.direction === 'rtl' ? <KeyboardArrowLeft /> : <KeyboardArrowRight />}
//         </Button>
//       }
//       backButton={
//         <Button size="small" onClick={handleBack} disabled={activeStep === 0}>
//           {theme.direction === 'rtl' ? <KeyboardArrowRight /> : <KeyboardArrowLeft />}
//           Back
//         </Button>
//       }
//     />
//   );
// }
