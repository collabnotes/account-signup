import React from "react";
//import htmlContent from '../../../proton/index.html';
import htmlContent from '../../../../singlepageapplication/index.html'
import { defaults } from "autoprefixer";
import { makeStyles } from '@material-ui/core/styles';
const useStyles = makeStyles((theme) => ({
  respcontainer: {
   // position: 'relative',
    overflow: 'hidden',
    paddingTop: '56.25%'
  },
  respiframe: {
    position: 'absolute',
    top: '0',
    left: '0',
    width: '100%',
    height: '100%',
    border: '0'
  }
}));
const Proton = () => {
  const classes = useStyles();
  return (
    <div className={classes.respcontainer}>
      <iframe className={classes.respiframe} src="/singlepageapplication/index.html"  allow="autoplay" allowFullScreen></iframe>
    </div>
  )
}
export default Proton;
// export default class Proton extends React.Component {
//   constructor(props) {
//     super(props);
//   }
//   render() {
//     let htmlDoc = { __html: htmlContent };

//     let height = window.innerHeight;
//     let width = window.innerWidth;
//     return (
//       <div className="respcontainer">
//         <iframe className="respiframe" src="/singlepageapplication/index.html" gesture="media" allow="encrypted-media" allowFullScreen></iframe>
//       </div>
//       // <div dangerouslySetInnerHTML={htmlDoc} />

//       // <div>

//       //    <iframe src="/singlepageapplication/index.html"  />
//       //     </div>
//     );
//   }

// }