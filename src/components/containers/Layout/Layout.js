import React from 'react';
import Appbars from "./Appbar/Appbar"
import Footer from "./Footer/Footer"
import Aux from '../hoc/hoc'
import { withRouter } from "react-router-dom";
const Layout = (props) => {


    return (


        <Aux>

            {props.location.pathname !== '/login' && props.location.pathname !== '/signup'
                && props.location.pathname !== "/demo" && props.location.pathname !== "/"
                && props.location.pathname !== "/forgotpassword" && props.location.pathname !== '/verification' && <Appbars />}
            {props.children}
            {/* <Footer/> */}
        </Aux>


    );

}

export default withRouter(Layout);





