import React from 'react';
import CssBaseline from '@material-ui/core/CssBaseline';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import Link from '@material-ui/core/Link';

const Footer = () => {

    function Copyright() {
        return (
            <Typography variant="body2" color="textSecondary" 
           // align='center' 
            style={{float:'right',marginTop:'-20px',color:'rgb(0, 122, 255)'}}>
                {'Copyright © '}
                <Link color="inherit" href="https://staging.collabkare.com/">
                staging.collabkare.com
            </Link>{' '}
                {new Date().getFullYear()}
                {'.'}
            </Typography>
        );
    }
  

    const useStyles = makeStyles(theme => ({
        root: {
            display: 'flex',
            flexDirection: 'column', 
            padding: theme.spacing(2, 2),
            
            //bottom:'10px',
            backgroundColor:'#dde6ff',
    //   position:'fixed',
    //   bottom:0,
    //   width:'100%',
    //   left:0
              //  theme.palette.type === 'dark' ? theme.palette.grey[800] : theme.palette.grey[200],
        },
   
        // position: fixed;
        // left: 0;
        // bottom: 0;
        // width: 100%;
        // background-color: red;
        // color: white;
        // text-align: center;

    }));
    const classes = useStyles();
    return (
        <div className={classes.root}>
            
            <footer className={classes.footer} style={{color:'rgb(0, 122, 255)'}}>
                {/* <Container > */}
                    <Typography variant="body1" >COLLABKARE</Typography>
                    <Copyright  />
                {/* </Container> */}
            </footer>
        </div>
    );

}

export default Footer;




