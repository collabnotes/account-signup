import React, { useEffect, useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import Avatar from 'react-avatar';
import Popover from '@material-ui/core/Popover';
import AccountCircle from '@material-ui/icons/AccountCircle';
import { Divider } from '@material-ui/core';
import { connect } from 'react-redux';
import Cookies from 'universal-cookie';
import { profildata } from "../../../redux/action-creators/profileinfo";
import { BrowserRouter as Router, Route, Link, Redirect } from "react-router-dom";
import { Grid } from '@material-ui/core'
import Appcss from "./Appbar.css"
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import Axios from 'axios';
//import muiThemeable from "material-ui/styles/muiThemeable";
import { MuiThemeProvider, createMuiTheme } from "@material-ui/core/styles";
import MuiExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import { withStyles } from '@material-ui/core/styles';

import Accordion from '@material-ui/core/Accordion';
import { AccordionSummary, AccordionDetails } from '@material-ui/core'
const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1,
  },
  menuButton: {
    marginRight: theme.spacing(2),
  },
  title: {
    flexGrow: 1,
  },
  icon: {
    marginRight: theme.spacing(2),
    width: '200px'
  },
}));
const ExpansionPanelSummary = withStyles({
  root: {

    display: 'flex',
    padding: '0 24px 0 24px',
    height: '40px',
    minHeight: '40px',
    '&$expanded': {
      minHeight: '36px',
    }
  },
  expandIcon: {
    padding: 0
  },
  content: {
    '&$expanded': {
      padding: 0
    },
  },

  expanded: { backgroundColor: '#f1f1f1' },
})(AccordionSummary);
// const theme = createMuiTheme({
//   MuiExpansionPanelSummary: {
//     root: { minHeight: '25px' }
//   },
//   Mui: {
//     expanded: { minHeight: '25px' }
//   }
// },

//);
const Toobars = (props) => {
  const classes = useStyles();
  useEffect(() => {
    props.profildata()
  }, [])
  const [profile, setProfile] = useState([])
  useEffect(() => {
    setProfile(props.profilee)
    console.log(props.profilee)
  }, [props.profilee])
  const [anchorEl, setAnchorEl] = React.useState(null);

  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };
  // const logout = () => {
  //   setAnchorEl(null);
  //   const cookies = new Cookies();
  //   cookies.remove('iPlanetDirectoryPro');
  //   cookies.remove('id');
  //   window.location = "/login"
  //   // <Redirect to="/login"/>

  // }
  const logout = () => {
    setAnchorEl(null);
    const cookies = new Cookies();

    Axios({
      method: 'post',
      url:'https://account-api.collabkare.com/logout',
      //'https://auth.collabkare.com/am/json/sessions?_action=logout',
      headers: { 'token': cookies.get('iPlanetDirectoryPro') }
    })
      .then(res => {
        console.log(res);
        sessionStorage && sessionStorage.clear && sessionStorage.clear();
        localStorage && localStorage.clear && localStorage.clear();
        cookies.remove('iPlanetDirectoryPro');
        cookies.remove('id');
        window.location = "/login"
      }
      ).catch(err => {
        console.log(err)
      })
    // cookies.remove('iPlanetDirectoryPro');
    // cookies.remove('id');
    // sessionStorage && sessionStorage.clear && sessionStorage.clear();
    // localStorage && localStorage.clear && localStorage.clear();
    // window.location = `https://auth.collabkare.com/am/XUI/?realm=${cookies.get('domain')}#logout/&goto=https://myaccount.collabkare.com/login`
    // <Redirect to="/login"/>
  }
  const numbers = ["1", "2", "3", "4", "5"];
  const listItems = numbers.map((number) =>
    <li>{number}</li>
  );
  const numbers1 = !props.loading && profile !== [] ? profile.map((res) => res.roles) : ""
  const num = <li key={numbers1.roles}>{numbers1.roles}</li>

  const open = Boolean(anchorEl);
  const id = open ? 'simple-popover' : undefined;
  return (
    <div className={classes.root}>

      <Toolbar style={{ backgroundColor: '#004087', borderRadius: '1px solid #dde6ff', minHeight: '0px' }}>
        <img src="../../../../../src/assets/images/Asset-1.svg" alt="logo" className={classes.icon}></img>

        {/* <IconButton style={{ marginLeft: 'auto' }} aria-describedby={id} color="primary" onClick={handleClick}>
        
          <AccountCircle style={{ color: 'white' }} />
        </IconButton> */}
        <div style={{ display: 'inline-block', padding: '2px', marginLeft: 'auto' }}>
          <div className={Appcss.aaa}

            aria-describedby={id} onClick={handleClick}
          >
            &nbsp; <img src='../../../../../src/assets/images/pic.jpeg' className={Appcss.imageicon} ></img>
            &nbsp; {!props.loading && profile !== [] ?
              <div>
                {profile.map(res => <div key={res._id}>
                  <span style={{ color: 'white' }}>
                    <ListItemText primary={
                      <span style={{ fontSize: '13px', color: '#FFFFFF' }}>{res.username}</span>}
                      secondary={<span style={{ fontSize: '10px', color: '#FFFFFF' }}>{res.roles[0]}</span>} />
                  </span>

                </div>)}
              </div>
              : ""}


            {/* <span style={{ color: 'white' }}>
              <ListItemText primary={'admin@collabnotes.com'} secondary={<span style={{color:'white'}}>super admin</span>} />
            </span>
            <div>
              <img src='../../../../../src/assets/images/pic.jpeg' className={Appcss.imageicon} ></img>
          
              </div> */}
          </div>

        </div>
      </Toolbar>

      <Popover
        id={id}
        open={open}
        anchorEl={anchorEl}
        onClose={handleClose}
        //style={{width:'500px'}}
        anchorOrigin={{
          vertical: 'bottom',
          horizontal: 'center',
        }}
        transformOrigin={{
          vertical: 'top',
          horizontal: 'center',
        }}
      >

        <Grid container
          direction="row"
          // justify="center"

          // alignItems="center"
          style={{ width: '400px', 
         // padding: '4px' 
          }} >
          <Grid item xs={6}>
            {!props.loading && profile !== [] ?
              <div style={{ textAlign: 'center', marginTop: '22px' }}>
                {profile.map(res => <div key={res._id}>
                  <Avatar
                    src='../../../../../src/assets/images/pic.jpeg'
                    name={res.username} size="98" round={true}
                    style={{ border: '1px solid #0055B4', height: '100px' }}
                  />


                  <br />
                  <div style={{ marginTop: '22px' }}>
                    <span style={{ textAlign: 'center', color: '#000000', fontWeight: 'bold', fontSize: '15px' }}>&nbsp;
                      {res.givenName}
                    </span>
                  </div>




                  <p style={{ textAlign: 'center', color: '#979797', fontSize: '14px' }}>{res.roles[0]}</p>
                </div>)}
              </div>
              : ""}
            {/* <div style={{ textAlign: 'center' }}>
              <Avatar
                color={Avatar.getRandomColor('sitebase', ['red', 'green', 'blue'])}
                // src="/assets/images/face-6.jpg"
                name="Admin"
                size="100" round={true}

              />
            </div>

            <span style={{ textAlign: 'center', color: 'black', fontWeight: 'bold' }}>&nbsp;{"users.sbmUserId"}</span>
            <p style={{ textAlign: 'center', color: 'black' }}>super admin</p> */}
          </Grid>
          <Grid item xs={6} >
            <div
              style={{ marginTop: '36px' }}
            >
              <Link to='/profile' style={{ textDecoration: 'none', color: '#0055B4', fontSize: '14px' }}>
                <ListItem
                //style={{ marginTop: '-3px' }}
                //  onClick={() => window.open('http://myaccount.collabkare.com/profile', "_blank")}
                >

                  <ListItemText primary={<span style={{ fontSize: '14px' }}>Manage My Account</span>} style={{ color: '#3f51b5' }} />
                </ListItem>
              </Link>


            </div>
          </Grid>
          <Grid item xs={6}>
            <div style={{ marginTop: '30px' }}>
              <Accordion  >
                <ExpansionPanelSummary

                  style={{ border: '1px solid rgba(63, 81, 181, 0.5)', borderRadius: '4px' }}
                  expandIcon={<ExpandMoreIcon />}
                  aria-controls="panel1a-content"
                  id="panel1a-header"
                >
                  <span>Switch Role</span>
                </ExpansionPanelSummary>

                <Divider />

                <div>
                  {!props.loading && profile !== [] ?
                    <div >
                      {profile.map(res => <div key={res._id}>

                        {/* <div>
                    <span style={{ textAlign: 'center', color: '#000000', fontWeight: 'bold', fontSize: '15px' }}>&nbsp;
                      {res.givenName}
                    </span>
                  </div> */}

                        <div>
                          {res.roles.map((option, i) =>
                          <ListItem button style={{padding:'0px'}}  key={`Key${i}`} onClick={()=>{
                            localStorage.setItem("Role", option)
                          }}>
                        
                          <ListItemText  primary={<span style={{fontSize:'14px',marginLeft:'20px'}}>{option}</span>} />
                        </ListItem>
                            // <div style={{marginLeft:'20px'}} key={`Key${i}`} onClick={()=>{
                            //   localStorage.setItem("Role", option)
                            // }}>{option}</div>
                          )}
                        </div>


                        {/* <p style={{ textAlign: 'center', color: '#979797', fontSize: '14px' }}>{res.roles[0]}</p> */}
                      </div>)}
                    </div>
                    : ""}



                  {/* <ListItem >
                          <ListItemText primary={<span style={{ fontSize: '14px', marginLeft: '2px' }}
                            onClick={() => localStorage.setItem('role', res.roles[index])}>
                            {res.roles[0]}

                          </span>} />
                        </ListItem> */}
                  {/* {listItems} */}
                  {/* <span style={{ display: 'flex', height: '36px' }}>Super Admin </span> */}






                </div>
              </Accordion>
            </div>
            {/* <Button
              variant="contained"
              // style={{ fontSize: '17px' }}



              fullWidth color='primary'
            >Switch Role</Button> */}
          </Grid>
          <Grid item xs={6} >
            <div style={{ marginTop: '30px' }}>
              <Button
              
                variant='outlined'
                onClick={logout}
                style={{ textTransform: 'none' ,fontSize:'17px'}}
                fullWidth color='primary'
              >Sign Out</Button>
            </div>
            <Divider/>
          </Grid>
        </Grid>



        {/* {!props.loading && profile !== [] ?
          <div style={{ textAlign: 'center', padding: '8px', width: '366px' }}>
            {profile.map(res => <div key={res._id}><Avatar name={res.username} size="100" round={true} />
              <h4>{res.username}</h4>
              {res.roles[0]}
            </div>)}
          </div>
          : ""}
        

        <div style={{ textAlign: 'center' }}>
          <Divider />

          <Link to='/profile' style={{ textDecoration: 'none', color: 'rgb(39, 39, 39)', fontSize: '14px' }}>
            <Button style={{ width: '100%', textTransform: 'none' }} >  <img src="../../../../../src/assets/images/Manage-my-Account.svg" />&nbsp;Manage My Account</Button>
          </Link>
         
          <Divider />
          <Button style={{ marginTop: '2px', marginBottom: '2px' }} onClick={logout}>sign out</Button>
        </div> */}

      </Popover>

    </div>
  );
}

const mapStateToProps = state => {
  return {
    error: state.profileinfo.error,
    profilee: state.profileinfo.profile,
    loading: state.profileinfo.loading
  }
}
const mapDispatchToProps = dispatch => {
  return {
    profildata: () => {
      dispatch(profildata());
    },

  };
};

export default withStyles()(connect(
  mapStateToProps,
  mapDispatchToProps
)(Toobars));


