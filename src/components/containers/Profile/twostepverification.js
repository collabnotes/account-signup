import React, { useState } from 'react';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import { BrowserRouter as Router, Route, Link, Redirect } from "react-router-dom";
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogActions from '@material-ui/core/DialogActions';
import ArrowBackIcon from '@material-ui/icons/ArrowBack';
import { Paper, Grid } from '@material-ui/core';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import QRCode from "qrcode.react";
import ListItemText from '@material-ui/core/ListItemText';
import EmailOutlinedIcon from '@material-ui/icons/EmailOutlined';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import TimelapseIcon from '@material-ui/icons/Timelapse';
import FormGroup from '@material-ui/core/FormGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Switch from '@material-ui/core/Switch';
import Dialog from '@material-ui/core/Dialog';
import Cookies from 'universal-cookie';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import Axios from 'axios';
import FormControl from "@material-ui/core/FormControl";
import Select from "@material-ui/core/Select";
import InputLabel from "@material-ui/core/InputLabel";
import MenuItem from "@material-ui/core/MenuItem";
import { profildata } from "../../redux/action-creators/profileinfo";
import { connect } from 'react-redux';
import Snackbar from '@material-ui/core/Snackbar';
import MuiAlert from '@material-ui/lab/Alert';
const emailRegex = RegExp(/^[^@]+@[^@]+\.[^@]+$/)
const passwordRegex = RegExp(/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,}$/)
function Alert(props) {
    return <MuiAlert elevation={6} variant="filled" {...props} />;
}
const useStyles = makeStyles((theme) => ({
    paper: {
        marginTop: theme.spacing(7),
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
    },
    form: {
        width: '100%', // Fix IE 11 issue.
        marginTop: theme.spacing(1),
    },

    submit: {
        margin: theme.spacing(3, 0, 2),
    },

}));

function TwoStepVerification(props) {
    const classes = useStyles();
    const [open, setOpen] = React.useState(false);
    const [state, setState] = React.useState({
        checkedA: true,
        checkedB: true,
    });

    const handleChange = (event) => {
        setState({ ...state, [event.target.name]: event.target.checked });
        setOpens(true);
    };
    const handleClickOpen = () => {
        setOpen(true);
    };

    const handleClose = () => {
        setOpen(false);
        setstep1(true);
    };
    const [step1, setstep1] = useState(true)
    const [step2, setstep2] = useState(false)

    //
    //password fields
    const initialForm = {
        id: null, username: '', password: ""
    }
    const [orguser, setorgUser] = useState(initialForm)

    const handleInputChange = event => {
        const { name, value } = event.target
        const lengthValidate = value.length > 0 && value.length < 3
        switch (name) {
            case "password":
                formErrors.password = passwordRegex.test(value) ? "" : "Minimum eight characters, at least one uppercase letter, one lowercase letter, one number and one special character"
                break
            case "username":
                formErrors.username = emailRegex.test(value) ? "" : "Email id is invalid"
                break
            default:
                break
        }
        setFieldError({
            ...formErrors

        })
        //  formErrors.username.length !== 0 ? setIsError(true)
        formErrors.password.length !== 0 ? setIsError(true)
            : setIsError(false)

        setorgUser({ ...orguser, [name]: value })
    }
    const [filedError, setFieldError] = useState({
        ...orguser
    })
    const formErrors = { ...filedError }
    const [isError, setIsError] = useState(false)
    const isEmpty =
        orguser.password.length > 0
    // two step verification
    const initial = {
        codes: ""
    }
    const [code, setCode] = useState(initial)
    const [qrValue, setQrValue] = useState("");
    const handlecodeChange = event => {
        const { name, value } = event.target
        const lengthValidate = value.length > 0 && value.length < 6
        switch (name) {
            case "codes":
                codeErrors.codes = lengthValidate
                    ? "Minimum 6 characaters required"
                    : ""
                break
            default:
                break
        }
        setcodeError({
            ...codeErrors
        })
        codeErrors.codes.length !== 0 ? seterr(true) : seterr(false)
        setCode({ ...code, [name]: value })
    }
    const [codeError, setcodeError] = useState({
        ...code
    })
    const [err, seterr] = useState(false)
    const iserr = code.codes.length > 0
    const codeErrors = { ...codeError }
    const [coded, setcoded] = useState()
    const submit = (event) => {
        event.preventDefault()
        const cookies = new Cookies();
        console.log(orguser)
        let url = `https://account-api.collabkare.com/mfa/${cookies.get('domain')}/${cookies.get('email')}`
        Axios({
            method: 'post',
            url: url,
            headers: { token: cookies.get('iPlanetDirectoryPro') },
            data: {
                username: cookies.get('email'),
                password: orguser.password
            }
        })

            .then(function (response) {
                console.log(response);
                // setcoded(response)
                setQrValue(response.data.callbacks[1].output[0].value.split("'")[3])
                sss(response)

            })
            .catch(function (error) {
                console.log(error);

            });
        setorgUser(initialForm)

    }
    const [otp, setotp] = useState('')
    const sss = (response) => {
        const cookies = new Cookies();
        //console.log(coded)
        let url = `https://account-api.collabkare.com/mfa/${cookies.get('domain')}/${cookies.get('email')}`
        Axios({
            method: 'post',
            url: url,
            headers: { token: cookies.get('iPlanetDirectoryPro') },
            data: response.data
        }
        ).then(res => {
            setotp(res)
            setstep1(false)
            setstep2(true)
            console.log(res)

        }).catch(err => {
            console.log(err)
        })
    }
    const submitcode = (e) => {
        e.preventDefault()
        console.log(code)
        const cookies = new Cookies();
        let url = `https://account-api.collabkare.com/mfa/${cookies.get('domain')}/${cookies.get('email')}`
        Axios({
            method: 'post',
            url: url,
            headers: { token: cookies.get('iPlanetDirectoryPro') },
            data: {
                "authId": otp.data.authId,
                "template": "",
                "stage": "AuthenticatorOATH7",
                "header": "ForgeRock Authenticator (OATH)",
                "callbacks": [{
                    "type": "NameCallback",

                    "output": [
                        {
                            "name": "prompt", "value": "Enter verification code:"
                        }],
                    "deviceotp": qrValue,
                    "input":
                        [
                            {
                                "name": "IDToken1", "value": code.codes
                            }
                        ]
                }, {
                    "type": "ConfirmationCallback",
                    "output": [
                        {
                            "name": "prompt", "value": ""
                        },
                        { "name": "messageType", "value": 0 },
                        { "name": "options", "value": ["Submit", "Skip this step"] },
                        { "name": "optionType", "value": -1 },
                        { "name": "defaultOption", "value": 0 }],
                    "input": [
                        { "name": "IDToken2", "value": 0 }]
                }]
            }
        }



        ).then(res => {
            // setQrValue(res.data.callbacks[1].output[0].value.split("'")[3])
            console.log(res)
            //setstep2(false)
            // setswit(true)

        }).catch(err => {
            console.log(err)
        })



        setCode(initial)

        console.log(code)
    }
    const [swit, setswit] = useState(false)
    const [qr, setqr] = useState("")
    const [show, setshow] = useState(false)
    const display = () => {
        setshow(true)
    }
    React.useEffect(() => {
        const cookies = new Cookies();
        let url = `https://account-api.collabkare.com/userid/${cookies.get('domain')}/${cookies.get('email')}`
        Axios({
            method: 'get',
            url: url,
            headers: { token: cookies.get('iPlanetDirectoryPro') },
        })

            .then(function (response) {
                console.log(response);
                setqr(response.data)

            })
            .catch(function (error) {
                console.log(error);

            });


    }, [])
    //email switch button
    const [opens, setOpens] = React.useState(false);

    const handleClickOpens = () => {
        setOpens(true);
    };

    const handleCloses = () => {
        setOpens(false);
    };

    const [profile, setProfile] = useState([])
    React.useEffect(() => {
        setProfile(props.profile)

    }, [props.profile])
    const aa = !props.loading && profile !== [] ?
        profile.map(res => res.hasOwnProperty("iplanet_am_user_auth_config")) : ''
    const bb = !props.loading && profile !== [] && aa == 'true' ? profile.map(res => res.iplanet_am_user_auth_config) : false
    const cc = !props.loading && profile !== [] && bb == "[Empty]" || bb == "MFAEmail" || bb == "MFAuthentication" ? true : false
    const dd = !props.loading && profile !== [] && bb == "MFAuthentication" || bb == "FRAuthChain" ? true : false
    const initialemail = {
        sn: "",
        mail: '',
        userpassword: "",
        iplanet_am_user_auth_config: 'MFAEmail'

    }
    const [orgem, setorgem] = useState(initialemail)

    const handleInputem = event => {
        const { name, value } = event.target
        setorgem({ ...orgem, [name]: value })
    }
    const emailsubmit = (e) => {
        e.preventDefault()
        if (!orgem.userpassword) return
        const cookies = new Cookies();
        const sn = !props.loading && profile !== [] ? profile.map(res => res.sn) : '';
        const mail = !props.loading && profile !== [] ? profile.map(res => res.mail) : '';
        const iplanet = !props.loading && profile !== [] ? profile.map(res => res.iplanet_am_user_auth_config) : ''
        let url = `https://account-api.collabkare.com/userinfo/${cookies.get('domain')}/${cookies.get('email')}`
        Axios({
            method: 'put',
            url: url,
            headers: { token: cookies.get('iPlanetDirectoryPro') },
            data: {
                sn: sn.toString(),
                mail: mail.toString(),
                userpassword: orgem.userpassword,
                'iplanet-am-user-auth-config': orgem.iplanet_am_user_auth_config
            }
        })

            .then(function (response) {
                console.log(response);
                setqr(response.data)

            })
            .catch(function (error) {
                console.log(error);
                seter(error.response.data.message)
                setOpensn(true)
            });

        //   let  data={

        //         sn:aa.toString(),
        //         mail: bb.toString(),

        //         userpassword: orgem.userpassword,
        //         'iplanet-am-user-auth-config':cc.toString()  
        //     }
        //     console.log(data)
        setorgem(initialemail)

    }
    const [opensn, setOpensn] = React.useState(false);

    const handleClicksn = () => {
        setOpensn(true);
    };

    const handleClosesn = (event, reason) => {
        if (reason === 'clickaway') {
            return;
        }

        setOpensn(false);
    };
    const [er, seter] = React.useState('')
    const cookies = new Cookies();
    if (cookies.get('iPlanetDirectoryPro') == undefined) {
        return window.location = '/login'
    }
    //  let aaa = qr !== "" && qr !== null ? qr.split("=") : ""
    // let bbb = qr !== "" && qr !== null ? qr.split(",") : ""

    return (
        <div>
            <Snackbar open={opensn} autoHideDuration={6000} onClose={handleClosesn}>
                <Alert onClose={handleClosesn} severity="error">
                    {er}
                </Alert>
            </Snackbar>
            {/* {console.log(aa, cc)} */}
            {/* {console.log(qr,aa ? cc : true,aaa!==""?`${aaa[1].split(",")[0]}=${bbb[1]}=${bbb[2]}`:"")} */}

            <Paper>
                <DialogTitle style={{ textAlign: 'center' }} >
                    <Link to='/profile' style={{ textDecoration: 'none', color: '#0055B4', fontSize: '14px' }}>
                        <ArrowBackIcon style={{ float: 'left' }} />
                    </Link>    <span >Two Step Verification</span>
                </DialogTitle>
            </Paper>
            <Container component="main" maxWidth="md">

                <CssBaseline />
                <div className={classes.paper}>


                    <form className={classes.form} noValidate>
                        {/* <Paper>
                            <p style={{ fontSize: '15px', padding: '25px' }}>2-Step verfication is ON since Dec 6, 2019
                             <Button style={{ float: 'right', marginTop: '-8px' }} color='primary' variant='contained'>Turn Off</Button>
                            </p>

                        </Paper> */}

                        <p style={{ fontSize: '17px' }}>Your second step</p>
                        <p style={{ fontSize: '12px', color: '#6D7278' }}>After entering your password, you’ll be asked for a second verfication step.</p>

                        <Paper>
                            <Grid container
                                direction="row"
                                justify="space-between"
                                alignItems="flex-start">
                                <Grid item xs={12} sm={6}>
                                    <List>
                                        <ListItem alignItems="flex-start">
                                            <ListItemIcon>
                                                <EmailOutlinedIcon />
                                            </ListItemIcon>
                                            <ListItemText
                                                primary={<span style={{ fontSize: '15px', fontWeight: 'bold' }}>E mail</span>}
                                            />
                                        </ListItem>

                                        <ListItem>

                                            <ListItemText inset
                                                primary={<span style={{ fontSize: '13px', fontWeight: 'bold' }}>{cookies.get('email')} &nbsp;<span style={{ color: '#488D00' }}>Verfied</span></span>}
                                                secondary={
                                                    <React.Fragment>

                                                        {<span style={{ fontSize: '12px', color: "#6D7278" }}>Verficatin codes are sent by email</span>}
                                                    </React.Fragment>
                                                }
                                            />
                                        </ListItem>
                                    </List>
                                </Grid>
                                <Grid item xs={12} sm={6}>

                                    <FormGroup row style={{ float: 'right', marginTop: '8px', marginRight: '30px' }}>
                                        <FormControlLabel
                                            control={
                                                <Switch
                                                    checked={aa == 'true' ? cc : true}
                                                    onChange={handleChange}
                                                    name="checkedA"
                                                    color="primary"
                                                />
                                            }
                                        // label={state.checkedA ? "ON" : "OFF"}
                                        />

                                    </FormGroup>
                                    <Dialog open={opens}
                                        fullWidth={true}
                                        maxWidth={'sm'}
                                        onClose={handleCloses} aria-labelledby="form-dialog-title">
                                        <DialogTitle id="form-dialog-title" style={{ backgroundColor: 'rgb(0, 64, 135)', color: 'white', textAlign: 'center' }}>Verification</DialogTitle>
                                        <form
                                            //className={classes.form}
                                            noValidate onSubmit={emailsubmit}>
                                            <DialogContent>

                                                <TextField
                                                    variant="outlined"
                                                    margin="normal"
                                                    required
                                                    fullWidth
                                                    margin='dense'
                                                    name="userpassword"
                                                    placeholder="password"
                                                    label="Password"
                                                    type="password"
                                                    id="password"
                                                    autoComplete="current-password"
                                                    value={orgem.userpassword}
                                                    onChange={handleInputem}
                                                //  error={filedError.password !== ""}
                                                // helperText={filedError.password !== "" ? `${filedError.password}` : ""}
                                                />
                                                <br />
                                                <div style={{ marginTop: '5px' }}></div>
                                                <FormControl fullWidth
                                                    required
                                                    variant="outlined"
                                                    value={orgem.iplanet_am_user_auth_config}
                                                    // className={classes.formControl}
                                                    //margin="normal"
                                                    name='iplanet_am_user_auth_config'
                                                    id="auth">
                                                    <InputLabel htmlFor="auth">Authentication type</InputLabel>
                                                    <Select value={orgem.iplanet_am_user_auth_config}
                                                        id="auth"
                                                        labelId="demo-simple-select-outlined-label"
                                                        onChange={handleInputem}
                                                        name='iplanet_am_user_auth_config'
                                                        margin="dense"


                                                    >

                                                        <MenuItem value={'MFAEmail'}>Email</MenuItem>
                                                        <MenuItem value={'MFAuthentication'}>Email & Authenticator</MenuItem>
                                                        {/* <MenuItem value={'FRAuthChain'}>Authenticator</MenuItem> */}
                                                    </Select>
                                                </FormControl>
                                            </DialogContent>
                                            <DialogActions>
                                                <Button onClick={handleCloses} variant='outlined' color="primary">
                                                    Cancel
                                         </Button>
                                                <Button
                                                    type="submit"
                                                    //fullWidth
                                                    variant="outlined"

                                                    color="primary"
                                                // className={classes.submit}
                                                >
                                                    submit
                                           </Button>

                                            </DialogActions>
                                        </form>
                                    </Dialog>

                                </Grid>

                            </Grid>
                        </Paper>
                        <br />
                        <Paper>
                            <Grid container
                                direction="row"
                                justify="space-between"
                                alignItems="flex-start">
                                <Grid item xs={12} sm={6}>
                                    <List>
                                        <ListItem alignItems="flex-start">
                                            <ListItemIcon>
                                                <TimelapseIcon />
                                            </ListItemIcon>

                                            <ListItemText
                                                primary={<span style={{ fontSize: '15px', fontWeight: 'bold' }}>Authenticator app</span>}
                                                secondary={
                                                    <React.Fragment>

                                                        {<span style={{ fontSize: '12px', color: "#6D7278" }}>Use the Authenticator app to get free verfication codes, even when your phone is offline.</span>}
                                                    </React.Fragment>
                                                }
                                            />
                                        </ListItem>


                                    </List>

                                </Grid>
                                <Grid item xs={12} sm={6}>
                                    <FormGroup row style={{ float: 'right', marginTop: '8px', marginRight: '30px' }}>
                                        <FormControlLabel
                                            control={
                                                <Switch
                                                    checked={dd}
                                                    onChange={handleChange}
                                                    name="checkedB"
                                                    color="primary"
                                                />
                                            }
                                        //label={state.checkedB ? "ON" : "OFF"}
                                        />

                                    </FormGroup>
                                </Grid>
                            </Grid>
                            <div>

                                <Dialog open={open} onClose={handleClose} aria-labelledby="form-dialog-title">
                                    <DialogTitle id="form-dialog-title" style={{ backgroundColor: 'rgb(0, 64, 135)', color: 'white', textAlign: 'center' }}>Registration</DialogTitle>
                                    {step1 ? <span>
                                        <form className={classes.form} noValidate onSubmit={submit}>
                                            <DialogContent>

                                                <DialogContentText>
                                                    To Enable Authenticator app, please enter your password here.
                                     </DialogContentText>

                                                <TextField
                                                    variant="outlined"
                                                    margin="normal"
                                                    required
                                                    fullWidth
                                                    margin='dense'
                                                    name="password"
                                                    placeholder="password"
                                                    label="Password"
                                                    type="password"
                                                    id="password"
                                                    autoComplete="current-password"
                                                    value={orguser.password}
                                                    onChange={handleInputChange}
                                                    error={filedError.password !== ""}
                                                    helperText={filedError.password !== "" ? `${filedError.password}` : ""}
                                                />
                                            </DialogContent>
                                            <DialogActions>
                                                <Button
                                                    type="submit"
                                                    fullWidth
                                                    variant="contained"
                                                    disabled={!isEmpty || isError}
                                                    color="primary"
                                                    className={classes.submit}
                                                >
                                                    Next
          </Button>

                                            </DialogActions>
                                        </form>

                                    </span> : step2 ?
                                            <DialogContent>
                                                <div style={{ textAlign: 'center' }}>
                                                    <p>Scan the QR code below with an Authentication application,Such as Google Authenticator,on your phone</p>

                                                    {qrValue && qrValue !== "" ?
                                                        <div>
                                                            <QRCode
                                                                id="qr-gen"
                                                                value={qrValue}
                                                                size={250}
                                                                level={"H"}
                                                                includeMargin={true}
                                                            />






                                                            <form className={classes.form} noValidate onSubmit={submitcode}>
                                                                <TextField
                                                                    variant="outlined"
                                                                    margin="normal"
                                                                    required
                                                                    fullWidth
                                                                    margin='dense'
                                                                    placeholder='Enter code created by authenticator app'
                                                                    name="codes"
                                                                    label="6-digit code"
                                                                    id="code"
                                                                    autoComplete="codes"
                                                                    value={code.codes}
                                                                    onChange={handlecodeChange}
                                                                    error={codeError.codes !== ""}
                                                                    helperText={codeError.codes !== "" ? `${codeError.codes}` : ""}
                                                                />
                                                                <DialogActions>
                                                                    <Button variant='outlined' color="primary" onClick={handleClose}>
                                                                        cancel
                                                       </Button>
                                                                    <Button variant='outlined' type="submit" color="primary" disabled={!iserr || err}>
                                                                        verify
                                                         </Button>
                                                                </DialogActions>
                                                            </form></div> : <p>loading QR code</p>}
                                                </div> </DialogContent> : ''}




                                </Dialog>

                            </div>
                            {/* {console.log(qr !== "" && (!qr.description.includes("otpauth")))} */}
                            {state.checkedB ?
                                <div style={{ textAlign: 'center' }}>
                                    {qr !== "" && qr.description && qr.description == null || qr.description == "null," ? <Button variant="outlined" color="primary" onClick={handleClickOpen}>
                                        Register
                                 </Button> : ''}
                                    {!show && qr && qr !== "" && qr.description && qr.description !== null && qr.description !== "null," ? <Button variant='outlined' onClick={display}>show</Button> : ''}
                                    {show && qr && qr !== "" && qr.description && qr.description !== null && qr.description.includes("otpauth") ? <QRCode
                                        id="qr-gen"
                                        value={qr.description}
                                        // {aaa !== "" ? `${aaa[1].split(",")[0]}=${bbb[1]}=${bbb[2]}` : ""}
                                        size={150}
                                        level={"H"}
                                        includeMargin={true}
                                    /> : ''}
                                    {/* <p style={{ fontSize: '13px', color: "#0055B4" }}>SCAN CODE </p> */}
                                </div> : ''}
                            <br />
                        </Paper>
                        {/* <Button
            type="submit"
            fullWidth
            variant="contained"
            color="primary"
            className={classes.submit}
          >
            Sign In
          </Button> */}

                    </form>
                </div>

            </Container>
        </div >
    );
}


const mapStateToProps = state => {
    return {
        error: state.profileinfo.error,
        profile: state.profileinfo.profile,
        loading: state.profileinfo.loading
    }
}
const mapDispatchToProps = dispatch => {
    return {
        profildata: () => {
            dispatch(profildata());
        },

    };
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(TwoStepVerification);


    //                                         swit ?
    //                                             <div>
    //                                                 <DialogContent>
    //                                                     <FormGroup row>
    //                                                         <FormControlLabel
    //                                                             control={<Switch checked={state.checkedA} onChange={handleChange} name="checkedA" />}
    //                                                             olor="primary"
    //                                                             label="enable email"
    //                                                         />
    //                                                         <FormControlLabel
    //                                                             control={
    //                                                                 <Switch
    //                                                                     checked={state.checkedB}
    //                                                                     onChange={handleChange}
    //                                                                     name="checkedB"
    //                                                                     color="primary"
    //                                                                 />
    //                                                             }
    //                                                             label="enable google authenticator"
    //                                                         />

    //                                                     </FormGroup></DialogContent>
    //                                                 <DialogActions>
    //                                                     <Button
    //                                                         type="submit"
    //                                                         fullWidth
    //                                                         variant="contained"
    //                                                         color="primary"
    //                                                         className={classes.submit}
    //                                                     >
    //                                                         submit
    //   </Button>

    //                                                 </DialogActions>
    //                                             </div>
    //                                             : ''}