import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import Avatar from '@material-ui/core/Avatar';
// import ImageIcon from '@material-ui/icons/Image';
import WorkIcon from '@material-ui/icons/Work';
import BeachAccessIcon from '@material-ui/icons/BeachAccess';
import Grid from '@material-ui/core/Grid';
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';
import IconButton from '@material-ui/core/IconButton';
import DeleteIcon from '@material-ui/icons/Delete';
import Badge from '@material-ui/core/Badge';
import { Divider } from '@material-ui/core';


const Groups = () => {
    const useStyles = makeStyles((theme) => ({
        root: {
            width: '100%',
            // maxWidth: 360,
            backgroundColor: theme.palette.background.paper,
        },
    }));
    const classes = useStyles();
    return (
        <div className={classes.root}>
            <div style={{ width: "800px", margin: 'auto' }}>
                <div style={{ paddingTop: '25px' }}>
                    <Grid container spacing={2}>
                        <Grid item xs={6} md={6}>
                            <p style={{ fontSize: '16px', marginLeft: '35px' }}> Suscribed Groups</p>
                        </Grid>
                        <Grid item xs={6} md={6}>
                            <List >

                                <ListItem>
                                    <ListItemAvatar>
                                        <Avatar>
                                            {/* <ImageIcon /> */}
                                        </Avatar>
                                    </ListItemAvatar>
                                    <ListItemText primary="Administration" />
                                    <Badge color="primary" badgeContent={7} showZero>
                                        <IconButton edge="end" aria-label="delete">
                                            <img src="../../../../../src/assets/images/Super-Administrator.svg" alt='user'></img>
                                        </IconButton>
                                    </Badge>

                                </ListItem>
                            </List>
                        </Grid></Grid>
                    <Divider />
                    <Grid container spacing={2}>
                        <Grid item xs={6} md={6}>
                            <List >

                                <ListItem>
                                    <ListItemAvatar>
                                        <Avatar>
                                            {/* <ImageIcon /> */}
                                        </Avatar>
                                    </ListItemAvatar>
                                    <ListItemText primary="Administration" secondary="No description" />
                                    <Badge color="primary" badgeContent={7} showZero>
                                        <IconButton edge="end" aria-label="delete">
                                            <img src="../../../../../src/assets/images/Super-Administrator.svg" alt='user'></img>
                                        </IconButton>
                                    </Badge>

                                </ListItem>
                                <ListItem>
                                    <ListItemAvatar>
                                        <Avatar>
                                            <WorkIcon />
                                        </Avatar>
                                    </ListItemAvatar>
                                    <ListItemText primary="Champions" secondary="No description" />
                                    <Badge color="primary" badgeContent={8} showZero>
                                        <IconButton edge="end" aria-label="delete">
                                            <img src="../../../../../src/assets/images/Super-Administrator.svg" alt='users'></img>
                                        </IconButton></Badge>
                                </ListItem>
                                <ListItem>
                                    <ListItemAvatar>
                                        <Avatar>
                                            <BeachAccessIcon />
                                        </Avatar>
                                    </ListItemAvatar>
                                    <ListItemText primary="The Crew" secondary="NO description" />
                                    <Badge color="primary" badgeContent={5} showZero>
                                        <IconButton edge="end" aria-label="delete">
                                            <img src="../../../../../src/assets/images/Super-Administrator.svg" alt='use'></img>
                                        </IconButton></Badge>
                                </ListItem>
                                <ListItem>
                                    <ListItemAvatar>
                                        <Avatar>
                                            <BeachAccessIcon />
                                        </Avatar>
                                    </ListItemAvatar>
                                    <ListItemText primary="Rainbows" secondary="NO description" />
                                    <Badge color="primary" badgeContent={2} showZero>
                                        <IconButton edge="end" aria-label="delete">
                                            <img src="../../../../../src/assets/images/Super-Administrator.svg" alt='userss'></img>
                                        </IconButton></Badge>
                                </ListItem>
                                <ListItem>
                                    <ListItemAvatar>
                                        <Avatar>
                                            <BeachAccessIcon />
                                        </Avatar>
                                    </ListItemAvatar>
                                    <ListItemText primary="Alpha Team" secondary="NO description" />
                                    <Badge color="primary" badgeContent={2} showZero>
                                        <IconButton edge="end" aria-label="delete">
                                            <img src="../../../../../src/assets/images/Super-Administrator.svg" alt='userimg'></img>
                                        </IconButton></Badge>
                                </ListItem>
                            </List>
                        </Grid>
                        <Grid item xs={6} md={6}>
                            <br/>
                            <Grid container spacing={2}>
                                <Grid item xs={6} md={6}>
                                    <span>Audrey Abrham</span>
                                </Grid>
                                <Grid item xs={6} md={6}>
                                    <span> audrey@gmail.com</span>
                                </Grid>
                                <Divider />
                                <Grid item xs={6} md={6}>
                                    <span>Cameron Grill</span>
                                </Grid>
                                <Grid item xs={6} md={6}>
                                    <span>gill@gmail.com</span>
                                </Grid>
                                <Divider />
                                <Grid item xs={6} md={6}>
                                    <span>Cameron Grill</span>
                                </Grid>
                                <Grid item xs={6} md={6}>
                                    <span>gill@gmail.com</span>
                                </Grid>
                                <Divider />
                                <Grid item xs={6} md={6}>
                                    <span>Andrew Allen</span>
                                </Grid>
                                <Grid item xs={6} md={6}>
                                    <span>allen@gmail.com</span>
                                </Grid>
                                <Divider />
                                <Grid item xs={6} md={6}>
                                    <span>Colin</span>
                                </Grid>
                                <Grid item xs={6} md={6}>
                                    <span>colin@gmail.com</span>
                                </Grid>
                                <Divider />
                                <Grid item xs={6} md={6}>
                                    <span>Johndoe</span>
                                </Grid>
                                <Grid item xs={6} md={6}>

                                    <span>doe@gmail.com</span>
                                </Grid>
                            </Grid>











                        </Grid>
                    </Grid>
                </div></div></div>
    )
}
export default Groups;




