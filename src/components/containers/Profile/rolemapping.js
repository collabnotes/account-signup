import React from 'react';
import Grid from '@material-ui/core/Grid';
import { makeStyles } from '@material-ui/core/styles';

import DoneIcon from '@material-ui/icons/Done';
import { Avatar, Divider } from '@material-ui/core';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import VpnKeyTwoToneIcon from '@material-ui/icons/VpnKeyTwoTone';
const Rolemaping = () => {
    const useStyles = makeStyles((theme) => ({
        root: {
            width: '100%',
            backgroundColor: theme.palette.background.paper,

        },
        fonts: {
            fontSize: '16px'
        }
    }));
    const classes = useStyles();
    return (
        <div>
            <List component="nav" className={classes.root} aria-label="contacts">
                <ListItem >
                    <ListItemIcon>
                        <VpnKeyTwoToneIcon />
                    </ListItemIcon>
                    <ListItemText primary="Change Password" secondary={"it's a good idea to use a strong password that you don't use elsewhere"} />
                </ListItem>

            </List>
            <Divider />
            <Grid container spacing={2}
                direction="row"
                justify="center"
                alignItems="flex-end"
                style={{ width: '500px' }}
            >
                <Grid item xs={12} sm={2}>
                    <label>Current</label>
                </Grid>
                <Grid item xs={12} sm={10}>
                    <input
                        label='current password'
                        name='current'
                        type='password'
                    />

                </Grid>
                <Grid item xs={12} sm={2}>
                    <label >New</label>
                </Grid>
                <Grid item xs={12} sm={10}>
                    <input
                        label='New password'
                        name='New'
                        type='password'
                    />

                </Grid>
                <Grid item xs={12} sm={2}>
                    <label >Retype new</label>
                </Grid>
                <Grid item xs={12} sm={10}>
                    <input
                        label='retype New password'
                        name='retypeNew'
                        type='password'
                    />

                </Grid>
            </Grid>
        </div>
        // <div className={classes.root}>
        //     <div style={{ width: "800px", margin: 'auto' }}>
        //         <div style={{paddingTop:'25px'}}>
        //         <Grid container spacing={2}>
        //             <Grid item xs={5} md={5}>
        //                 {/* <h3>Avaliable roles</h3>
        //                 <p className={classes.fonts}>Super Admin</p>
        //                 <p className={classes.fonts}>IT Admin</p>
        //                 <p className={classes.fonts}>Business Admin</p>
        //                 <p className={classes.fonts}>Developer Admin</p>
        //                 <p className={classes.fonts}>Research Admin</p>
        //                 <p className={classes.fonts}>Case Manger</p>
        //                 <p className={classes.fonts}>Patient </p>
        //                 <p className={classes.fonts}>Provider</p> */}
        //             </Grid>
        //             <Grid item xs={6} md={6}>

        //                 <h3>assign roles</h3>
        //                 <div>
        //                 <h4 style={{display:'flex'}}><Avatar style={{ width: '24px', height: '24px', backgroundColor: 'rgb(66, 133, 244)' ,marginTop:'-2px'}}><DoneIcon /></Avatar>&nbsp; Super Admin</h4>
        //                     {/* <h4 style={{display:'flex'}}><Avatar style={{ width: '24px', height: '24px', backgroundColor: 'rgb(66, 133, 244)',marginTop:'-2px' }}><DoneIcon /></Avatar>&nbsp; IT Admin</h4>
        //                     <h4 style={{display:'flex'}}><Avatar style={{ width: '24px', height: '24px', backgroundColor: 'rgb(66, 133, 244)' ,marginTop:'-2px'}}><DoneIcon /></Avatar>&nbsp; Business Admin</h4> */}
        //                 </div>
        //             </Grid>
        //         </Grid></div>
        //     </div></div>
    )
}
export default Rolemaping;