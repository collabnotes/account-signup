import React, { useState } from "react";
import Grid from '@material-ui/core/Grid';
import { makeStyles } from '@material-ui/core/styles';
import { BrowserRouter as Router, Route, Link, Redirect } from "react-router-dom";
import DialogTitle from '@material-ui/core/DialogTitle';
import { Divider, Paper } from '@material-ui/core';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import VpnKeyTwoToneIcon from '@material-ui/icons/VpnKeyTwoTone';
import PasswordStrengthBar from 'react-password-strength-bar';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import ArrowBackIcon from '@material-ui/icons/ArrowBack';
import DialogActions from '@material-ui/core/DialogActions';
import Cookies from 'universal-cookie';
import Typography from '@material-ui/core/Typography';
import Axios from 'axios';
import Container from '@material-ui/core/Container';
import Snackbar from '@material-ui/core/Snackbar';
import MuiAlert from '@material-ui/lab/Alert';
const passwordRegex = RegExp(/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,}$/)
function Alert(props) {
    return <MuiAlert elevation={6} variant="filled" {...props} />;
  }
const ResetPassword = () => {
    const useStyles = makeStyles((theme) => ({
        paper: {
            marginTop: theme.spacing(8),
            display: 'flex',
            flexDirection: 'column',
            alignItems: 'center',
        },
        avatar: {
            margin: theme.spacing(1),
            backgroundColor: theme.palette.secondary.main,
        },
        form: {
            width: '100%', // Fix IE 11 issue.

        },
        submit: {
            margin: theme.spacing(3, 0, 2),
        },
    }));
    const classes = useStyles();
    const initialForm = {
        currentpassword: "",
        newpassword: "",
        retypenewpassword: "",
    }

    const [fields, setFields] = useState(initialForm)

    // Copy fields as they all have the same name
    const [filedError, setFieldError] = useState({
        ...fields
    })

    const [isError, setIsError] = useState(false)


    const formErrors = { ...filedError }
    // Handle fields change



    const handleChange = (e) => {
        const { name, value } = e.target;

        // Handle errors



        switch (name) {

            case "newpassword":
                formErrors.newpassword = passwordRegex.test(value) ? "" : "Minimum eight characters, at least one uppercase letter, one lowercase letter, one number and one special character"
                fields.retypenewpassword == value ? formErrors.retypenewpassword = "" : formErrors.retypenewpassword = "passwords are not matching"
                break

            case "retypenewpassword":
                fields.newpassword == value ? formErrors.retypenewpassword = "" : formErrors.retypenewpassword = "passwords are not matching"
                break
            default:
                break
        }
        // set errors hook




        setFieldError({
            ...formErrors

        })
        formErrors.currentpassword.length !== 0 ? setIsError(true)
            : formErrors.retypenewpassword.length !== 0 ? setIsError(true)
                : setIsError(false)


        // Object.values(formErrors).forEach(error =>
        //   error.length > 0 ? setIsError(true) : setIsError(false)
        // )
        // Set values to the fields
        setFields({
            ...fields,
            [name]: value
        })

    }
    const isEmpty =
        fields.currentpassword.length > 0 && fields.newpassword.length > 0 && fields.retypenewpassword.length > 0

    const handlesubmit = (event) => {
        event.preventDefault()
        console.log(fields)
        const cookies = new Cookies();
        var cooki = cookies.get('iPlanetDirectoryPro')
        var email = cookies.get('email');
        var domain = cookies.get('domain');
        Axios({
            method: 'post',
            url: `https://account-api.collabkare.com/updatePassword/${domain}/${email}`,
            data: {

                "currentpassword": fields.currentpassword,
                "userpassword": fields.retypenewpassword

            },

            headers: { 'token': cooki }
        })
            .then(function (response) {
                console.log(response);
            })
            .catch(function (error) {
                console.log(error.response);
                seter(error.response.data.message)
                setOpen(true);
            });
        setFields(initialForm)
    }
    // React.useEffect(()=>{
        
    //     const cookies = new Cookies();
    //     let url = `https://account-api.collabkare.com/${cookies.get('domain')}/${cookies.get('email')}`
    //     Axios({
    //         method: 'get',
    //         url: url,
    //         headers: { token: cookies.get('iPlanetDirectoryPro') },
    //     })
    //         .then(function (response) {
    //             console.log(response);

    //         })
    //         .catch(function (error) {
    //             console.log(error);

    //         });

    // },[])
    const [open, setOpen] = React.useState(false);

    const handleClick = () => {
      setOpen(true);
    };
  
    const handleClose = (event, reason) => {
      if (reason === 'clickaway') {
        return;
      }
  
      setOpen(false);
    };
    const [er,seter]=React.useState('')
        return (
        <div
        //  style={{
        //     position: 'absolute', left: '50%', top: '25%',
        //     transform: 'translate(-50%, -0%)'
        // }}
        //style={{ width: "800px", margin: 'auto' }}
        >
             <Snackbar open={open} autoHideDuration={6000} onClose={handleClose}>
        <Alert onClose={handleClose} severity="error">
          {er}
        </Alert>
      </Snackbar>
            <Paper>
                <DialogTitle style={{ textAlign: 'center' }} >
                    <Link to='/profile' style={{ textDecoration: 'none', color: '#0055B4', fontSize: '14px' }}>
                        <ArrowBackIcon style={{ float: 'left' }} />
                    </Link>   <span > password update</span>
                </DialogTitle>
            </Paper>
            <Container component="main" maxWidth="md">
                <CssBaseline />

                <div className={classes.paper}>

                    <Paper style={{ padding: '16px' }}>
                        <form className={classes.form} noValidate onSubmit={handlesubmit}>
                            <List component="nav" className={classes.root} aria-label="contacts">
                                <ListItem >

                                    <ListItemText primary="Change Password" secondary={"it's a good idea to use a strong password that you don't use elsewhere"} />
                                </ListItem>

                            </List>
                            <Divider />
                            <br />

                            <Grid container spacing={2}
                                direction="row"
                                justify="center"
                                alignItems="flex-end"

                            >
                                <Grid item xs={12} sm={3}>

                                    <label>Current</label>

                                </Grid>
                                <Grid item xs={12} sm={8}>
                                    <input
                                        style={{ width: '200px', height: '28px' }}
                                        label='current password'
                                        name='currentpassword'
                                        type='password'
                                        placeholder="current password"
                                        value={fields.currentpassword}
                                        onChange={handleChange}
                                        margin="normal"
                                        // error={filedError.currentpassword !== ""}

                                        required
                                    />

                                </Grid>
                                <Grid item xs={12} sm={3}>
                                    <label >New</label>
                                </Grid>
                                <Grid item xs={12} sm={4}>
                                    <input
                                        style={{ width: '200px', height: '28px' }}
                                        label='New password'
                                        name='newpassword'
                                        type='password'
                                        placeholder="New password"
                                        value={fields.newpassword}
                                        onChange={handleChange}
                                        margin="normal"

                                    />
                                    <div>{filedError.newpassword !== "" ? <span style={{ color: 'red' }}>{filedError.newpassword}</span> : ""}</div>
                                </Grid>
                                <Grid item xs={12} sm={4}>
                                    <PasswordStrengthBar password={fields.newpassword} />
                                </Grid>

                                <Grid item xs={12} sm={3}>
                                    <label >Retype new</label>
                                </Grid>
                                <Grid item xs={12} sm={8}>
                                    <input
                                        style={{ width: '200px', height: '28px' }}

                                        label='Retype password'
                                        name='retypenewpassword'
                                        type='password'
                                        placeholder="Retype password"
                                        value={fields.retypenewpassword}
                                        onChange={handleChange}
                                        margin="normal"
                                    // error={filedError.currentpassword !== ""}


                                    />
                                    <div>{filedError.retypenewpassword !== "" ? <span style={{ color: 'red' }}>{filedError.retypenewpassword}</span> : ""}</div>
                                </Grid>
                            </Grid>
                            <DialogActions>
                                <Button
                                    type="submit"
                                    variant="contained"
                                    color="primary"
                                    disabled={!isEmpty || isError}
                                >
                                    save changes
                                   </Button>
                            </DialogActions>



                        </form>
                    </Paper>
                </div>

            </Container>


        </div>
    )
}
export default ResetPassword;






