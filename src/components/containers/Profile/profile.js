import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';
import Avatar from '@material-ui/core/Avatar';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';
import EditIcon from '@material-ui/icons/Edit';
import IconButton from '@material-ui/core/IconButton';
import DoneIcon from '@material-ui/icons/Done';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import { Button, Divider } from '@material-ui/core';
import Avata from 'react-avatar';
import Badge from '@material-ui/core/Badge';
import { profildata } from "../../redux/action-creators/profileinfo";
import PhotoCameraOutlinedIcon from '@material-ui/icons/PhotoCameraOutlined';
import { connect } from 'react-redux';
import Cookies from 'universal-cookie';
import RoleMaping from './rolemapping';
import Groups from './groups'
import Axios from 'axios';
import Breadcrumbs from '@material-ui/core/Breadcrumbs';
import NavigateNextIcon from '@material-ui/icons/NavigateNext';
import { BrowserRouter as Router, Route, Link } from "react-router-dom";
import Container from '@material-ui/core/Container';
import CssBaseline from '@material-ui/core/CssBaseline';
import CloseIcon from '@material-ui/icons/Close';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
//import ImageUploader from 'react-images-upload';
import Avatars from 'react-avatar-edit'
const passwordRegex = RegExp(/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,}$/)
const emailRegex = RegExp(/^[^@]+@[^@]+\.[^@]+$/)
const phoneRegex = RegExp(/^[(]{0,1}[0-9]{3}[)]{0,1}[-\s\.]{0,1}[0-9]{3}[-\s\.]{0,1}[0-9]{4}$/)
function TabPanel(props) {
    const { children, value, index, ...other } = props;

    return (
        <div
            role="tabpanel"
            hidden={value !== index}
            id={`simple-tabpanel-${index}`}
            aria-labelledby={`simple-tab-${index}`}
            {...other}
        >
            {value === index && (
                <Box p={3}>
                    {children}
                </Box>
            )}
        </div>
    );
}

TabPanel.propTypes = {
    children: PropTypes.node,
    index: PropTypes.any.isRequired,
    value: PropTypes.any.isRequired,
};

function a11yProps(index) {
    return {
        id: `simple-tab-${index}`,
        'aria-controls': `simple-tabpanel-${index}`,
    };
}
const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1,
        //backgroundColor: theme.palette.background.paper,
        // textAlign: 'center'

        //backgroundColor: theme.palette.background.paper,

    },
    avat: {
        //flexGrow: 1,
        flex: 1,
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
    },
    paper: {
        padding: theme.spacing(1),
        //textAlign: 'center',
        color: theme.palette.text.secondary,
    },
    icon: {
        marginTop: '10px',
        marginBottom: '10px'
    },
    cheicon: {
        marginTop: '-4px',
        float: 'right',
        color: '#757272'
    },
    cheicon1: {
        marginTop: '-4px',

        color: '#757272'
    },
    paper2: {
        marginTop: theme.spacing(1),
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
    },

}));


const Profile = (props) => {
    const classes = useStyles();
    const [value, setValue] = React.useState(0);
    const [file, setFile] = useState(undefined);


    const handleChangef = (event) => {
        setFile(URL.createObjectURL(event.target.files[0]));
        //
        //     if (elem.target.files[0].size > 71680) {
        //         alert("File is too big!");
        //         elem.target.value = "";

        //     }else if(elem.target.files[0].size < 71680) {
        //         setsrcs(elem.target.files[0])}
        //         console.log(elem.target.files[0])
        // }
        const [file] = event.target.files;
        if (file.size > 200000) {
            alert("File size should be less than 200kb");
            // const formData = new FormData();
            // formData.append('file', event.target.files[0]);
            // // let users = JSON.parse(localStorage.getItem('sbmUserId'))
            // // let domain = users.substring(users.lastIndexOf("@") + 1);
            // fetch(`https://manager-api.collabkare.com/user/uploadFile?sbmUserId=admin@collabnotes.com`, {
            //     method: 'put',
            //     body: formData,
            //     // headers: {
            //     //     Authorization: window.fhirClient && window.fhirClient.server && window.fhirClient.server.auth ? `Bearer ${window.fhirClient.server.auth.token}` : undefined,

            //     //     "Accept": "application/json",

            //     // },
            // }).then(res => {
            //     if (res.ok) {
            //         console.log(res);
            //         alert("File uploaded successfully.")
            //     }
            // }).catch(err => console.log(err));
            // // console.log(URL.createObjectURL(file))
        } else if (file.size < 200000) {
            const formData = new FormData();
            formData.append('file', event.target.files[0]);
            // let users = JSON.parse(localStorage.getItem('sbmUserId'))
            // let domain = users.substring(users.lastIndexOf("@") + 1);
            fetch(`https://manager-api.collabkare.com/user/uploadFile?sbmUserId=admin@collabnotes.com`, {
                method: 'put',
                body: formData,
                // headers: {
                //     Authorization: window.fhirClient && window.fhirClient.server && window.fhirClient.server.auth ? `Bearer ${window.fhirClient.server.auth.token}` : undefined,

                //     "Accept": "application/json",

                // },
            }).then(res => {
                if (res.ok) {
                    console.log(res);
                    alert("File uploaded successfully.")
                }
            }).catch(err => console.log(err));
            // console.log(URL.createObjectURL(file))
        }




        //
    }
    const handleChange = (event, newValue) => {
        setValue(newValue);
    };
    // useEffect(() => {
    //     props.profildata()
    // }, [])
    const [profile, setProfile] = useState([])
    useEffect(() => {
        setProfile(props.profile)

    }, [props.profile])


    const [open, setOpen] = React.useState(false);

    const handleClickOpen = () => {
        setOpen(true);
    };

    const handleClose = () => {
        setOpen(false);
    };
    const handle = (e) => {
        e.preventDefault();
        const cookies = new Cookies();
        var cooki = cookies.get('iPlanetDirectoryPro')
        var email = cookies.get('email');
        var domain = cookies.get('domain');

        Axios({
            method: 'put',
            url: `https://account-api.collabkare.com/userinfo/${domain}/${email}`,
            data: {
                givenName: currentUser.givenName,
                sn: currentUser.sn,
                mail: currentUser.mail,
                postalAddress: currentUser.postalAddress,
                telephoneNumber: currentUser.telephoneNumber,
                userpassword: currentUser.userpassword
            },

            headers: { 'token': cooki }
        })
            .then(function (response) {
                console.log(response);
            })
            .catch(function (error) {
                console.log(error);
            });
        setCurrentUser(initialFormState)
        setuser(initialFormState)
    }
    const initialFormState = {

        givenName: "",
        sn: "",
        mail: "",
        postalAddress: "",
        telephoneNumber: "",
        userpassword: ""
    }
    const [user, setuser] = useState(currentUser)
    const [currentUser, setCurrentUser] = useState(initialFormState)
    const editRow = res => {
        setCurrentUser({ givenName: res.givenName, sn: res.sn, mail: res.mail, postalAddress: res.postalAddress, telephoneNumber: res.telephoneNumber, userpassword: res.userpassword })
        setOpen(true);


    }
    const [filedError, setFieldError] = useState({
        ...currentUser
    })
    const formErrors = { ...filedError }
    const [isError, setIsError] = useState(false)

    const handleInputChange = event => {
        const { name, value } = event.target
        const lengthValidate = value.length > 0 && value.length < 3
        switch (name) {
            case "givenName":
                formErrors.givenName = lengthValidate
                    ? "required"
                    : ""

                break
            case "sn":
                formErrors.sn = lengthValidate
                    ? "Minimum 3 characaters required"
                    : ""
                break
            case "postalAddress":
                formErrors.postalAddress = lengthValidate
                    ? "Minimum 3 characaters required"
                    : ""
                break
            case "telephoneNumber":
                formErrors.telephoneNumber = phoneRegex.test(value)
                    ? ""
                    : "Must be with formats: 123-456-7890 (123)456-7890 1234567890 123.456.7890."

                break
            case "mail":
                formErrors.mail = emailRegex.test(value) ? "" : "Email id is invalid"

                break

            default:
                break
        }
        setFieldError({
            ...formErrors

        })
        formErrors.givenName.length !== 0 ? setIsError(true)
            : formErrors.sn.length !== 0 ? setIsError(true) :
                formErrors.postalAddress.length !== 0 ? setIsError(true) :
                    formErrors.telephoneNumber.length !== 0 ? setIsError(true) :
                        setIsError(false)

        setCurrentUser({ ...currentUser, [name]: value })



    }



    //passwords fields
    const [opens, setOpens] = React.useState(false);
    const handleClickOpens = () => {
        setOpens(true);
    };
    const handleCloses = () => {
        setOpens(false);
    };
    const initialstate = {

        currentpassword: "",
        updatepassword: ""

    }
    const [pass, updatepass] = useState(initialstate)
    const [filed, setFields] = useState({
        ...pass
    })
    const formErr = { ...filed }
    const [err, seterr] = useState(false)
    const handleInputChanges = event => {
        const { name, value } = event.target
        switch (name) {

            case "updatepassword":
                formErr.updatepassword = passwordRegex.test(value) ? "" : "Minimum eight characters, at least one uppercase letter, one lowercase letter, one number and one special character"
                break
            default:
                break
        }

        setFields({
            ...formErr

        })
        // set error hook
        Object.values(formErr).forEach(error =>
            error.length > 0 ? seterr(true) : seterr(false)
        )
        updatepass({ ...pass, [name]: value })

    }
    const Emp =
        pass.updatepassword.length > 0
    const passwordsubmit = (e) => {
        e.preventDefault();
        const cookies = new Cookies();
        var cooki = cookies.get('iPlanetDirectoryPro')
        var email = cookies.get('email');
        var domain = cookies.get('domain');
        Axios({
            method: 'post',
            url: `https://account-api.collabkare.com/updatePassword/${domain}/${email}`,
            data: {

                "currentpassword": pass.currentpassword,
                "userpassword": pass.updatepassword

            },

            headers: { 'token': cooki }
        })
            .then(function (response) {
                console.log(response);
            })
            .catch(function (error) {
                console.log(error);
            });
        updatepass(initialstate)

    }

    //  const isEmpty = 
    //  currentUser.givenName.length>0 && currentUser.sn.length>0 && currentUser.mail.length>0 && currentUser.postalAddress.length>0 &&currentUser.telephoneNumber.length>0

    const [passs, uppass] = React.useState('')
    React.useEffect(() => {
        const cookies = new Cookies();
        let url = `https://account-api.collabkare.com/userid/${cookies.get('domain')}/${cookies.get('email')}`
        Axios({
            method: 'get',
            url: url,
            headers: { token: cookies.get('iPlanetDirectoryPro') },
        })

            .then(function (response) {
                // console.log(response);
                uppass(response.data)

            })
            .catch(function (error) {
                console.log(error);

            });


    }, [])
    const [hosp, sethosp] = React.useState('')
    React.useEffect(() => {
        const cookies = new Cookies();

        let url = `https://manager-api.collabkare.com/user/getUserSandboxes?sbmUserId=${cookies.get('email')}`
        Axios({
            method: 'get',
            url: url,
            //headers: { token: cookies.get('iPlanetDirectoryPro') },
        })

            .then(function (response) {
                // console.log(response);
                sethosp(response.data)

            })
            .catch(function (error) {
                console.log(error);

            });


    }, [])
    const [preview, setpreview] = React.useState()
    const [srcs, setsrcs] = React.useState('../../../../../src/assets/images/Camera.svg')
    const onClose = () => {
        setpreview(null)
    }

    const onCrop = (preview) => {
        setpreview(preview)
    }

    const onBeforeFileLoad = (elem) => {
        if (elem.target.files[0].size > 71680) {
            alert("File is too big!");
            elem.target.value = "";

        } else if (elem.target.files[0].size < 71680) {
            setsrcs(elem.target.files[0])
        }
        console.log(elem.target.files[0])
    }
    const cookies = new Cookies();
    if (cookies.get('iPlanetDirectoryPro') == undefined) {
        return window.location = '/login'
    }

    return (
        <div className={classes.root}>
            {/* {console.log(passs)} */}


            <div style={{ marginLeft: "16px" }}>
                <Breadcrumbs separator={<NavigateNextIcon />} aria-label="breadcrumb">
                    <Link to={"home"} style={{ textDecoration: "none", color: '#2D6BB2', fontSize: '15px' }}>
                        Home
              </Link>
                    <Link to={"#"} style={{ textDecoration: "none", color: 'black', fontSize: '13px' }}>
                        Profile
               </Link>

                </Breadcrumbs></div>
            <br />

            <div className={classes.avat}>
                {!props.loading && profile !== [] ?
                    profile.map(res =>
                        <div key={res._id} style={{ display: 'flex', marginTop: '5px' }}>

                            <div >
                                <Badge
                                    overlap="circle"
                                    anchorOrigin={{
                                        vertical: 'bottom',
                                        horizontal: 'right',
                                        // right: '-106px'
                                    }}
                                    badgeContent={
                                        <form>
                                            <input
                                                accept="image/*"
                                                style={{ display: 'none' }}
                                                id="raised-button-file"
                                                type="file"
                                                onChange={handleChangef}
                                            />
                                            {/* preview of file */}
                                            {/* {file && <img src={file} />} */}
                                            <label htmlFor="raised-button-file">
                                                <img style={{ position: 'sticky' }} src="../../../../../src/assets/images/Camera.svg" />

                                            </label>
                                        </form>}
                                >
                                    <Avata size="100" name={res.username} src={file} round={true} />
                                </Badge>
                                {/* <Avatars
                                    width={100}
                                    height={200}
                                    onCrop={onCrop}
                                    onClose={onClose}
                                    onBeforeFileLoad={onBeforeFileLoad}
                                    src={srcs}
                                /> */}
                                {/* <img src={preview} alt="Preview" /> */}
                            </div>
                            <div style={{ marginLeft: '8px' }}>
                                <h3>{res.username}</h3>
                                {/* <h3>{res.username}</h3> */}
                                <p style={{ fontSize: '13px' }}>01/01/2000  20 years</p>
                                <p style={{ fontSize: '13px' }}>Male</p>
                            </div>
                        </div>) : ''}
            </div>

            <br />
            <Tabs value={value}
                indicatorColor="primary"
                textColor="primary"
                centered
                onChange={handleChange} aria-label="simple tabs example">
                <Tab label="Details" {...a11yProps(0)} />
                <Tab label="Credentials" {...a11yProps(1)} />
                <Tab label="Roles" {...a11yProps(2)} />
                <Tab label="Assigned hospitals" {...a11yProps(3)} />

            </Tabs>
            <TabPanel value={value} index={0}>

                <React.Fragment>
                    {/* <Dialog open={open} onClose={handleClose} aria-labelledby="form-dialog-title">
                        <DialogTitle id="form-dialog-title" style={{ backgroundColor: 'rgb(0, 64, 135)', textAlign: 'center', color: 'white' }}>Edit <CloseIcon style={{ float: 'right' }} onClick={handleClose} /></DialogTitle> */}


                    {open ?
                        <Grid
                            container
                            spacing={0}
                            direction="column"
                            alignItems="center"
                            justify="center"
                        // style={{ minHeight: '100vh' }}
                        > <form noValidate autoComplete="off" style={{ maxWidth: '500px' }} onSubmit={handle} >
                                <DialogContent>

                                    <TextField id="standgivenName"
                                        label="Given name"
                                        margin='dense'
                                        name='givenName'
                                        value={currentUser.givenName}
                                        onChange={handleInputChange}
                                        variant="outlined"
                                        fullWidth
                                        error={filedError.givenName !== ""}
                                        helperText={filedError.givenName !== "" ? `${filedError.givenName}` : ""}
                                    />
                                    <TextField
                                        id="sn"
                                        label="sn"
                                        margin='dense'
                                        name='sn'
                                        onChange={handleInputChange}
                                        value={currentUser.sn}
                                        variant="outlined"
                                        fullWidth
                                        error={filedError.sn !== ""}
                                        helperText={filedError.sn !== "" ? `${filedError.sn}` : ""}
                                    />
                                    <TextField
                                        id="mail"
                                        label="mail"
                                        margin='dense'
                                        name='mail'
                                        onChange={handleInputChange}
                                        value={currentUser.mail}
                                        variant="outlined"
                                        fullWidth
                                        error={filedError.mail !== ""}
                                        helperText={filedError.mail !== "" ? `${filedError.mail}` : ""}
                                    />
                                    <TextField
                                        id="postalAddress"
                                        label="postalAddress"
                                        margin='dense'
                                        name='postalAddress'
                                        onChange={handleInputChange}
                                        value={currentUser.postalAddress}
                                        variant="outlined"
                                        fullWidth
                                        error={filedError.postalAddress !== ""}
                                        helperText={filedError.postalAddress !== "" ? `${filedError.postalAddress}` : ""}
                                    />
                                    <TextField
                                        id="telephoneNumber"
                                        label="telephoneNumber"
                                        margin='dense'
                                        name='telephoneNumber'
                                        onChange={handleInputChange}
                                        value={currentUser.telephoneNumber}
                                        variant="outlined"
                                        fullWidth
                                        error={filedError.telephoneNumber !== ""}
                                        helperText={filedError.telephoneNumber !== "" ? `${filedError.telephoneNumber}` : ""}
                                    />
                                    <TextField
                                        id="userpassword"
                                        label="userpassword"
                                        margin='dense'
                                        name='userpassword'
                                        onChange={handleInputChange}
                                        value={currentUser.userpassword}
                                        variant="outlined"
                                        fullWidth
                                        error={filedError.userpassword !== ""}
                                        helperText={filedError.userpassword !== "" ? `${filedError.userpassword}` : ""}
                                    />


                                </DialogContent>
                                <DialogActions>
                                    <Button onClick={handleClose} variant='outlined'>
                                        Cancel
                                           </Button>
                                    <Button disabled={isError} variant='outlined' type='submit' style={{ textTransform: 'none' }}>submit</Button>
                                </DialogActions>
                            </form>
                        </Grid> :


                        <div style={{ width: '550px', marginLeft: 'auto', marginRight: 'auto' }}>
                            {!props.loading && profile !== [] ?
                                profile.map(res => <div key={res._id}>
                                    <div style={{ marginLeft: '450px' }}>
                                        <Button variant='outlined' style={{ textTransform: 'none' }} onClick={() => editRow(res)}>Edit</Button>
                                    </div>
                                    <Divider className={classes.icon} />
                                    <Grid container spacing={1} >
                                        {/* <Grid container item xs={12} spacing={1} > */}
                                        <Grid item xs={6}>
                                            ID
                                </Grid>
                                        <Grid item xs={6}>
                                            0000-8989899-889
                                </Grid>

                                        {/* </Grid> */}
                                    </Grid>

                                    <Divider className={classes.icon} />



                                    <Grid container spacing={1}>
                                        {/* <Grid container item xs={12} spacing={3}> */}
                                        <Grid item xs={6}>
                                            User Name
                                </Grid>
                                        <Grid item xs={6}>
                                            {res.username}
                                        </Grid>
                                        {/* <Grid item xs={4}>
                                          
                                            <EditIcon className={classes.cheicon} onClick={() => editRow(res)} />
                                        </Grid> */}
                                        {/* </Grid> */}
                                    </Grid>

                                    <Divider className={classes.icon} />
                                    <Grid container spacing={1}>
                                        {/* <Grid container item xs={12} spacing={3}> */}
                                        <Grid item xs={6}>
                                            Gender
                                </Grid>
                                        <Grid item xs={6}>
                                            Male
                                </Grid>

                                        {/* </Grid> */}
                                    </Grid>
                                    <Divider className={classes.icon} />
                                    <Grid container spacing={1}>
                                        {/* <Grid container item xs={12} spacing={3}> */}
                                        <Grid item xs={6}>
                                            Date of Birth
                                </Grid>
                                        <Grid item xs={6}>
                                            01/01/2000
                                </Grid>

                                        {/* </Grid> */}
                                    </Grid>
                                    <Divider className={classes.icon} />
                                    <Grid container spacing={1}>
                                        {/* <Grid container item xs={12} spacing={3}> */}
                                        <Grid item xs={6}>
                                            Address Line 1
                                </Grid>
                                        <Grid item xs={6}>
                                            Sample Address
                                </Grid>
                                        {/* <Grid item xs={4}>
                                            <EditIcon className={classes.cheicon} onClick={() => editRow(res)} />
                                        </Grid> */}
                                        {/* </Grid> */}
                                    </Grid>
                                    <Divider className={classes.icon} />
                                    <Grid container spacing={1}>
                                        {/* <Grid container item xs={12} spacing={3}> */}
                                        <Grid item xs={6}>
                                            Address Line 2
                                </Grid>
                                        <Grid item xs={6}>
                                            Sample Address
                                </Grid>

                                        {/* </Grid> */}
                                    </Grid>
                                    <Divider className={classes.icon} />
                                    <Grid container spacing={1}>
                                        {/* <Grid container item xs={12} spacing={3}> */}
                                        <Grid item xs={6}>
                                            Zip code
                                </Grid>
                                        <Grid item xs={6}>
                                            12345
                                </Grid>

                                        {/* </Grid> */}
                                    </Grid>
                                    <Divider className={classes.icon} />
                                    <Grid container spacing={1}>
                                        {/* <Grid container item xs={12} spacing={3}> */}
                                        <Grid item xs={6}>
                                            City
                                </Grid>
                                        <Grid item xs={6}>
                                            city Name
                                </Grid>

                                        {/* </Grid> */}
                                    </Grid>
                                    <Divider className={classes.icon} />
                                    <Grid container spacing={1}>
                                        {/* <Grid container item xs={12} spacing={3}> */}
                                        <Grid item xs={6}>
                                            State/Province/Region
                                </Grid>
                                        <Grid item xs={6}>
                                            state Name
                                </Grid>

                                        {/* </Grid> */}
                                    </Grid>
                                    <Divider className={classes.icon} />


                                    <Grid container spacing={1}>
                                        {/* <Grid container item xs={12} spacing={3}> */}
                                        <Grid item xs={6}>
                                            Country
                                </Grid>
                                        <Grid item xs={6}>
                                            Country Name
                                </Grid>

                                        {/* </Grid> */}
                                    </Grid>
                                    <Divider className={classes.icon} />
                                    <Grid container spacing={1}>
                                        {/* <Grid container item xs={12} spacing={3}> */}
                                        <Grid item xs={6}>
                                            Phone
                                </Grid>
                                        <Grid item xs={6}>9934652348

                                        </Grid>
                                        {/* <Grid item xs={4}>
                                            <EditIcon className={classes.cheicon} onClick={() => editRow(res)} />
                                        </Grid> */}
                                        {/* </Grid> */}
                                    </Grid>
                                    <Divider className={classes.icon} />
                                    {/* <Grid container spacing={1}>
                                 
                                        <Grid item xs={6}>
                                         Password
                                </Grid>
                                        <Grid item xs={4}>
                                           ************
                                            
                                        </Grid>
                                        <Grid item xs={2}>
                                            <EditIcon className={classes.cheicon} onClick={handleClickOpens}/>
                                        </Grid>
                                   
                                </Grid> */}

                                    <Dialog
                                        open={opens}
                                        onClose={handleCloses}
                                        aria-labelledby="alert-dialog"
                                        aria-describedby="alert-dialo"
                                    >
                                        <DialogTitle id="form-dialog" style={{ backgroundColor: 'rgb(0, 64, 135)', textAlign: 'center', color: 'white' }}>Update Password <CloseIcon style={{ float: 'right' }} onClick={handleCloses} /></DialogTitle>

                                        <form noValidate autoComplete="off" onSubmit={passwordsubmit} >
                                            <DialogContent>
                                                <TextField id="cpassword"
                                                    label="current Password"
                                                    margin='dense'
                                                    name='currentpassword'
                                                    value={pass.currentpassword}
                                                    onChange={handleInputChanges}
                                                    variant="outlined"
                                                    fullWidth

                                                />
                                                <TextField id="password"
                                                    label="New Password"
                                                    margin='dense'
                                                    name='updatepassword'
                                                    value={pass.updatepassword}
                                                    onChange={handleInputChanges}
                                                    variant="outlined"
                                                    fullWidth
                                                    error={formErr.updatepassword !== ""}
                                                    helperText={formErr.updatepassword !== "" ? `${formErr.updatepassword}` : ""}

                                                />
                                            </DialogContent>

                                            <DialogActions>
                                                <Button onClick={handleCloses} color="primary" variant='outlined'>
                                                    cancel
                                                  </Button>
                                                <Button type='submit' disabled={!Emp || err} autoFocus variant='outlined'>
                                                    update
                                               </Button>

                                            </DialogActions>
                                        </form>
                                    </Dialog>
                                </div>)

                                : <p>loading....</p>}
                        </div>}
                </React.Fragment>
            </TabPanel>
            <TabPanel value={value} index={1}>
                <React.Fragment>
                    <Container component="main" maxWidth="md">
                        <div
                        // style={{ width: '800px', marginLeft: 'auto', marginRight: 'auto' }}
                        >
                            <Paper style={{ padding: '10px' }}>
                                <p style={{ fontSize: '17px', fontWeight: 'bold' }}>Signing in to Collabkare </p>

                                <Grid container spacing={6}>
                                    <Grid container item xs={12} spacing={3}>
                                        <Grid item xs={4}>
                                            <span style={{ fontSize: '13px', color: '#6D7278' }}>Password</span>
                                        </Grid>
                                        <Grid item xs={4}>
                                            <span style={{ fontSize: '14px' }}>Last Changed on {passs.date}</span>
                                        </Grid>
                                        <Grid item xs={4}>
                                            <Link to='/resetpassword' style={{ textDecoration: 'none', color: '#0055B4', fontSize: '14px' }}>
                                                <ChevronRightIcon className={classes.cheicon} />
                                            </Link>
                                        </Grid>
                                    </Grid>
                                </Grid>
                                <Divider className={classes.icon} />
                                <Grid container spacing={6}>
                                    <Grid container item xs={12} spacing={3}>
                                        <Grid item xs={4}>
                                            <span style={{ fontSize: '13px', color: '#6D7278' }}>Two Step verification</span>

                                        </Grid>
                                        <Grid item xs={4}>
                                            <span style={{ display: 'flex', fontSize: '14px' }}> <Avatar style={{ width: '24px', height: '24px', backgroundColor: 'rgb(66, 133, 244)', marginTop: '-4px' }}><DoneIcon /></Avatar> &nbsp; on</span>
                                        </Grid>
                                        <Grid item xs={4}>
                                            <Link to='/twostepverification' style={{ textDecoration: 'none', color: '#0055B4', fontSize: '14px' }}>
                                                <ChevronRightIcon className={classes.cheicon} />
                                            </Link>
                                        </Grid>
                                    </Grid>
                                </Grid>

                            </Paper>
                            <br />
                            {/* <Divider className={classes.icon} /> */}
                            {/* <Paper style={{padding:'10px'}}>
                        <p style={{fontSize:'17px',fontWeight:'bold'}}>Ways we can verify it's you </p>
                     

                        <p style={{fontSize:'12px',color:'#6D7278'}}>These can be used to make sure it's really you signing you or to reach you if there's suspicious activity in your account</p>
                        <Grid container spacing={6}>
                            <Grid container item xs={12} spacing={3}>
                                <Grid item xs={4}>
                                <span style={{fontSize:'13px',color:'#6D7278'}}>Recovery phone</span> 
                                    
                                </Grid>
                                <Grid item xs={4}>
                                    <span style={{ display: 'flex',fontSize:'14px' }}>000-000-0000</span>
                                    
                                </Grid>
                               
                            </Grid>
                        </Grid>
                        <Divider className={classes.icon} />
                                <Grid container spacing={6}>
                                    <Grid container item xs={12} spacing={3}>
                                        <Grid item xs={4}>
                                            <span style={{ fontSize: '13px', color: '#6D7278' }}>Recovery email</span>

                                        </Grid>
                                        <Grid item xs={4}>
                                            <span style={{ display: 'flex', fontSize: '14px' }}>admin@demo.com</span>

                                        </Grid>
                                      
                                    </Grid>
                                </Grid>
                            </Paper> */}
                            {/* <Divider className={classes.icon} /> */}
                            {/* <br /> */}
                            <Paper style={{ padding: '10px' }}>
                                <p style={{ fontSize: '17px', fontWeight: 'bold' }}>Recent security activity</p>

                                <p style={{ fontSize: '12px', color: '#6D7278' }}>No activity in the last 28 days.You'll be notified if unusual security is detected,like a sign-in form a new device or if a sensitive setting is changed in your account</p>
                            </Paper>
                        </div>
                    </Container>
                </React.Fragment>
            </TabPanel>
            <TabPanel value={value} index={2}>
                <React.Fragment>

                    <Container component="main" maxWidth="sm">
                        {/* <div style={{ width: "800px", margin: 'auto' }}> */}
                        <div className={classes.paper2}>
                            {/* <Grid container spacing={2}>
                                <Grid item xs={5} md={5}>

                                </Grid>
                                <Grid item xs={6} md={6}> */}

                            <h3>assign roles</h3>
                            <div>
                                {!props.loading && profile !== [] ?
                                    <div >
                                        {profile.map(res => <div key={res._id}>
                                            <span>
                                                {res.roles.map((option, i) =>
                                                    <h4 key={`Key${i}`} style={{ display: 'flex' }}><Avatar style={{ width: '24px', height: '24px', backgroundColor: 'rgb(66, 133, 244)', marginTop: '-2px' }}><DoneIcon /></Avatar>&nbsp; {option}</h4>

                                                )}
                                            </span>

                                        </div>)}
                                    </div>
                                    : ""}
                                {/* <h4 style={{display:'flex'}}><Avatar style={{ width: '24px', height: '24px', backgroundColor: 'rgb(66, 133, 244)' ,marginTop:'-2px'}}><DoneIcon /></Avatar>&nbsp; Super Admin</h4> */}

                            </div>
                            {/* </Grid>
                            </Grid> */}
                        </div>
                        {/* </div> */}
                    </Container>
                </React.Fragment>
                {/* <RoleMaping /> */}


            </TabPanel>
            <TabPanel value={value} index={3}>
                <Grid
                    container
                    direction="row"
                    justify="center"
                    alignItems="center"
                >
                    <div>
                        {hosp !== '' && hosp !== [] && hosp !== undefined ?
                            //  hosp.map(res => res.sandboxId)

                            <span>
                                {hosp.map((option, i) =>
                                    <h5 key={`Key${i}`}> {option.sandboxId}</h5>

                                )}
                            </span>



                            : ''}

                    </div>
                    {/* <Groups /> */}
                </Grid>
            </TabPanel>

        </div>

    )
}

const mapStateToProps = state => {
    return {
        error: state.profileinfo.error,
        profile: state.profileinfo.profile,
        loading: state.profileinfo.loading
    }
}
const mapDispatchToProps = dispatch => {
    return {
        profildata: () => {
            dispatch(profildata());
        },

    };
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Profile);




