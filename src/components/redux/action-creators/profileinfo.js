import * as types from "../action-creators/types";
import axios from 'axios';
import Cookies from 'universal-cookie';
const profileSuccess = data => ({
    type: types.USER_INFO_SUCCESS,
    payload: {
        ...data
    }
});
const profileRequest = () => ({
    type: types.USER_INFO_STARTED
});
const profileFailure = error => ({
    type: types.USER_INFO_FAILURE,
    payload: {
        error
    }
});
export const profildata = () => {
   


    return (dispatch, getState) =>{
        const url = getState()
        const cookies = new Cookies();

        var cooki =cookies.get('iPlanetDirectoryPro') 
        var id =cookies.get('email')
        var domain=cookies.get('domain');
        console.log(cooki)
        dispatch(profileRequest());
        axios.get(`https://account-api.collabkare.com/userinfo/${domain}/${id}`, {
            headers: {'token':cooki }
        })
            .then(res => {
                setTimeout(() => {
                    dispatch(profileSuccess(res.data));
                    console.log(res)
                }, 2500);
            })
            .catch(err => {
                dispatch(profileFailure(err.message));
                console.log(err)
            });
    };
};