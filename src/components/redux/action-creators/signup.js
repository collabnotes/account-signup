import * as types from "../action-creators/types";
import axios from 'axios';

export const signupsubmit = (values) => {
    console.log(values)

    // "signup_url": "https://account-api.collabkare.com/en_account",
    return (dispatch, getState) => {
        const url = getState()
        dispatch(signupRequest());
        axios.post(url.config.data[0].signup_url, {
            user: {
                email: values.email,
                // password: values.password,
                phone: values.phone,
                lastName: values.lastName,
                firstName: values.firstName
            },
            organization: {
                taxId: values.organisationtax,
                type: values.Tax,
                phone: values.companyphone,
                address: values.address,
                
                city: values.city,
                zipcode: values.zipcode,
                region: values.state,
                name: values.organisationname,
                createdBy: values.email,
                code: "",
            }
           

        })
            .then(res => {
                setTimeout(() => {
                    dispatch(signupSuccess(res.data));
                    console.log(res)
                }, 2500);
            })
            .catch(err => {
                dispatch(signupFailure(err.message));
                console.log(err)
            });
    };
};






export const signup = (values) => {

    return (dispatch, getState) => {
        const url = getState()
        dispatch(signupRequestbasic());
        axios.post(url.config.data[0].signup_url_demo, {

            email: values.email,
            password: values.password,
            phone: values.phone,
            givenName: values.firstName,
            lastName: values.lastName,
            firstName: values.firstName
        })
            .then(res => {
                setTimeout(() => {
                    dispatch(signupSuccessbasic(res.data));
                    console.log(res)
                }, 2500);
            })
            .catch(err => {
                dispatch(signupFailurebasic(err.message));
                console.log(err)
            });
    };
};
const signupSuccess = data => ({
    type: types.SIGNUP_SUCCESS,
    payload: {
        ...data
    }
});
const signupRequest = () => ({
    type: types.SIGNUP_REQUEST
});
const signupFailure = error => ({
    type: types.SIGNUP_FAILURE,
    payload: {
        error
    }
});

const signupSuccessbasic = data => ({
    type: types.SIGNUP_SUCCESS_BASIC,
    payload: {
        ...data
    }
});
const signupRequestbasic = () => ({
    type: types.SIGNUP_REQUEST_BASIC
});
const signupFailurebasic = error => ({
    type: types.SIGNUP_FAILURE_BASIC,
    payload: {
        error
    }
});
