
import * as types from "../action-creators/types";
// export const addTodo = () => {
//   return dispatch => {
//     dispatch(addTodoStarted());
//     console.log(data)
//     axios
//       .get(data,dispatch)


//       .then(res => {
//         dispatch(addTodoSuccess(res.data));
//         console.log('succ')
//       })
//       .catch(err => {
//         dispatch(addTodoFailure(err.message));
//         console.log('fail')
//       });
//   };
// };

const configSuccess = data => ({
  type: types.CONFIG_SUCCESS,
  payload: {
    ...data
  }
});

const configStarted = () => ({
  type: types.CONFIG_STARTED
});

// const addTodoFailure = error => ({
//   type: types.CONFIG_FAILURE,
//   payload: {
//     error
//   }
// });

export const config = () => {
  return (dispatch, getState) => {

     dispatch(configStarted());
    dispatch(configSuccess())
    console.log('current state:', getState());
    
    // ...
  };
};

