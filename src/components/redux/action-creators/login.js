
import * as types from "../action-creators/types";
import Cookies from 'universal-cookie';
import axios from 'axios';
import { Redirect, Route } from "react-router-dom";

const configuration = 'https://account-api.collabkare.com/';
const cookies = new Cookies();
//  let domaincookie = cookies.get('domain');

export const loginsubmit = (user) => {

    // const userdata = {
    //     "username": user.email,
    //     "password": user.password,
    // }

    return (dispatch, getState) => {
        const url = getState()
        const cookies = new Cookies();
        dispatch(loginRequest());

        axios.post(`${configuration}${'test'}/authenticate`
            //{cookies.get('domain')}
            , {
                "username": user.email,
                "password": user.password,
            }

        ).then(res => {
            dispatch(loginSuccess(res.data));
            console.log(res)
            {
                sessionStorage.setItem('userdata', JSON.stringify(res.data.callbacks[0].output[0].value).includes('One Time Password'));
            }
            // dispatch(login(res.data, userdata))

        })
            .catch(err => {
                dispatch(loginFailure(err.response.data.message));
                console.log(err)
            });


    };
};
const login = (res, userdata) => {
    return (dispatch, getState) => {
        const url = getState()

        dispatch(logindataRequest()); //https://cors-anywhere.herokuapp.com/
        axios.post(`${configuration}${cookies.get('domain')}/authenticate`, {

            "authId": res.authId,
            "callbacks": [
                {
                    "type": res.callbacks[0].type,
                    "output": [
                        {
                            "name": res.callbacks[0].output[0].name,
                            "value": res.callbacks[0].output[0].value
                        }
                    ],
                    "input": [
                        {
                            "name": res.callbacks[0].input[0].name,
                            "value": userdata.username
                        }
                    ],
                    "_id": res.callbacks[0]._id
                },
                {
                    "type": res.callbacks[1].type,
                    "output": [
                        {
                            "name": res.callbacks[1].output[0].name,
                            "value": res.callbacks[1].output[0].value
                        }
                    ],
                    "input": [
                        {
                            "name": res.callbacks[1].input[0].name,
                            "value": userdata.password
                        }
                    ],
                    "_id": res.callbacks[1]._id
                }
            ]

        })
            .then(res => {

                console.log(res)
                localStorage.setItem('userdata1', JSON.stringify(res.data.callbacks[0].output[1].value));
                localStorage.setItem('userdata', JSON.stringify(res.data.callbacks[0].output[1].value).includes('skip'));

                dispatch(logindataSuccess(res.data))
                const cookies = new Cookies();

                cookies.set('id', userdata.username)
            })
            .catch(err => {
                console.log(err)
                dispatch(logindataFailure(err.response.data.message))

            });
    }
};
export const mfalogin = (val) => {

    return (dispatch, getState) => {
        dispatch(mfaRequest());
        const data = getState()

        console.log(data)
        axios.post(`${configuration}${cookies.get('domain')}/authenticate`, {
            "authId": data.login.userdata[0].authId,
            "callbacks": [
                {
                    "type": "ChoiceCallback",
                    "output": [
                        {
                            "name": "prompt",
                            "value": "Please Choose a MFA Of Your Choice"
                        },
                        {
                            "name": "choices",
                            "value": [
                                "EMAIL",
                                "TOTP"
                            ]
                        },
                        {
                            "name": "defaultChoice",
                            "value": 0
                        }
                    ],
                    "input": [
                        {
                            "name": "IDToken1",
                            "value": val
                        }
                    ]
                }
            ]

        })
            .then(res => {
                console.log(res)
                setTimeout(() => {
                    dispatch(mfaSuccess(res.data))
                    if (res.data.callbacks[0].output[2].value.includes('Register device')) {
                        localStorage.setItem('register', JSON.stringify(res.data.callbacks[0].output[2].value).includes('Register device'));
                    }

                    //     const cookies = new Cookies();

                    //     cookies.set('iPlanetDirectoryPro', res.data.tokenId, {
                    //      domain: getTopLevelDomain(),
                    //      path: '/'
                    //    });
                    // Cookies.set('iPlanetDirectoryPro', res.data.tokenId, { path: '' });
                    // document.cookie = 'iPlanetDirectoryPro1' +"=" + res.data.tokenId +";expires=" + 'Thu, 01 Jan 1970 00:00:00 GMT'
                    // + ";domain=..collabkare.com;path=/";
                    // console.log('============------' + res.data.tokenId)
                }, 2500)
            })
            .catch(err => {
                dispatch(mfaFailure(err.response.data.message))
                console.log(err)
            });
    }
};

export const loginotp = (otp) => {
    console.log(otp)
    return (dispatch, getState) => {
        dispatch(loginotpRequest());
        const data = getState()
         console.log(data)
        axios.post(`${configuration}${cookies.get('domain')}/authenticate`, {

            "authId": data.login.mfa[0].authId,

            "callbacks": [
                {
                    "type": "PasswordCallback",
                    "output": [
                        {
                            "name": "prompt",
                            "value": "One Time Password"
                        }
                    ],
                    "input": [
                        {
                            "name": "IDToken1",
                            "value": otp.password
                        }
                    ]
                }
            ]


        })
            .then(res => {

                dispatch(loginotpSuccess(res))
                // Cookies.defaults = {
                //     path: "/",
                //     domain: ".collabkare.com",
                //     secure: false,
                //     expires: 365,
                //   };
                //  Cookies.settings({ expires: 1, domain:  '.collabkare.com' });
                //Cookies.set('iPlanetDirectoryPro', res.data.tokenId, { path: '/'});
                //Cookies.settings({domain: '.collabkare.com'})

                const cookies = new Cookies();
                //console.log('iPlanetDirectoryPro: '+ res.data.tokenId)
                cookies.set('iPlanetDirectoryPro', res.data.tokenId, {
                    domain: getTopLevelDomain(),
                    path: '/',
                    expires: new Date((new Date()).getTime() + 30 * 60000)
                });
                // var encodedKey2 = decodeURIComponent(window.location.href, "UTF-8");
                // var encodedKey3 = decodeURIComponent(encodedKey2, "UTF-8");
                // var url = new URL(encodedKey3);
                // var c = url.searchParams.get("redirect_uri");
                // {
                //     c !== null ? window.location = c
                //         : window.location = '/home'
                // }
                var encodedKey2 = decodeURIComponent(window.location.href, "UTF-8");
                var uu = new URL(encodedKey2)
                let params = new URLSearchParams(uu.search.slice(1));
                params.set('realm', cookies.get('domain'));
                let aa = params.toString()
                var dd = decodeURIComponent(aa, "UTF-8");
                var e = dd.split("?")
                var d = uu.searchParams.get("goto");
                var c = encodedKey2.includes("manager");
                var f = uu.searchParams.get("redirect_uri");
                var g = d == null ? null : d.split("?")

                {
                    f !== null && c && res.data.successUrl == '/am/console' ?
                        window.location = f :
                        f !== null && !c && g !== null && res.data.successUrl == "/am/console" ?
                            window.location = g[0] + "?" + e[1]
                            : res.data.successUrl == '/am/console' ? window.location = '/home' : ''

                }
            })
            .catch(err => {
                dispatch(loginotpFailure(err.response.data.message))
                console.log(err)
            });
    }
};


export function getTopLevelDomain() {
    if (window.location.hostname === 'localhost')
        return 'localhost';

    let domains = window.location.hostname.split('.');
    return `${domains[domains.length - 2]}.${domains[domains.length - 1]}`;
};
const loginSuccess = data => ({
    type: types.LOGIN_SUCCESS,
    payload: {
        ...data
    }
});

const loginRequest = () => ({
    type: types.LOGIN_REQUEST
});

const loginFailure = error => ({
    type: types.LOGIN_FAILURE,
    payload: {
        error
    }
});
const logindataSuccess = data => ({
    type: types.LOGIN_SUCCESS_USER,
    payload: {
        ...data
    }
});

const logindataRequest = () => ({
    type: types.LOGIN_REQUEST_USER
});

const logindataFailure = error => ({
    type: types.LOGIN_FAILURE_USER,
    payload: {
        error
    }
});

const loginotpSuccess = data => ({
    type: types.LOGIN_SUCCESS,
    payload: {
        ...data
    }
});

const loginotpRequest = () => ({
    type: types.LOGIN_REQUEST
});

const loginotpFailure = error => ({
    type: types.LOGIN_FAILURE,
    payload: {
        error
    }
});

const mfaSuccess = data => ({
    type: types.LOGIN_SUCCESS_MFA,
    payload: {
        ...data
    }
});

const mfaRequest = () => ({
    type: types.LOGIN_REQUEST_MFA
});

const mfaFailure = error => ({
    type: types.LOGIN_FAILURE_MFA,
    payload: {
        error
    }
});

const skipSuccess = data => ({
    type: types.LOGIN_SUCCESS_SKIP,
    payload: {
        ...data
    }
});

const skipRequest = () => ({
    type: types.LOGIN_REQUEST_SKIP
});

const skipFailure = error => ({
    type: types.LOGIN_FAILURE_SKIP,
    payload: {
        error
    }
});
export const skiplogin = (val) => {

    return (dispatch, getState) => {
        dispatch(skipRequest());
        const data = getState()

        axios.post(`${configuration}${cookies.get('domain')}/authenticate`, {

            "authId": data.login.userdata[0].authId,
            "callbacks": [
                {
                    "type": "ChoiceCallback",
                    "output": [
                        {
                            "name": "prompt",
                            "value": "Please Choose a MFA Of Your Choice"
                        },
                        {
                            "name": "choices",
                            "value": [
                                "EMAIL",
                                "TOTP"
                            ]
                        },
                        {
                            "name": "defaultChoice",
                            "value": 0
                        }
                    ],
                    "input": [
                        {
                            "name": "IDToken1",
                            "value": val
                        }
                    ]
                }
            ]

        })
            .then(res => {
                setTimeout(() => {
                    dispatch(skipSuccess(res.data))
                    console.log('res----------' + res)
                    const cookies = new Cookies();
                    //console.log('iPlanetDirectoryPro:'+ res.data.tokenId)
                    cookies.set('iPlanetDirectoryPro', res.data.tokenId, {
                        domain: getTopLevelDomain(),
                        path: '/',
                        expires: new Date((new Date()).getTime() + 30 * 60000)
                    });

                    //redirect-uri for root realm
                    // var encodedKey2 = decodeURIComponent(window.location.href, "UTF-8");
                    // var encodedKey3 = decodeURIComponent(encodedKey2, "UTF-8");
                    // var url = new URL(encodedKey3);
                    // var url1 = new URL(encodedKey2);
                    // var c = encodedKey2.includes("sand_man");
                    // var d = url1.searchParams.get("goto");
                    // var f = url.searchParams.get("redirect_uri");
                    // var e = encodedKey2.split("?")
                    // var g = d == null ? null : d.split("?")
                    // {
                    // f !== null && c ?
                    // window.location = f :
                    // f !== null && !c && g !== null ?
                    // window.location=g[0] + "?" + e[2]
                    // : window.location = '/home'

                    //                 }

                    var encodedKey2 = decodeURIComponent(window.location.href, "UTF-8");
                    var uu = new URL(encodedKey2)
                    let params = new URLSearchParams(uu.search.slice(1));
                    params.set('realm', cookies.get('domain'));
                    let aa = params.toString()
                    var dd = decodeURIComponent(aa, "UTF-8");
                    var e = dd.split("?")
                    var d = uu.searchParams.get("goto");
                    var c = encodedKey2.includes("manager");
                    var f = uu.searchParams.get("redirect_uri");
                    var g = d == null ? null : d.split("?")

                    // {
                    //     f !== null && c ?
                    //         window.location = f :
                    //         f !== null && !c && g !== null ?
                    //             window.location = g[0] + "?" + e[1]
                    //             : window.location = '/home'

                    // }
                    {
                        f !== null && c && res.data.successUrl == '/am/console' ?
                            window.location = f :
                            f !== null && !c && g !== null && res.data.successUrl == "/am/console" ?
                                window.location = g[0] + "?" + e[1]
                                : res.data.successUrl == '/am/console' ? window.location = '/home' : ''

                    }


                    console.log('============' + res.data.tokenId)
                }, 2500);
            })
            .catch(err => {
                dispatch(skipFailure(err.response.data.message))
                console.log(err)
            });
    }
};


const emailRequest = () => ({
    type: types.LOGIN_REQUEST_EMAIL
});

const emailFailure = error => ({
    type: types.LOGIN_FAILURE_EMAIL,
    payload: {
        error
    }
});

const emailSuccess = data => ({
    type: types.LOGIN_SUCCESS_EMAIL,
    payload: {
        ...data
    }
});

export const emaildata = (otps) => {

    return (dispatch, getState) => {
        dispatch(emailRequest());
        const data = getState()


        axios.post(`${configuration}${cookies.get('domain')}/authenticate`, {
            "authId": data.login.mfa[0].authId,
            "template": "",
            "stage": "AuthenticatorOATH7",
            "header": "ForgeRock Authenticator (OATH)",
            "callbacks": [
                {
                    "type": "NameCallback",
                    "output": [
                        {
                            "name": "prompt",
                            "value": "Enter verification code:"
                        }
                    ],
                    "input": [
                        {
                            "name": "IDToken1",
                            "value": otps
                        }
                    ]
                },
                {
                    "type": "ConfirmationCallback",
                    "output": [
                        {
                            "name": "prompt",
                            "value": ""
                        },
                        {
                            "name": "messageType",
                            "value": 0
                        },
                        {
                            "name": "options",
                            "value": [
                                "Submit",
                                "Skip this step"
                            ]
                        },
                        {
                            "name": "optionType",
                            "value": -1
                        },
                        {
                            "name": "defaultOption",
                            "value": 0
                        }
                    ],
                    "input": [
                        {
                            "name": "IDToken2",
                            "value": 0
                        }
                    ]
                }
            ]

        })
            .then(res => {
                setTimeout(() => {
                    dispatch(emailSuccess(res.data))
                    console.log('res----------' + res)
                    const cookies = new Cookies();
                    //console.log('iPlanetDirectoryPro:'+ res.data.tokenId)
                    cookies.set('iPlanetDirectoryPro', res.data.tokenId, {
                        domain: getTopLevelDomain(),
                        path: '/',
                        expires: new Date((new Date()).getTime() + 30 * 60000)
                        // 10 * 60000 ten minutes
                        // 2*60*60*1000 2 hrs

                    });



                    var encodedKey2 = decodeURIComponent(window.location.href, "UTF-8");
                    var uu = new URL(encodedKey2)
                    let params = new URLSearchParams(uu.search.slice(1));
                    params.set('realm', cookies.get('domain'));
                    let aa = params.toString()
                    var dd = decodeURIComponent(aa, "UTF-8");
                    var e = dd.split("?")
                    var d = uu.searchParams.get("goto");
                    var c = encodedKey2.includes("manager");
                    var f = uu.searchParams.get("redirect_uri");
                    var g = d == null ? null : d.split("?")


                    {
                        f !== null && c && res.data.successUrl == '/am/console' ?
                            window.location = f :
                            f !== null && !c && g !== null && res.data.successUrl == "/am/console" ?
                                window.location = g[0] + "?" + e[1]
                                : res.data.successUrl == '/am/console' ? window.location = '/home' : ''

                    }



                }, 2500);
            })
            .catch(err => {
                dispatch(emailFailure(err.response.data.message))
                console.log(err)
            });
    }
}