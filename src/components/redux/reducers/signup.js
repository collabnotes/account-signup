import * as types from "../action-creators/types";
const initialState = {
  hasloading: false,
  users: [],
  haserror: null,
  loading: false,
  user: [],
  
  
};

export default function signupReducer(state = initialState, action) {
  switch (action.type) {
    case types.SIGNUP_REQUEST:
      return {
        ...state,
        hasloading: true
      };
    case types.SIGNUP_SUCCESS:
      return {
        ...state, 
        hasloading: false,
        haserror: null,
        users: [...state.users, action.payload]
      };
    case types.SIGNUP_FAILURE:
      return {
        ...state,
        hasloading: false,
        haserror: action.payload.error
      };

      case types.SIGNUP_REQUEST_BASIC:
        return {
          ...state,
          loading: true
        };
      case types.SIGNUP_SUCCESS_BASIC:
        return {
          ...state, 
          loading: false,
          haserror: null,
          user: [...state.user, action.payload]
        };
      case types.SIGNUP_FAILURE_BASIC:
        return {
          ...state,
          loading: false,
          haserror: action.payload.error
        };
   
    default:
      return state;
  }
}