import * as types from "../action-creators/types";
const initialState = {
    loading: false,
    profile: [],
    error: null,
};

export default function profileinfo(state = initialState, action) {
    switch (action.type) {
        case types.USER_INFO_STARTED:
            return {
                ...state,
                loading: true,
                profile:[]
            };
        case types.USER_INFO_SUCCESS:
            return {
                ...state,
                loading: false,
                error: null,
                profile: [...state.profile, action.payload],
                
            };
        case types.USER_INFO_FAILURE:
            return {
                ...state,
                loading: false,
                error: action.payload.error
            };
        default:
            return state;
    }
}