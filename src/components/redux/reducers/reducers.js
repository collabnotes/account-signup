import { combineReducers } from "redux"

import config from "./config"
import login from './login'
import signup from './signup'
import profileinfo from './profileinfo'
const rootReducer = combineReducers({
config,
login,
signup,
profileinfo
})

export default rootReducer;

