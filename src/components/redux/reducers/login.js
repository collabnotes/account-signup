import * as types from "../action-creators/types";



const initialState = {
  loading: false,
  user: [],
  error: null,
  otploading: false,
  otpres: [],
  userdata: [],
  userloading: false,
  mfa: [],
  mfaloading: false,
  skip: [],
  skipload: false,
  emailload: false,
  emailres: []


};

export default function loginReducer(state = initialState, action) {
  switch (action.type) {
    case types.LOGIN_REQUEST:
      return {
        ...state,
        loading: true,
        user:[]
      };
    case types.LOGIN_SUCCESS:
      return {
        ...state,
        loading: false,
        error: null,

        user: [...state.user, action.payload]
      };
    case types.LOGIN_FAILURE:
      return {
        ...state,
        loading: false,
        error: action.payload.error
      };
    case types.LOGIN_REQUEST_USER:
      return {
        ...state,
        userloading: true,
        userdata: []
      };
    case types.LOGIN_SUCCESS_USER:
      return {
        ...state,
        userloading: false,
        error: null,

        userdata: [...state.userdata, action.payload]
      };
    case types.LOGIN_FAILURE_USER:
      return {
        ...state,
        userloading: false,
        error: action.payload.error
      };
    case types.LOGIN_REQUEST_OTP:
      return {
        ...state,
        otploading: true,
        otpres:[]
      };
    case types.LOGIN_SUCCESS_OTP:
      return {
        ...state,
        otploading: false,
        error: null,
        otpres: [...state.otpres, action.payload]
      };
    case types.LOGIN_FAILURE_OTP:
      return {
        ...state,
        otploading: false,
        error: action.payload.error
      };

    case types.LOGIN_REQUEST_MFA:
      return {
        ...state,
        mfaloading: true,
        mfa:[]
      };
    case types.LOGIN_SUCCESS_MFA:
      return {
        ...state,
        mfaloading: false,
        error: null,
        mfa: [...state.mfa, action.payload]
      };
    case types.LOGIN_FAILURE_MFA:
      return {
        ...state,
        mfaloading: false,
        error: action.payload.error
      };


    case types.LOGIN_REQUEST_SKIP:
      return {
        ...state,
        skipload: true,
        skip:[]
      };
    case types.LOGIN_SUCCESS_SKIP:
      return {
        ...state,
        skipload: false,
        error: null,

        skip: [...state.skip, action.payload]
      };
    case types.LOGIN_FAILURE_SKIP:
      return {
        ...state,
        skipload: false,
        error: action.payload.error
      };

    case types.LOGIN_REQUEST_EMAIL:
      return {
        ...state,
        emailload: true,
        emailres:[]
      };
    case types.LOGIN_SUCCESS_EMAIL:
      return {
        ...state,
        emailload: false,
        error: null,

        emailres: [...state.emailres, action.payload]
      };
    case types.LOGIN_FAILURE_EMAIL:
      return {
        ...state,
        emailload: false,
        error: action.payload.error
      };



    default:
      return state;
  }
}