import * as types from "../action-creators/types";
import data from '../../../assets/Data/production.json'
const  initialState = {
loading: false,
data: [data],
error: null
}
export default function (state = initialState, action) {
    switch (action.type) {
      case types.CONFIG_STARTED:
        return {
          ...state,
          loading: true
        };
      case types.CONFIG_SUCCESS:
        return {
          ...state,
          loading: false,
          error: null,
          data: [...state.data, action.payload]
        };
      case types.CONFIG_FAILURE:
        return {
          ...state,
          loading: false,
          error: action.payload.error
        };
      default:
        return state;
    }
  }
