#!/usr/bin/env bash
cd ..
docker login --username $username --password $password harbor.${MY_DOMAIN}
echo "harbor login successfully.."
docker push harbor.collabkare.dev/collabkare/$PROJECT_NAME:$BITBUCKET_COMMIT
echo "Pushed image into harbor successfully..."